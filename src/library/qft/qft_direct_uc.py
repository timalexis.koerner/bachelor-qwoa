"""QTF for arbitrary moduli with direct uncomputation via quantum phase estimation."""

from typing import Optional, Union
from math import ceil, log2
from qiskit import QuantumCircuit, QuantumRegister, AncillaRegister
from . import FSC, SEDirectUC


class QFTDirectUC(QuantumCircuit):
    r"""An fourier transformation with an arbitrary modulo.
    
    Quantum fourier transform based on the quantum fourier transform proposed by Kitaev.    
    
    Closely related to QFT, but uses state estimation with direct state
    uncomputation (SEDirectUC), resulting in lower depth.
    
    Example circuit for modulo=6, num_precision_qubits=num_garbage_qubits=2:
    .. parsed-literal::
    
                     ┌──────┐┌──────────────┐
            state_0: ┤0     ├┤0             ├
                     │      ││              │
            state_1: ┤1     ├┤1             ├
                     │      ││              │
            state_2: ┤2     ├┤2             ├
                     │      ││              │
        state_ext_0: ┤      ├┤3             ├
                     │      ││              │
        state_ext_1: ┤      ├┤4             ├
                     │  FSC ││              │
               ft_0: ┤3     ├┤5             ├
                     │      ││              │
               ft_1: ┤4     ├┤6  SEDirectUC ├
                     │      ││              │
               ft_2: ┤5     ├┤7             ├
                     │      ││              │
              aux_0: ┤6     ├┤8             ├
                     │      ││              │
              aux_1: ┤7     ├┤9             ├
                     └──────┘│              │
              aux_2: ────────┤10            ├
                             │              │
              aux_3: ────────┤11            ├
                             │              │
              aux_4: ────────┤12            ├
                             └──────────────┘
    
    **References:**

    [1] A.Yu.Kitaev, Quantum measurements and the Abelian Stabilizer Problem, 1995.
    `arxiv.org/abs/quant-ph/9511026 <https://arxiv.org/abs/quant-ph/9511026>`_
    
    """

    def __init__(self, modulo: int, num_precision_qubits: Optional[int] = 1, 
                 inplace_mult: Optional[bool] = False, num_garbage_qubits: Optional[int] = 0,
                 name: Optional[str] = "QFTPreMult") -> None:
        """A quantum fourier transform over an arbitrary modulo.       

        Args:
            modulo: Modulo of the quantum fourier transformation, also the number of superposed states.
            num_precision_qubits: Number of additional qubits (+1) determining the precision of the factor.
                May be reduced if ``inplace_mult`` to avoid trailing 0s in factors decimal digits. 
                The final ``num_precision_qubits`` is also the number of extension qubits for the state.
            inplace_mult: If the multiplication should be in-place (short depth but less precise) or out-of-place.
            num_garbage_qubits: Number of uncomputed state qubits (including precision qubits).
            name: Name of the circuit object.
        Raises:
            ValueError: If ``modulo`` is smaller than 1.
        """
        if modulo < 1:
            raise ValueError(
                "The modulo must be at least 1.")
        
        super().__init__(name=name)
        
        self._modulo = modulo
        if modulo == 1:
            self._num_state_qubits = 1
        else:
            self._num_state_qubits = ceil(log2(modulo))
                
        # circuit blocks
        fsc = FSC(modulo)
        self._se_direct_uc = SEDirectUC(
            modulo, num_precision_qubits=num_precision_qubits, inplace_mult=inplace_mult, num_garbage_qubits=num_garbage_qubits)
        num_ancillas = max(fsc.num_ancillas, self._se_direct_uc.num_ancillas)
        
        # registers
        regs = []
        qr_state = QuantumRegister(self._num_state_qubits, "state")
        regs.append(qr_state)
        all_state_qubits = qr_state[:]
        if self.num_precision_qubits > 0:
            qr_state_ext = QuantumRegister(self.num_precision_qubits, "state_ext")
            all_state_qubits += qr_state_ext[:]
            regs.append(qr_state_ext)
        qr_ft = QuantumRegister(self._num_state_qubits, "ft")
        regs.append(qr_ft)
        qr_aux = AncillaRegister(num_ancillas, "aux")
        regs.append(qr_aux)
        
        # build circuit
        circuit = QuantumCircuit(*regs, name=name)
        self.add_register(*regs)
        
        circuit.append(fsc.to_gate(), qr_state[:] + qr_ft[:] + qr_aux[:fsc.num_ancillas])
        circuit.append(self._se_direct_uc.to_gate(),
                       all_state_qubits + qr_ft[:] + qr_aux[:self._se_direct_uc.num_ancillas])
        
        self.append(circuit.to_gate(), self.qubits)

    @property
    def num_fourier_qubits(self) -> int:
        """The number of qubits in the fourier register.
        Returns:
            The number of fourier qubits.
        """
        return self._num_state_qubits
    
    @property
    def num_state_qubits(self) -> int:
        """The number of qubits for an input state.
        Returns:
            The number of state qubits for an input state.
        """
        return self._num_state_qubits
    
    @property
    def num_state_ext_qubits(self) -> int:
        """The number of extension qubits for the multiplied input state, 
        equivalent with the number of precision qubits.
        Returns:
            The number of precision qubits.
        """
        return self.num_precision_qubits

    @property
    def num_precision_qubits(self) -> int:
        """The number of additional qubits (+1) used by the factor.
        Returns:
            The number of precision qubits.
        """
        return self._se_direct_uc.num_precision_qubits
    
    @property
    def num_uncomputed_qubits(self) -> int:
        """The number uncomputed (not garbage) overall state qubits.
        Returns:
            The number of uncomputed overall state qubits.
        """
        return self._se_direct_uc.num_uncomputed_qubits
    
    @property
    def num_garbage_qubits(self) -> int:
        """The number garbage (not uncomputed) overall state qubits.
        Returns:
            The number of garbage overall state qubits.
        """
        return self._se_direct_uc.num_garbage_qubits

    @property
    def modulo(self) -> int:
        """The fourier transform modulo.
        Returns:
            The modulo.
        """
        return self._modulo
    
    @property
    def factor(self) -> Union[int, float]:
        """The factor used for the multiplication. For out-of-place 
        multiplication the factor is only approximate.        
        Returns:
            The factor used by the multiplication.
        """
        return self._se_direct_uc.factor
