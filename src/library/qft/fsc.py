"""Fourier state creation with an arbitrary modulo."""

from typing import Optional
from math import ceil, log2, pi
from qiskit import QuantumCircuit, QuantumRegister, AncillaRegister
from library.distributions import UniformDistribution


class FSC(QuantumCircuit):
    r"""Fourier state creation with an arbitrary modulo, i.e. an out-of-place
    quantum fourier transform.
    
    The fourier state is created from an uniform distribution over
    :math:`modulo` states. Given the initial state :math:`|s\rangle` with 
    :math:`s = (s_{n-1} \dots s_0)_2` each state :math:`|j\rangle` with
    :math:`j = (j_{n-1} \dots j_0)_2$` of the superposition is given the phase 
    :math:`\exp(2\pi i \frac{s j}{M})` using CRZ gates.
    
    Example circuit for modulo=11:
    .. parsed-literal::
    
                             ┌──────┐
        state_0: ────────────┤0     ├
                             │      │
        state_1: ────────────┤1     ├
                             │      │
        state_2: ────────────┤2     ├
                             │      │
        state_3: ────────────┤3     ├
                 ┌──────────┐│  CRz │
           ft_0: ┤0         ├┤4     ├
                 │          ││      │
           ft_1: ┤1         ├┤5     ├
                 │          ││      │
           ft_2: ┤2         ├┤6     ├
                 │          ││      │
           ft_3: ┤3         ├┤7     ├
                 │  UniDist │└──────┘
          aux_0: ┤4         ├────────
                 │          │        
          aux_1: ┤5         ├────────
                 │          │        
          aux_2: ┤6         ├────────
                 │          │        
          aux_3: ┤7         ├────────
                 └──────────┘        
    
    """

    def __init__(self, modulo: int, is_plus_sign: Optional[bool] = True,
                 name: Optional[str] = "FSC") -> None:
        """An out-of-place quantum fourier transform over an arbitrary modulo.       

        Args:
            modulo: Modulo of the quantum fourier transformation, also the number of superposed states.
            is_plus_sign: If the phases are positive.
            name: Name of the circuit object.
        Raises:
            ValueError: If ``modulo`` is smaller than 1.
        """
        if modulo < 1:
            raise ValueError(
                "The modulo must be at least 1.")
        
        super().__init__(name=name)
        
        self._modulo = modulo
        if modulo == 1:
            self._num_state_qubits = 1
        else:
            self._num_state_qubits = ceil(log2(modulo))
        
        # uniform distribution
        uni_dist = UniformDistribution(modulo)
        num_ancillas = uni_dist.num_ancillas
        
        # registers
        qr_state = QuantumRegister(self._num_state_qubits, "state")
        qr_ft = QuantumRegister(self._num_state_qubits, "ft")
        qr_aux = AncillaRegister(num_ancillas, "aux")
        regs = [qr_state, qr_ft, qr_aux]
        
        # build circuit
        circuit = QuantumCircuit(*regs, name=name)
        self.add_register(*regs)
        
        circuit.append(uni_dist.to_gate(), qr_ft[:] + qr_aux[:uni_dist.num_ancillas])
        circuit.append(self._controlled_rotations(is_plus_sign).to_gate(), qr_state[:] + qr_ft[:])

        self.append(circuit.to_gate(), self.qubits)

    @property
    def num_fourier_qubits(self) -> int:
        """The number of qubits in the fourier register.
        Returns:
            The number of fourier qubits.
        """
        return self._num_state_qubits
    
    @property
    def num_state_qubits(self) -> int:
        """The number of qubits for an input state.
        Returns:
            The number of state qubits for an input state.
        """
        return self._num_state_qubits

    @property
    def modulo(self) -> int:
        """The fourier transform modulo.
        Returns:
            The modulo.
        """
        return self._modulo

    def _controlled_rotations(self, is_plus_sign):
        """Circuit block containing the controlled rotations neccessary for
        the quantum fourier transform.
        
        Args:
            is_plus_sign: If the phases are positive.
        Returns:
            Circuit: Controlled rotations as a circuit.
        """
        qr_state = QuantumRegister(self._num_state_qubits, "state")
        qr_ft = QuantumRegister(self._num_state_qubits, "ft")
        circuit = QuantumCircuit(qr_state, qr_ft, name="CRz")
        
        for i in range(self._num_state_qubits):
            for j in range(self._num_state_qubits):
                phase = 2**(i+j) / self._modulo
                if phase == int(phase):
                    continue
                if not is_plus_sign:
                    phase = -phase
                circuit.crz(2*pi*phase, qr_state[i], qr_ft[j])
        return circuit
