"""Approximate QTF for arbitrary moduli with ancillary base-2-QFT."""

from math import log2, ceil
from typing import Optional
from qiskit import QuantumCircuit, QuantumRegister, AncillaRegister
from qiskit.circuit.library.basis_change import QFT
from library.adders import FullAdd
from library.multipliers import ImplicitCnstMult
from library.divisors import CnstDivisor


class AQFT(QuantumCircuit):
    r"""An approximate fourier transformation with an arbitrary modulo.
    
    The transformation is approximated over two simple fourier transforms
    (with modulo a power of two, one can be substituted by Hadamard-Gates)
    and multiplication and division by known constants. The approximation 
    accuracy can be increased with a parameter, but comes at a cost of 
    increased qubit requirements and increased depth and size. The first 
    qubits of the :math:`rmd` register are used as the input, the :math:`ft`
    register contains the fourier state qubits as the intial qubits.
    
    The circuit currently uses exact division (and its inversion) with 
    rest for the division as well as the multiplication with additional 
    summand, this increases the depth massively but produces no garbage
    in the auxilliary registers. The only exception being the remainder in
    the :math:`rmd` register, but this is (nearly) input independent.
    
    Example circuit for modulo=7 with precision eps=6:
    .. parsed-literal::
    
                    ┌──────────────┐┌──────┐┌───────────┐
        rmd_0: ─────┤4             ├┤0     ├┤6          ├
                    │              ││      ││           │
        rmd_1: ─────┤5             ├┤1     ├┤7          ├
                    │              ││      ││           │
        rmd_2: ─────┤6             ├┤2     ├┤8          ├
               ┌───┐│              ││      ││           │
        rmd_3: ┤ H ├┤7             ├┤3 QFT ├┤9          ├
               ├───┤│              ││      ││           │
        rmd_4: ┤ H ├┤8             ├┤4     ├┤10         ├
               ├───┤│              ││      ││           │
         ft_0: ┤ H ├┤9             ├┤5     ├┤11         ├
               ├───┤│   CnstDiv_dg ││      ││           │
         ft_1: ┤ H ├┤10            ├┤6     ├┤12         ├
               └───┘│              │└──────┘│   CnstDiv │
         ft_2: ─────┤11            ├────────┤13         ├
                    │              │        │           │
        aux_0: ─────┤0             ├────────┤0          ├
                    │              │        │           │
        aux_1: ─────┤1             ├────────┤1          ├
                    │              │        │           │
        aux_2: ─────┤2             ├────────┤2          ├
                    │              │        │           │
        aux_3: ─────┤3             ├────────┤3          ├
                    └──────────────┘        │           │
        aux_4: ─────────────────────────────┤4          ├
                                            │           │
        aux_5: ─────────────────────────────┤5          ├
                                            └───────────┘
    
    **References:**

    [1] Lisa R. Hales, The Quantum Fourier Transform and Extensions of the Abelian Hidden Subgroup Problem, 2002.
    `arXiv:quant-ph/0212002 <https://arxiv.org/abs/arXiv:quant-ph/0212002>`_
    
    """

    def __init__(self, modulo: int, eps: Optional[float] = 6, 
                 uncompute_remainder: Optional[bool] = False, num_uc_precision_qubits: Optional[int] = 0,
                 uc_remainder_operation: Optional[str] = "sub", suppress_warning: Optional[bool] = False,
                 name: Optional[str] = "AQFTWithSubQFT") -> None:
        """A quantum fourier transform over an arbitrary modulo.       

        Args:
            modulo: Modulo of the quantum fourier transformation, also the number of superposed states.
            eps: Precision measure, also determines the qubit requirements.
            uncompute_remainder: If remainder should be approximately uncomputed.
            num_uc_precision_qubits: Number of additional precision qubits for the remainder uncomputation.
            uc_remainder_operation: Uncomputation operation acting on the remainder register, can either be
                subtraction ("sub") or XOR ("xor").
            suppress_warning: If the possible precision warning for uncomputation should be suppressed.
            name: Name of the circuit object.

        Raises:
            ValueError: If ``modulo`` is smaller than 1.
            ValueError: If ``eps`` is smaller than 0.
            ValueError: If ``state_operation`` is neither "sub" nor "xor".
        """
        if modulo < 1:
            raise ValueError(
                "The modulo must be at least 1.")
        if eps <= 0:
            raise ValueError(
                "The precision eps must be greater than 0.")
        if uc_remainder_operation not in ["sub", "xor"]:
            raise ValueError(
                "The operation acting on the state must be eiter \"sub\" or \"xor\".")
        
        super().__init__(name=name)
        self._modulo = modulo
        self._eps = eps
        
        # parameters
        if modulo == 1:
            self._num_state_qubits = 1
        else:
            self._num_state_qubits = ceil(log2(modulo))
        small_qft = max(ceil(2 * log2(8*log2(modulo)/eps)), 1)
        big_qft = max(ceil(log2(4 * 2**small_qft * modulo/eps)), self._num_state_qubits + small_qft)
        dvsr = int(2**big_qft / modulo)
        fct = 2**(big_qft + num_uc_precision_qubits) / modulo - 2**num_uc_precision_qubits * dvsr
        
        # bulding blocks
        div = CnstDivisor(dvsr, num_quotient_qubits=self._num_state_qubits)
        num_ancillas = div.num_ancillas
        self._num_remainder_qubits = div.num_remainder_qubits
        mult = CnstDivisor(modulo, num_quotient_qubits=small_qft + 1)
        num_ancillas = max(num_ancillas,
                            mult.num_qubits - self.num_remainder_qubits - self.num_fourier_qubits)
        if uncompute_remainder:
            try:
                uc_mult = ImplicitCnstMult(fct, num_factor_qubits=self.num_state_qubits)
                num_ancillas = max(num_ancillas, uc_mult.num_ancillas + uc_mult.num_product_qubits)
                add = FullAdd(min(uc_mult.num_product_qubits - num_uc_precision_qubits, self._num_remainder_qubits),
                                has_cout=False)
            except ValueError:
                if not suppress_warning:
                    print("Warning: Not enough precision too uncompute remainder.")
                uncompute_remainder = False

        # build circuit
        qr_rmd = QuantumRegister(self._num_remainder_qubits, "rmd")
        qr_ft = QuantumRegister(self._num_state_qubits, "ft")
        qr_aux = AncillaRegister(num_ancillas, "aux")
        circuit = QuantumCircuit(qr_rmd, qr_ft, qr_aux, name=name)
        self.add_register(qr_rmd, qr_ft, qr_aux)
        
        if modulo > 1:
            num_work_qubits = mult.num_qubits - mult.num_ancillas
            work_qubits = (qr_rmd[:] + qr_ft[:] + qr_aux[:])[:num_work_qubits]
            ancilla_qubits = (qr_rmd[:] + qr_ft[:] + qr_aux[:])[num_work_qubits: mult.num_qubits]

            circuit.h(work_qubits[-small_qft - 1 : -1])
            circuit.append(mult.inverse().to_gate(), ancilla_qubits + work_qubits)
            
            circuit.append(QFT(big_qft).to_gate(), qr_rmd[:] + qr_ft[:-1])
            circuit.append(div.to_gate(), qr_aux[:div.num_ancillas] + qr_rmd[:] + qr_ft[:])
            
            if uncompute_remainder:
                product_qubits = qr_aux[1: 1 + uc_mult.num_product_qubits]
                circuit.append(uc_mult.to_gate(), [qr_aux[0]] + qr_ft[:] + product_qubits)
                
                high_product_qubits = product_qubits[num_uc_precision_qubits:][:add.num_state_qubits]
                if uc_remainder_operation == "sub":
                    circuit.x(qr_rmd[:len(high_product_qubits)])
                    circuit.append(add.to_gate(),
                                [qr_aux[0]] + high_product_qubits[:] + qr_rmd[:len(high_product_qubits)])
                    circuit.x(qr_rmd[:len(high_product_qubits)])
                else:
                    circuit.cx(high_product_qubits, qr_rmd[:len(high_product_qubits)])
                
                #uncompute
                circuit.append(uc_mult.inverse().to_gate(), [qr_aux[0]] + qr_ft[:] + product_qubits)
                
        self.append(circuit.to_gate(), self.qubits)

    @property
    def num_remainder_qubits(self) -> int:
        """The number of qubits is the remainder register.
        Returns:
            The number of remainder qubits.
        """
        return self._num_remainder_qubits

    @property
    def num_fourier_qubits(self) -> int:
        """The number of qubits in the fourier register.
        Returns:
            The number of fourier qubits.
        """
        return self._num_state_qubits
    
    @property
    def num_state_qubits(self) -> int:
        """The number of qubits for an input state.
        Returns:
            The number of state qubits for an input state.
        """
        return self._num_state_qubits

    @property
    def modulo(self) -> int:
        """The fourier transform modulo.
        Returns:
            The modulo.
        """
        return self._modulo
    
    @property
    def eps(self) -> int:
        """The precision measure.
        Returns:
            The precision.
        """
        return self._eps
