"""State estimation of fourier transformations with arbitrary modulo."""

from typing import Optional
from math import ceil, log2
from qiskit import QuantumCircuit, QuantumRegister, AncillaRegister
from library.multipliers import ImplicitCnstMult
from library.adders import FullAdd
from . import FPE


class SE(QuantumCircuit):
    r"""General state estimation for fourier transformations
    with arbitrary modulo.
    
    Using FPE, the approximate phase
    :math:`\left\lfloor 2^n\frac{s}{M} \right\rfloor/2^n`
    can be estimated but only with poor probability. 
    Increasing the probabilty for the highest qubits and
    increasing the precision requires additional qubits.
    The resulting state is multiplied with :math:`modulo`
    and the highest :math:`n` qubits are either subtracted
    or XOR-ed from the initial state :math:`|s\rangle`.
    While the CNOTs for XOR are faster to perform, subtraction 
    generally yields results closer to :math:`|0\rangle`,
    value wise. Because of the uncertain nature of :math:`FPE`
    and rounding errors the lowest qubits should be treated
    as garbage qubits. Applying the inversed multiplication
    and :math:`FPE^\dagger` uncomputes the ancilla register.    
    
    Example circuit for modulo=6, num_precision_qubits=2, uc_state_operation="sub":
    .. parsed-literal::
    
                   ┌───┐                    ┌──────────┐        ┌───┐                    
        state_0: ──┤ X ├────────────────────┤4         ├────────┤ X ├────────────────────
                   ├───┤                    │          │        ├───┤                    
        state_1: ──┤ X ├────────────────────┤5         ├────────┤ X ├────────────────────
                   ├───┤                    │          │        ├───┤                    
        state_2: ──┤ X ├────────────────────┤6         ├────────┤ X ├────────────────────
                 ┌─┴───┴─┐                  │          │        └───┘        ┌──────────┐
           ft_0: ┤5      ├──────────────────┤          ├─────────────────────┤5         ├
                 │       │                  │          │                     │          │
           ft_1: ┤6      ├──────────────────┤          ├─────────────────────┤6         ├
                 │       │                  │          │                     │          │
           ft_2: ┤7      ├──────────────────┤          ├─────────────────────┤7         ├
                 │       │┌────────────────┐│          │┌───────────────────┐│          │
          aux_0: ┤0      ├┤1               ├┤          ├┤1                  ├┤0         ├
                 │       ││                ││          ││                   ││          │
          aux_1: ┤1      ├┤2               ├┤          ├┤2                  ├┤1         ├
                 │       ││                ││          ││                   ││          │
          aux_2: ┤2      ├┤3               ├┤          ├┤3                  ├┤2         ├
                 │       ││                ││          ││                   ││          │
          aux_3: ┤3      ├┤4               ├┤  FullAdd ├┤4                  ├┤3         ├
                 │       ││                ││          ││                   ││          │
          aux_4: ┤4      ├┤5               ├┤          ├┤5                  ├┤4         ├
                 │   FPE ││                ││          ││                   ││   FPE_dg │
          aux_5: ┤8      ├┤6               ├┤          ├┤6                  ├┤8         ├
                 │       ││                ││          ││                   ││          │
          aux_6: ┤9      ├┤7  ImplCnstMult ├┤          ├┤7  ImplCnstMult_dg ├┤9         ├
                 │       ││                ││          ││                   ││          │
          aux_7: ┤10     ├┤8               ├┤          ├┤8                  ├┤10        ├
                 │       ││                ││          ││                   ││          │
          aux_8: ┤11     ├┤9               ├┤          ├┤9                  ├┤11        ├
                 │       ││                ││          ││                   ││          │
          aux_9: ┤12     ├┤10              ├┤1         ├┤10                 ├┤12        ├
                 │       ││                ││          ││                   ││          │
         aux_10: ┤13     ├┤11              ├┤2         ├┤11                 ├┤13        ├
                 │       ││                ││          ││                   ││          │
         aux_11: ┤14     ├┤12              ├┤3         ├┤12                 ├┤14        ├
                 │       ││                ││          ││                   ││          │
         aux_12: ┤15     ├┤0               ├┤0         ├┤0                  ├┤15        ├
                 └───────┘└────────────────┘└──────────┘└───────────────────┘└──────────┘
        
    """

    def __init__(self, modulo: int, num_precision_qubits: Optional[int] = 1,
                 uc_state_operation: Optional[str] = "sub",  name: Optional[str] = "SE") -> None:
        """State estimation of a quantum fourier transform over an arbitrary modulo.       

        Args:
            modulo: Modulo of the quantum fourier transformation, also the number of superposed states.
            num_precision_qubits: Number of additional qubits (+1) determining the precision of the uncomputation.
                ``num_precision_qubits`` is also the number of (auxillary) extension qubits for the state.
            uc_state_operation: Uncomputation operation acting on the state register, can either be subtraction
                ("sub") or XOR ("xor").
            name: Name of the circuit object.
        Raises:
            ValueError: If ``modulo`` is smaller than 1.
            ValueError: If ``num_precision_qubits`` is smaller than 0.
            ValueError: If ``state_operation`` is neither "sub" nor "xor".
        """
        if modulo < 1:
            raise ValueError(
                "The modulo must be at least 1.")
        if num_precision_qubits < 0:
            raise ValueError(
                "The number of precision qubits must be at least 0.")
        if uc_state_operation not in ["sub", "xor"]:
            raise ValueError(
                "The operation acting on the state must be eiter \"sub\" or \"xor\".")
        
        super().__init__(name=name)
        
        self._modulo = modulo
        if modulo == 1:
            self._num_state_qubits = 1
        else:
            self._num_state_qubits = ceil(log2(modulo))
        
        factor = modulo
        num_omitted_qubits = 0
        # reduce factor to save qubits
        while factor % 2 == 0:
            factor //= 2
            num_omitted_qubits += 1
        
        # circuit blocks
        fpe = FPE(modulo, num_state_qubits=self._num_state_qubits + num_precision_qubits)
        mult = ImplicitCnstMult(factor, fpe.num_state_qubits)
        num_ancillas = max(fpe.num_state_qubits + fpe.num_ancillas,
                           mult.num_factor_qubits + mult.num_product_qubits + 1)
        if uc_state_operation == "sub":
            full_adder = FullAdd(num_state_qubits=self._num_state_qubits, has_cout=False)
        
        # registers
        qr_state = QuantumRegister(self._num_state_qubits, "state")
        qr_ft = QuantumRegister(self._num_state_qubits, "ft")
        qr_aux = AncillaRegister(num_ancillas, "aux")
        
        # build circuit
        circuit = QuantumCircuit(qr_state, qr_ft, qr_aux, name=name)
        self.add_register(qr_state, qr_ft, qr_aux)

        circuit.append(fpe.to_gate(), 
                        qr_aux[:fpe.num_state_qubits] + qr_ft[:] + 
                        qr_aux[fpe.num_state_qubits: fpe.num_state_qubits + fpe.num_ancillas])
        
        circuit.append(mult.to_gate(),
                        [qr_aux[-1]] + qr_aux[:mult.num_factor_qubits + mult.num_product_qubits])
        approx_state_qubits = qr_aux[:mult.num_factor_qubits + mult.num_product_qubits][-self._num_state_qubits:]
        if uc_state_operation == "sub":
            # qr_state - qr_aux[approx state]
            circuit.x(qr_state)
            circuit.append(full_adder.to_gate(),
                            [qr_aux[-1]] + approx_state_qubits + qr_state[:])
            circuit.x(qr_state)
        else:
            circuit.cx(approx_state_qubits, qr_state[:])
        
        # uncomputation
        circuit.append(mult.inverse().to_gate(),
                        [qr_aux[-1]] + qr_aux[:mult.num_factor_qubits + mult.num_product_qubits])
        circuit.append(fpe.inverse().to_gate(), 
                        qr_aux[:fpe.num_state_qubits] + qr_ft[:] + 
                        qr_aux[fpe.num_state_qubits: fpe.num_state_qubits + fpe.num_ancillas])

        self.append(circuit.to_gate(), self.qubits)

    @property
    def num_fourier_qubits(self) -> int:
        """The number of qubits in the fourier register.
        Returns:
            The number of fourier qubits.
        """
        return self._num_state_qubits
    
    @property
    def num_state_qubits(self) -> int:
        """The number of qubits for an input state.
        Returns:
            The number of state qubits for an input state.
        """
        return self._num_state_qubits

    @property
    def modulo(self) -> int:
        """The fourier transform modulo.
        Returns:
            The modulo.
        """
        return self._modulo
