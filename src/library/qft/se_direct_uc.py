"""State estimation of fourier transformations with arbitrary modulo."""

from typing import Optional, Union
from math import ceil, log2
from qiskit import QuantumCircuit, QuantumRegister, AncillaRegister
from qiskit.circuit.library.basis_change import QFT
from library.multipliers import InPlaceCnstMult, CnstMult
from . import FPE


class SEDirectUC(QuantumCircuit):
    r"""General state estimation for fourier transformations
    with arbitrary modulo.
    
    The depth of the general SE can be greatly reduced if it is used to uncompute
    the leftover initial state of an fourier state creation (FSC).
    
    This is done by multiplying the intial state with an approximation
    :math:`f \approx 2^{n+n_p}/M, f \leq 2^{n+n_p}/M`. At the core part of the circuit,
    a single fourier phase estimation (FPE) is used to uncompute the approximately
    multiplied state :math:`|s'\rangle := |s\cdot f\rangle`. The transformed state can be
    represented with :math:`|\hat s'\rangle = \bigotimes_{j=0}^{n + n_p-1} 
    (|0\rangle + e^{i \cdot \phi_{approx}(j)} |1\rangle)/\sqrt{2}` with 
    :math:`\phi_{approx}(j) := 2 \pi \cdot \frac{s'}{2^{n + n_p}} \cdot 2^j`.
    Using modular addtion on controlled by each qubit, the phase at each qubit is
    substracted by the exact phase :math:`\phi_{exact}(j) = 2 \pi \cdot \frac{s}{M} \cdot 2^j`
    to get a phase close to 0. With an Hadamard gate following the controlled modular
    additions, the final state is close to :math:`|0\rangle`.
    
    There are two different kinds of multiplications implemented: In-place for low-depth
    but low-precision multiplication and out-of-place for higher depth and precision.
    The higher precision stems form approximate float multiplication. The out-of-place
    mutliplication is SWAPped back into the previously uncomputed stat register.
    
    Example circuit for modulo=6, num_precision_qubits=num_garbage_qubits=2, inplace_mult=False:
    .. parsed-literal::
    
                     ┌───────────────────────────┐┌───┐                         ┌──────┐┌──────────┐
            state_0: ┤4                          ├┤ X ├────────────■────────────┤0     ├┤0         ├
                     │                           │└─┬─┘┌───┐       │            │      ││          │
            state_1: ┤5                          ├──┼──┤ X ├───────┼────■───────┤1     ├┤1         ├
                     │                           │  │  └─┬─┘┌───┐  │    │       │      ││          │
            state_2: ┤6                          ├──┼────┼──┤ X ├──┼────┼────■──┤2 QFT ├┤2         ├
                     │                           │  │    │  └─┬─┘  │    │    │  │      ││          │
        state_ext_0: ┤10                         ├──┼────┼────┼────┼────┼────┼──┤3     ├┤          ├
                     │                           │  │    │    │    │    │    │  │      ││          │
        state_ext_1: ┤11                         ├──┼────┼────┼────┼────┼────┼──┤4     ├┤          ├
                     │                           │  │    │    │    │    │    │  └──────┘│          │
               ft_0: ┤                           ├──┼────┼────┼────┼────┼────┼──────────┤3         ├
                     │                           │  │    │    │    │    │    │          │          │
               ft_1: ┤                           ├──┼────┼────┼────┼────┼────┼──────────┤4  FPE_dg ├
                     │                           │  │    │    │    │    │    │          │          │
               ft_2: ┤   Mult(5.333333333333333) ├──┼────┼────┼────┼────┼────┼──────────┤5         ├
                     │                           │  │    │    │  ┌─┴─┐  │    │          │          │
              aux_0: ┤7                          ├──■────┼────┼──┤ X ├──┼────┼──────────┤6         ├
                     │                           │       │    │  └───┘┌─┴─┐  │          │          │
              aux_1: ┤8                          ├───────■────┼───────┤ X ├──┼──────────┤7         ├
                     │                           │            │       └───┘┌─┴─┐        │          │
              aux_2: ┤9                          ├────────────■────────────┤ X ├────────┤8         ├
                     │                           │                         └───┘        │          │
              aux_3: ┤0                          ├──────────────────────────────────────┤9         ├
                     │                           │                                      │          │
              aux_4: ┤1                          ├──────────────────────────────────────┤10        ├
                     │                           │                                      └──────────┘
              aux_5: ┤2                          ├──────────────────────────────────────────────────
                     │                           │                                                  
              aux_6: ┤3                          ├──────────────────────────────────────────────────
                     └───────────────────────────┘                                                  
    
    """

    def __init__(self, modulo: int, num_precision_qubits: Optional[int] = 1, 
                 inplace_mult: Optional[bool] = False, num_garbage_qubits: Optional[int] = 0,
                 name: Optional[str] = "SEDUC") -> None:
        """State estimation of a quantum fourier transform over an arbitrary modulo.     

        Args:
            modulo: Modulo of the quantum fourier transformation, also the number of superposed states.
            num_precision_qubits: Number of additional qubits (+1) determining the precision of the factor.
                May be reduced if ``inplace_mult`` to avoid trailing 0s in factors decimal digits. 
                The final ``num_precision_qubits`` is also the number of extension qubits for the state.
            inplace_mult: If the multiplication should be in-place (short depth but less precise) or out-of-place.
            num_garbage_qubits: Number of uncomputed state qubits (including precision qubits).
            name: Name of the circuit object.
        Raises:
            ValueError: If ``modulo`` is smaller than 1.
            ValueError: If ``num_precision_qubits`` is smaller than 0.
            ValueError: If ``num_garbage_qubits`` is greater or equal tothan the number of state qubits.
        """
        if modulo < 1:
            raise ValueError(
                "The modulo must be at least 1.")
        if num_precision_qubits < 0:
            raise ValueError(
                "The number of precision qubits must be at least 0.")
        
        super().__init__(name=name)
        
        self._modulo = modulo
        if modulo == 1:
            self._num_state_qubits = 1
        else:
            self._num_state_qubits = ceil(log2(modulo))
        
        if num_garbage_qubits > self._num_state_qubits:
            raise ValueError(
                "The number of garbage qubits must be smaller or equal to the number of state qubits.")
        self._num_garbage_qubits = num_garbage_qubits
                
        if num_garbage_qubits == self._num_state_qubits:
            self._factor = 1
            self._num_precision_qubits = 0
        else:
            self._factor = 2**(ceil(log2(modulo)) + num_precision_qubits) / modulo
            if inplace_mult:
                self._factor = int(self._factor)
                # reduce factor to save qubits
                while self._factor % 2 == 0:
                    self._factor //= 2
                    num_precision_qubits -= 1
            self._num_precision_qubits = num_precision_qubits
        
        # circuit blocks
        num_ancillas = 0
        if self._num_precision_qubits > 0:
            if inplace_mult:
                # no padding qubits due to odd factor
                mult = InPlaceCnstMult(
                    self._factor, num_factor_qubits=self._num_state_qubits,
                    omit_highest_qubit=True, name=f"Mult({self._factor})")
                num_ancillas = max(num_ancillas, mult.num_constant_qubits)
            else:
                mult = CnstMult(
                    self._factor, num_factor_qubits=self._num_state_qubits, uncompute=True,
                    omit_highest_qubit=True, name=f"Mult({self._factor})")
                num_ancillas = max(num_ancillas, mult.num_constant_qubits + mult.num_product_qubits - self._num_precision_qubits)
        if self.num_uncomputed_qubits > 0:
            pe_dg = FPE(modulo, num_state_qubits=self.num_uncomputed_qubits,
                        append_QFT=False).inverse()
            num_ancillas = max(num_ancillas, pe_dg.num_ancillas)
        
        # registers
        regs = []
        qr_state = QuantumRegister(self._num_state_qubits, "state")
        regs.append(qr_state)
        all_state_qubits = qr_state[:]
        if self._num_precision_qubits > 0:
            qr_state_ext = QuantumRegister(self._num_precision_qubits, "state_ext")
            all_state_qubits += qr_state_ext[:]
            regs.append(qr_state_ext)
        qr_ft = QuantumRegister(self._num_state_qubits, "ft")
        regs.append(qr_ft)
        qr_aux = AncillaRegister(num_ancillas, "aux")
        regs.append(qr_aux)
        
        # build circuit
        circuit = QuantumCircuit(*regs, name=name)
        self.add_register(*regs)
        
        if self._num_precision_qubits > 0:
            if inplace_mult:
                circuit.append(mult.to_gate(), qr_aux[:mult.num_constant_qubits] + qr_state[:] + qr_state_ext[:])
            else:
                low_product_qubits = qr_aux[: self._num_state_qubits]
                circuit.append(mult.to_gate(), 
                               qr_aux[self._num_state_qubits: self._num_state_qubits + mult.num_constant_qubits] 
                               + qr_state[:] + low_product_qubits + qr_state_ext[:])
                # half swap with 0-register
                circuit.cx(low_product_qubits, qr_state)
                circuit.cx(qr_state, low_product_qubits)
        
        if self.num_uncomputed_qubits > 0:
            circuit.append(QFT(len(all_state_qubits)).to_gate(), all_state_qubits)
            circuit.append(pe_dg.to_gate(),
                        all_state_qubits[:len(all_state_qubits) - num_garbage_qubits] + qr_ft[:] + qr_aux[:pe_dg.num_ancillas])

        self.append(circuit.to_gate(), self.qubits)

    @property
    def num_fourier_qubits(self) -> int:
        """The number of qubits in the fourier register.
        Returns:
            The number of fourier qubits.
        """
        return self._num_state_qubits
    
    @property
    def num_state_qubits(self) -> int:
        """The number of qubits for an input state.
        Returns:
            The number of state qubits for an input state.
        """
        return self._num_state_qubits
    
    @property
    def num_state_ext_qubits(self) -> int:
        """The number of extension qubits for the multiplied input state, 
        equivalent with the number of precision qubits.
        Returns:
            The number of precision qubits.
        """
        return self._num_precision_qubits

    @property
    def num_precision_qubits(self) -> int:
        """The number of additional qubits (+1) used by the factor.
        Returns:
            The number of precision qubits.
        """
        return self._num_precision_qubits
    
    @property
    def num_uncomputed_qubits(self) -> int:
        """The number uncomputed (not garbage) overall state qubits.
        Returns:
            The number of uncomputed overall state qubits.
        """
        return self._num_state_qubits + self._num_precision_qubits - self._num_garbage_qubits
    
    @property
    def num_garbage_qubits(self) -> int:
        """The number garbage (not uncomputed) overall state qubits.
        Returns:
            The number of garbage overall state qubits.
        """
        return self._num_garbage_qubits

    @property
    def modulo(self) -> int:
        """The fourier transform modulo.
        Returns:
            The modulo.
        """
        return self._modulo
    
    @property
    def factor(self) -> Union[int, float]:
        """The factor used for the multiplication. For out-of-place 
        multiplication the factor is only approximate.        
        Returns:
            The factor used by the multiplication.
        """
        return self._factor
