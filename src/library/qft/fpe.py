"""Quantum phase estimation for fourier transforms."""

from typing import Optional, Any, Tuple, List
from math import ceil, log2
from qiskit import QuantumCircuit, QuantumRegister, AncillaRegister
from qiskit.circuit.library.basis_change import QFT
from qiskit.circuit.library.arithmetic import IntegerComparator
from library.comparators import Comparator, CtrlIntegerComparator
from library.adders import FullAdd, CtrlCnstHalfSub, CtrlCnstHalfAdd


class FPE(QuantumCircuit):
    r"""Quantum phase estimation for fourier transforms over 
    an arbitrary moduli :math:`M`, as proposed by Kitaev.
    
    The phase estimation is composed of controlled modular additions of summands
    :math:`a_k = 2^k \mod M` on a second state with the same modulus.
    The controlled modular addition is build from four steps: 
    controlled constant addition (:math:`+a_k`), integer comparision (:math:`>=M`), 
    controlled substraction (:math:`-M`), constant comparision (:math:`>a_k`)
    to uncompute the previous comparision. In order to perform a proper modulo operation
    the resulting sum with the second state :math:`b<M` in the first step requires
    :math:`max(a_k + b) < 2M \Rightarrow a_k \leq M`, so that one substraction (step 3) suffices. 
    Some summands can be grouped s.t. :math:`\exist K: |K|>1, a_K \coloneqq \sum_{k\in K} a_k \leq M`,
    then the first and last step act on the previously accumulated summand register containing
    :math:`a_K`, which is non-constant but does not need controls. If the second state is a fourier
    transform over an intial state :math:`|s\rangle \to |b\rangle = |\hat s\rangle` the resulting
    phase shift of a controlled modular addition is :math:`exp(-2\pi i \frac{sa}{M})`. 
    The inverse results in a positive phase shift.
    
    Example circuit for modulo=11:
    .. parsed-literal::
    
                 ┌───┐                       ┌───────────────────────┐┌─────────┐
        state_0: ┤ H ├───────────────────────┤0                      ├┤0        ├
                 ├───┤                       │                       ││         │
        state_1: ┤ H ├───────────────────────┤1                      ├┤1        ├
                 ├───┤                       │                       ││  QFT_dg │
        state_2: ┤ H ├───────────────────────┤2                      ├┤2        ├
                 ├───┤┌─────────────────────┐│                       ││         │
        state_3: ┤ H ├┤0                    ├┤                       ├┤3        ├
                 └───┘│                     ││                       │└─────────┘
           ft_0: ─────┤1                    ├┤3                      ├───────────
                      │                     ││                       │           
           ft_1: ─────┤2                    ├┤4                      ├───────────
                      │                     ││                       │           
           ft_2: ─────┤3                    ├┤5                      ├───────────
                      │                     ││   SuperPosModAdder_dg │           
           ft_3: ─────┤4                    ├┤6                      ├───────────
                      │                     ││                       │           
          aux_0: ─────┤5  SingleModAdder_dg ├┤7                      ├───────────
                      │                     ││                       │           
          aux_1: ─────┤6                    ├┤8                      ├───────────
                      │                     ││                       │           
          aux_2: ─────┤7                    ├┤9                      ├───────────
                      │                     ││                       │           
          aux_3: ─────┤8                    ├┤10                     ├───────────
                      │                     ││                       │           
          aux_4: ─────┤9                    ├┤11                     ├───────────
                      │                     ││                       │           
          aux_5: ─────┤10                   ├┤12                     ├───────────
                      └─────────────────────┘└───────────────────────┘           
    
    **References:**

    [1] A.Yu.Kitaev, Quantum measurements and the Abelian Stabilizer Problem, 1995.
    `arxiv.org/abs/quant-ph/9511026 <https://arxiv.org/abs/quant-ph/9511026>`_
    
    """

    def __init__(self, modulo: int, num_state_qubits: Optional[int] = None,
                 append_QFT: Optional[bool] = True, name: Optional[str] = "FPE") -> None:
        """Quantum phase estimation for arbitrary moduli.       

        Args:
            modulo: Modulo of the quantum fourier transformation, also the number of superposed states.
            num_state_qubits: Number of state qubits controlling the modular additions.
            append_QFT: If the QFT should be included.
            name: Name of the circuit object.
        Raises:
            ValueError: If ``modulo`` is smaller than 1.
            ValueError: If ``num_state_qubits`` is smaller than 1.
        """
        if modulo < 1:
            raise ValueError(
                "The modulo must be at least 1.")
        self._modulo = modulo
        self._num_fourier_qubits = ceil(log2(modulo))
        if modulo == 1:
            self._num_fourier_qubits = 1
        if num_state_qubits is None:
            num_state_qubits = self._num_fourier_qubits
        elif num_state_qubits < 1:
            raise ValueError(
                "The number of state qubits must be at least 1.")
        self._num_state_qubits = num_state_qubits
        
        super().__init__(name=name)
        
        mod_adders = []
        mod_adder_ctrl_qubit_nums = []
        
        # modular addition with the first n qubits, so that 2^(n+1)-1 <= modulo
        if self._num_fourier_qubits > 1:
            mod_adders.append(self._superpos_mod_adder([]))
            ctrl_qubit_nums = list(range(min(self._num_fourier_qubits - 1, self._num_state_qubits)))
            mod_adder_ctrl_qubit_nums.append(ctrl_qubit_nums)
        # modular addition for the remaining qubits
        remaining_mod_adder_infos = []
        for qubit_num in range(self._num_fourier_qubits - 1, self._num_state_qubits):
            summand = 2**qubit_num % modulo
            if summand != 0:
                remaining_mod_adder_infos.append((qubit_num, summand))
        
        for group in self._first_fit_decreasing(remaining_mod_adder_infos, modulo):
            if len(group) == 1:
                ctrl_qubit_num, summand = group[0]
                
                mod_adders.append(self._single_mod_adder(summand))
                mod_adder_ctrl_qubit_nums.append([ctrl_qubit_num])
            else:
                ctrl_qubit_nums = [mod_adder_info[0] for mod_adder_info in group]
                summands = [mod_adder_info[1] for mod_adder_info in group]
                
                mod_adders.append(self._superpos_mod_adder(summands))
                mod_adder_ctrl_qubit_nums.append(ctrl_qubit_nums)   
    
        self._num_ancillas = 0    
        for mod_adder in mod_adders:
            self._num_ancillas = max(self._num_ancillas, mod_adder.num_ancillas)
        
        # quantum registers
        qr_state = QuantumRegister(self._num_state_qubits, "state")
        qr_ft = QuantumRegister(self._num_fourier_qubits, "ft")
        qr_aux = AncillaRegister(self._num_ancillas, "aux")
        
        # build inverse circuit
        circuit = QuantumCircuit(name=name)
        circuit.add_register(qr_state, qr_ft, qr_aux)
        self.add_register(qr_state, qr_ft, qr_aux)
        
        if append_QFT:
            circuit.append(QFT(self._num_state_qubits).to_gate(), qr_state)
        
        for i in range(len(mod_adders)):
            ctrl_qubits = [qr_state[qubit_num] for qubit_num in mod_adder_ctrl_qubit_nums[i]]
            circuit.append(mod_adders[i].to_gate(), 
                           ctrl_qubits + qr_ft[:] + qr_aux[:mod_adders[i].num_ancillas])                  

        circuit.h(qr_state)

        self.append(circuit.inverse().to_gate(), self.qubits)
    
    @property
    def num_state_qubits(self) -> int:
        """The number of qubits for an input state.
        Returns:
            The number of state qubits for an input state.
        """
        return self._num_state_qubits

    @property
    def num_fourier_qubits(self) -> int:
        """The number of qubits in the quotient register.
        Returns:
            The number of quotient qubits.
        """
        return self._num_fourier_qubits
    
    @property
    def modulo(self) -> int:
        """The fourier transform modulo.
        Returns:
            The modulo.
        """
        return self._modulo
    

    def _first_fit_decreasing(self, items: List[Tuple[Any, int]], capacity: int) -> List[List[Any]]:
        """Approximation algorithm for the bin packing problem.
        First sorts the items by size in decreasing order, then uses
        the first possible bin.

        Args:
            items: Tuples of (id, weight) representing each item.
            capacity: Maximum (inlcusive) size of each bin.
        Returns:
            The bins as grouped lists of items.
        """
        items = sorted(items, key=lambda x:x[1], reverse=True)
        bins = []
        for item in items:
            weight = item[1]
            inserted = False
            for bin in bins:
                if bin[1] + weight <= capacity:
                    bin[0].append(item)
                    bin[1] += weight
                    inserted = True
                    break
            
            if not inserted:
                bins.append([[item], weight])
        return [bin[0] for bin in bins]
        
    def _single_mod_adder(self, summand: int) -> QuantumCircuit:
        """Performs a controlled modular addition of a constant
        on a fourier register.

        Args:
            summand (int): Constant summand of the addition.
        Returns:
            QuantumCircuit: Controlled modular adder as a circuit.
        """       
        # building blocks
        ctrl_add = CtrlCnstHalfAdd(summand, num_state_qubits=self._num_fourier_qubits, name=f"CtrlAdd(+{summand})")
        int_comp = IntegerComparator(self._num_fourier_qubits + 1, self._modulo, name=f"IntCmp(>={self._modulo})")
        ctrl_sub = CtrlCnstHalfSub(self._modulo, num_state_qubits=self._num_fourier_qubits + 1, has_cout=False, name=f"CtrlSub(-{self._modulo})")
        ctrl_cmp = CtrlIntegerComparator(summand, num_state_qubits=self._num_fourier_qubits, geq=False)
        
        qr_ctrl = QuantumRegister(1, "ctrl")
        qr_ft = QuantumRegister(self._num_fourier_qubits, "ft")
        qr_ft_ext = AncillaRegister(1, "ft_ext")
        qr_mod_ctrl = AncillaRegister(1, "mod_ctrl")
        qr_aux = AncillaRegister(max(ctrl_add.num_constant_qubits, 
                                     int_comp.num_ancillas, 
                                     ctrl_sub.num_constant_qubits,
                                     self._num_fourier_qubits), "aux")
        circuit = QuantumCircuit(qr_ctrl, qr_ft, qr_ft_ext, qr_mod_ctrl, qr_aux, name="SingleModAdder")
        
        circuit.append(ctrl_add.to_gate(), qr_aux[:ctrl_add.num_constant_qubits] + qr_ft[:] + [qr_ft_ext[0]] + [qr_ctrl[0]])
        circuit.append(int_comp.to_gate(), qr_ft[:] + [qr_ft_ext[0]] + [qr_mod_ctrl[0]] + qr_aux[:int_comp.num_ancillas])
        circuit.append(ctrl_sub.to_gate(), qr_aux[:ctrl_sub.num_constant_qubits] + qr_ft[:] + [qr_ft_ext[0]] + [qr_mod_ctrl[0]])
        circuit.append(ctrl_cmp.to_gate(), qr_ft[:] + [[qr_ctrl[0]]] + [qr_mod_ctrl[0]] + qr_aux[:self._num_fourier_qubits])
        
        return circuit

    def _superpos_mod_adder(self, summands: List[int]) -> QuantumCircuit:
        """Performs a modular addition of a register containing 
        superposed summands on a fourier register. The summands are
        intialized with addition controlled additions, with qubits taken
        in the order of the given summands, and will be uncomputed.
        
        Args:
            summands (List[int]): Constant summands of the addition. 
                Can be empty for the initial partly input register,
                since the last qubit would enable to have a sum greater
                than modulo, the last qr_mod_sum (qr_state) qubit will 
                then be substituded with the last ancilla qubit. 
        Returns:
            QuantumCircuit: Superposition modular adder as a circuit.
        """
        # building blocks
        full_adder = FullAdd(self._num_fourier_qubits)
        int_comp = IntegerComparator(self._num_fourier_qubits + 1, self._modulo, name=f"IntCmp(>={self._modulo})")
        ctrl_sub = CtrlCnstHalfSub(self._modulo, num_state_qubits=self._num_fourier_qubits + 1, has_cout=False, name=f"CtrlSub(-{self._modulo})")
        cmp = Comparator(self._num_fourier_qubits, geq=False)
        
        num_ancillas = max(2, int_comp.num_ancillas, ctrl_sub.num_constant_qubits)
        ctrl_adders = []
        for summand in summands:
            ctrl_add = CtrlCnstHalfAdd(
                summand, num_state_qubits=self._num_fourier_qubits, has_cout=False, name=f"CtrlAdd(+{summand})")
            ctrl_adders.append(ctrl_add)
            num_ancillas = max(num_ancillas, ctrl_add.num_constant_qubits)
        
        regs = []
        if len(summands) > 0:
            qr_ctrl = QuantumRegister(len(summands), "ctrl")
            regs.append(qr_ctrl)
        else:
            # for state register, no ancilla
            qr_mod_sum = QuantumRegister(min(self._num_fourier_qubits - 1, self._num_state_qubits), "mod_sum")
            regs.append(qr_mod_sum)
        
        qr_ft = QuantumRegister(self._num_fourier_qubits, "ft")
        qr_ft_ext = AncillaRegister(1, "ft_ext")
        regs += [qr_ft, qr_ft_ext]
        
        if len(summands) > 0:
            # for ancilla register
            qr_mod_sum = AncillaRegister(self._num_fourier_qubits, "mod_sum")
            regs.append(qr_mod_sum)
            
        qr_mod_ctrl = AncillaRegister(1, "mod_ctrl")
        qr_aux = AncillaRegister(num_ancillas, "aux")
        regs += [qr_mod_ctrl, qr_aux]
        circuit = QuantumCircuit(*regs, name="SuperPosModAdder")
        
        state_qubits = qr_mod_sum[:]
        if len(summands) == 0:
            state_qubits += qr_aux[-(self._num_fourier_qubits - len(qr_mod_sum)):]
        
        # accumulate qr_mod_sum
        for idx, ctrl_add in enumerate(ctrl_adders):
            circuit.append(
                ctrl_add.to_gate(), 
                qr_aux[:ctrl_add.num_constant_qubits] + qr_mod_sum[:] + [qr_ctrl[idx]])
        
        circuit.append(full_adder.to_gate(), [qr_aux[0]] + state_qubits + qr_ft[:] + [qr_ft_ext[0]])
        circuit.append(int_comp.to_gate(), qr_ft[:] + [qr_ft_ext[0]] + [qr_mod_ctrl[0]] + qr_aux[:int_comp.num_ancillas])
        circuit.append(ctrl_sub.to_gate(), qr_aux[:ctrl_sub.num_constant_qubits] + qr_ft[:] + [qr_ft_ext[0]] + [qr_mod_ctrl[0]])
        circuit.append(cmp.to_gate(), qr_aux[:cmp.num_ancillas] + qr_ft[:] + state_qubits + [qr_mod_ctrl[0]])
        
        # uncompute qr_mod_sum
        for idx, ctrl_add in enumerate(reversed(ctrl_adders)):
            circuit.append(
                ctrl_add.inverse().to_gate(), 
                qr_aux[:ctrl_add.num_constant_qubits] + qr_mod_sum[:] + [qr_ctrl[len(ctrl_adders) - 1 - idx]])

        return circuit
