"""QTF for arbitrary moduli with quantum phase estimation."""

from typing import Optional
from math import ceil, log2
from qiskit import QuantumCircuit, QuantumRegister, AncillaRegister
from . import FSC, SE


class QFT(QuantumCircuit):
    r"""An fourier transformation with an arbitrary modulo.
    
    Quantum fourier transform using quantum phase estimation proposed by Kitaev.
    
    The quantum fourier transform is created from an out-of-place fourier
    transformation (fourier state creation FSC) and uncomputation of the
    state register with general state estimation (SE, uses fourier phase estimation FPE).
    The result is the fourier transformation :math:`QFT_M|s\rangle`. 
    The resulting circuit has a high depth, restricting its current use.
    
    Example circuit for modulo=6, num_precision_qubits=2:
    .. parsed-literal::
    
                 ┌──────┐┌──────┐
        state_0: ┤0     ├┤0     ├
                 │      ││      │
        state_1: ┤1     ├┤1     ├
                 │      ││      │
        state_2: ┤2     ├┤2     ├
                 │      ││      │
           ft_0: ┤3     ├┤3     ├
                 │  FSC ││      │
           ft_1: ┤4     ├┤4     ├
                 │      ││      │
           ft_2: ┤5     ├┤5     ├
                 │      ││      │
          aux_0: ┤6     ├┤6     ├
                 │      ││      │
          aux_1: ┤7     ├┤7     ├
                 └──────┘│      │
          aux_2: ────────┤8     ├
                         │      │
          aux_3: ────────┤9  SE ├
                         │      │
          aux_4: ────────┤10    ├
                         │      │
          aux_5: ────────┤11    ├
                         │      │
          aux_6: ────────┤12    ├
                         │      │
          aux_7: ────────┤13    ├
                         │      │
          aux_8: ────────┤14    ├
                         │      │
          aux_9: ────────┤15    ├
                         │      │
         aux_10: ────────┤16    ├
                         │      │
         aux_11: ────────┤17    ├
                         │      │
         aux_12: ────────┤18    ├
                         └──────┘
                 
    **References:**

    [1] A.Yu.Kitaev, Quantum measurements and the Abelian Stabilizer Problem, 1995.
    `arxiv.org/abs/quant-ph/9511026 <https://arxiv.org/abs/quant-ph/9511026>`_
    
    """

    def __init__(self, modulo: int, num_precision_qubits: Optional[int] = 1,
                 name: Optional[str] = "QFT") -> None:
        """A quantum fourier transform over an arbitrary modulo.       

        Args:
            modulo: Modulo of the quantum fourier transformation, also the number of superposed states.
            num_precision_qubits: Number of additional qubits (+1) determining the precision of the uncomputation.
                ``num_precision_qubits`` is also the number (auxillary) of extension qubits for the state.
            name: Name of the circuit object.
        Raises:
            ValueError: If ``modulo`` is smaller than 1.
        """
        if modulo < 1:
            raise ValueError(
                "The modulo must be at least 1.")
        
        super().__init__(name=name)
        
        self._modulo = modulo
        if modulo == 1:
            self._num_state_qubits = 1
        else:
            self._num_state_qubits = ceil(log2(modulo))
        
        # circuit blocks
        fsc = FSC(modulo)
        se = SE(modulo, num_precision_qubits=num_precision_qubits, uc_state_operation="sub")
        num_ancillas = max(fsc.num_ancillas, se.num_ancillas)
        
        # registers
        qr_state = QuantumRegister(self._num_state_qubits, "state")
        qr_ft = QuantumRegister(self._num_state_qubits, "ft")
        qr_aux = AncillaRegister(num_ancillas, "aux")
        
        # build circuit
        circuit = QuantumCircuit(qr_state, qr_ft, qr_aux, name=name)
        self.add_register(qr_state, qr_ft, qr_aux)
        
        circuit.append(fsc.to_gate(), qr_state[:] + qr_ft[:] + qr_aux[:fsc.num_ancillas])
        circuit.append(se.to_gate(), qr_state[:] + qr_ft[:] + qr_aux[:se.num_ancillas])

        self.append(circuit.to_gate(), self.qubits)

    @property
    def num_fourier_qubits(self) -> int:
        """The number of qubits in the fourier register.
        Returns:
            The number of fourier qubits.
        """
        return self._num_state_qubits
    
    @property
    def num_state_qubits(self) -> int:
        """The number of qubits for an input state.
        Returns:
            The number of state qubits for an input state.
        """
        return self._num_state_qubits

    @property
    def modulo(self) -> int:
        """The fourier transform modulo.
        Returns:
            The modulo.
        """
        return self._modulo
