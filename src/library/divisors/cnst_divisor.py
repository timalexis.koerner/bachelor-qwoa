"""Compute the quotient and rest of a qubit devided by a constant."""

from typing import Optional
from math import log2, ceil
from qiskit.circuit import QuantumCircuit, QuantumRegister, AncillaRegister
from ..adders import CnstHalfSub, CnstAddSub, CtrlCnstHalfAdd


class CnstDivisor(QuantumCircuit):
    r"""Compute the the quotient and remainder of the division by a constant. 
    The dividend needs to be initialized in the following way: The lowest bits
    are set in the :math:`rmd` register, the remaining bits are set using the 
    all but the highest (reserved for negation) qubits of the :math:`qt` register.
    After the circuit the register :math:`qt` will store the quotient and the
    :math:`rmd` register the remainder.
    
    The constant adder-subtractors are improved by toggeling the inversion
    for subtraction depending on the previous adder-subtractors inversion status.

    The circuit is based on the non-restoring division approach propsed by Thapliyal et al..  

    Example circuit with 4 constant qubits and 3 quotient qubits:
    .. parsed-literal::
                ┌──────────────┐     ┌─────────────┐     ┌─────────────┐┌──────────────────┐     
        cnst_0: ┤0             ├─────┤1            ├─────┤2            ├┤0                 ├─────
                │              │     │             │     │             ││                  │     
        cnst_1: ┤1             ├─────┤2            ├─────┤3            ├┤1                 ├─────
                │              │     │             │     │             ││                  │     
        cnst_2: ┤2             ├─────┤3            ├─────┤4            ├┤2                 ├─────
                │              │     │             │     │             ││                  │     
        cnst_3: ┤3             ├─────┤4            ├─────┤5            ├┤                  ├─────
                │              │     │             │     │             ││  CtrlCnstHalfAdd │     
         rmd_0: ┤              ├─────┤             ├─────┤6            ├┤3                 ├─────
                │  CnstHalfSub │     │  CnstAddSub │     │  CnstAddSub ││                  │     
         rmd_1: ┤              ├─────┤5            ├─────┤7            ├┤4                 ├─────
                │              │     │             │     │             ││                  │     
         rmd_2: ┤4             ├─────┤6            ├─────┤8            ├┤5                 ├─────
                │              │     │             │     │             ││                  │┌───┐
          qt_0: ┤5             ├─────┤7            ├─────┤9            ├┤6                 ├┤ X ├
                │              │     │             │┌───┐│             │└──────────────────┘└───┘
          qt_1: ┤6             ├─────┤8            ├┤ X ├┤1            ├─────────────────────────
                │              │┌───┐│             │└───┘│             │                         
          qt_2: ┤7             ├┤ X ├┤0            ├─────┤0            ├─────────────────────────
                └──────────────┘└───┘└─────────────┘     └─────────────┘                         

    **References:**

    [1] Thapliyal et al., Quantum Circuit Designs of Integer Division Optimizing T-count and T-depth, 2018.
    `arXiv:1809.09732 <https://arxiv.org/pdf/arXiv:1809.09732.pdf>`_

    """

    def __init__(
        self, constant: int, num_quotient_qubits: int, num_constant_qubits: Optional[int] = None,
        name: Optional[str] = "CnstDiv", suppress_warning: Optional[bool] = False
    ) -> None:
        """
        Args:
            constant: Number to multiply with.
            num_constant_qubits: Size of the register :math:`cnst` in which the (negated) constant is stored.
                The reminder register :math:`rmd` always contains one qubit less.
            num_quotient_qubits: Size of the register :math:`qt` in which the quotient is stored.
                Determines the maximum dividend storable without resulting in a quotient overflow.
            name: The name of the circuit object.
        Raises:
            ValueError: If ``constant`` is lower than 1.
            ValueError: If ``num_quotient_qubits`` is lower than 1.
            ValueError: If ``num_constant_qubits`` is not big enough to hold the negated constant.
        """
        if constant < 1:
            raise ValueError(
                "The constant number to devide with must be at least 1.")

        if num_quotient_qubits < 1:
            raise ValueError(
                "The size of the quotient register must be at least 1.")

        min_num_cq = ceil(log2(constant + 1)) + 1
        if num_constant_qubits is None:
            num_constant_qubits = min_num_cq
        elif num_constant_qubits < min_num_cq:
            raise ValueError(
                f"The number of qubits must be at least {min_num_cq} to fit the negated constant.")

        highest_storable_dividend = 2**(num_constant_qubits +
                                        num_quotient_qubits - 2) - 1
        highest_no_overflow_dividend = constant * 2**num_quotient_qubits - 1
        if highest_storable_dividend > highest_no_overflow_dividend and not suppress_warning:
            print(f"Warning: All dividends greater than {highest_no_overflow_dividend} " +
                  "will result in nonsensical results due to overflow in the quotient register!")

        self._constant = constant
        self._num_constant_qubits = num_constant_qubits
        self._num_remainder_qubits = num_constant_qubits - 1
        self._num_quotient_qubits = num_quotient_qubits
        super().__init__(name=name)

        # build non-restoring division circuit
        qr_cnst = AncillaRegister(num_constant_qubits, "cnst")
        qr_rmd = QuantumRegister(self._num_remainder_qubits, "rmd")
        qr_qt = QuantumRegister(num_quotient_qubits, "qt")
        circuit = QuantumCircuit(qr_cnst, qr_rmd, qr_qt, name=name)
        self.add_register(qr_cnst, qr_rmd, qr_qt)
        division_qubits = qr_rmd[:] + qr_qt[:]

        sub = CnstHalfSub(constant, num_state_qubits=num_constant_qubits - 1, has_cout=True)
        circuit.compose(
            sub,
            qr_cnst[:sub.num_constant_qubits] + division_qubits[-num_constant_qubits:],
            inplace=True)

        if num_quotient_qubits == 2:
            # single constant-adder-subtractor
            for i in reversed(range(num_constant_qubits, len(division_qubits))):
                circuit.x(division_qubits[-1])
                circuit.compose(
                    CnstAddSub(
                        constant, num_state_qubits=num_constant_qubits, has_cout=False),
                    [division_qubits[-1]] + qr_cnst[:] + division_qubits[:-1], inplace=True)
        elif num_quotient_qubits > 2:
            # chain of constant-adder-subtractors
            circuit.x(division_qubits[-1])
            circuit.compose(
                CnstAddSub(
                    constant, num_state_qubits=num_constant_qubits, has_cout=False, chain="start"),
                [division_qubits[-1]] + qr_cnst[:] +
                division_qubits[-num_constant_qubits - 1 : -1],
                inplace=True)

            for i in reversed(range(num_constant_qubits + 1, len(division_qubits) - 1)):
                circuit.x(division_qubits[i])
                circuit.compose(
                    CnstAddSub(
                        constant, num_state_qubits=num_constant_qubits, has_cout=False, chain="inbetween"),
                    [division_qubits[i + 1]] + [division_qubits[i]] +
                    qr_cnst[:] + division_qubits[i - num_constant_qubits: i],
                    inplace=True)

            circuit.x(division_qubits[num_constant_qubits])
            circuit.compose(
                CnstAddSub(
                    constant, num_state_qubits=num_constant_qubits, has_cout=False, chain="end"),
                [division_qubits[num_constant_qubits + 1]] + [division_qubits[num_constant_qubits]] + 
                qr_cnst[:] + division_qubits[:num_constant_qubits],
                inplace=True)
            
            
        add_cnst = constant % 2**(num_constant_qubits - 1)
        if add_cnst != 0:
            ctrl_adder = CtrlCnstHalfAdd(
                    add_cnst, num_state_qubits=num_constant_qubits - 1, has_cout=False)
            circuit.compose(
                ctrl_adder,
                qr_cnst[:ctrl_adder.num_constant_qubits] + qr_rmd[:] + [qr_qt[0]],
                inplace=True)
        circuit.x(qr_qt[0])

        self.append(circuit.to_gate(), self.qubits)

    @property
    def num_constant_qubits(self) -> int:
        """The number of qubits in the constant register.
        Returns:
            The number of constant qubits.
        """
        return self._num_constant_qubits

    @property
    def num_remainder_qubits(self) -> int:
        """The number of qubits is the remainder register.
        Returns:
            The number of remainder qubits.
        """
        return self._num_remainder_qubits

    @property
    def num_quotient_qubits(self) -> int:
        """The number of qubits in the quotient register.
        Returns:
            The number of quotient qubits.
        """
        return self._num_quotient_qubits

    @property
    def constant(self) -> int:
        """The constant divisor.
        Returns:
            The constant.
        """
        return self._constant

    def highest_dividend(self) -> int:
        """The highest storable dividend not resulting in an 
        overflow due to the qoutient register size. It is related to
        the number of qubits available to store the negated dividend
        and number of qubits available to store the quotient (depending 
        on the constant).

        Returns:
            The dividend.
        """
        highest_storable = 2**(self._num_remainder_qubits +
                               self._num_quotient_qubits - 1) - 1
        highest_no_overflow = self._constant * 2**self._num_quotient_qubits - 1
        return min(highest_storable, highest_no_overflow)
