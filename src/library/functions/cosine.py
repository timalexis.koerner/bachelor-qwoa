"""Approximate calculation of cosine(2πx)."""

from typing import Optional
from qiskit.circuit import QuantumRegister, AncillaRegister, QuantumCircuit
from qiskit.circuit.library import HRSCumulativeMultiplier, MCXVChain
from ..adders import Incrementer, CtrlIncrementer, FullAdd

class Cosine2PI(QuantumCircuit):
    r"""Approximate calculation of cosine(2πx) with :math:`-1 + 16(x-0.5)^2 - 32(x-0.5)^4`.

    The implementation uses :math:`n` qubits for the input state :math:`|x\rangle`, 
    where the highest qubit of :math:`x` is assumed to be representing the value :math:`2^{-1}`,
    so :math:`x<1`. It creates a rough cosine approximation with :math:`4n-3` qubits
    in the signed integer (two's complement) representation. 
    The first part of the circuit starts with a case destinction. For the initial state
    :math:`|0\rangle^n` we have :math:`\cos(0)=cos_{aprx}(0)=1`. This case is handled later.
    For the cases :math:`0 < x < 0.5` (highest qubit is :math:`|0\rangle`, at least one other
    qubit is :math:`|1\rangle`) the value in the lower qubits :math:`x':=x \text{ mod } 0.5`
    gets negated, since :math:`0.5-x \Rightarrow 1 - x'`. This is done by performing
    :math:`x' \to x'':= \bar x' + 1` via NOT gates and an incrementer, controlled by the highest qubit.
    For the second part of the circuit, this :math:`n-1` qubit state gets copied with CNOT
    gates into the first qubits of the empty :math:`cos` output register. Now the state is
    multiplied with its own copy via an qiskit :math:`HRSCumulativeMultiplier(n-1, 2n-2)`,
    where the second argument is the number of product qubits. These product qubits now contain
    :math:`|x''^2\rangle` and are also stored as ancillas. The copied :math:`|x''\rangle`
    state is uncomputed with CNOT gates. This step is repeated one more time with
    :math:`2 \times (2n-2) \to 4n-4` qubits to create :math:`|x''^4\rangle`, this time directly
    in the :math:`cos` register. The factor :math:`32=2^5` only results in a shift.
    Because :math:`\forall 0< x <1: 32\cdot (x-0.5)^4 <2`, the highest qubit representing :math:`2^1`
    can be ommited in the multiplication, so :math:`HRSCumulativeMultiplier(2n-2, 4n-5)`
    is used. Together with a the omitted qubit and the sign qubit this puts the number of qubits
    in the :math:`cos` register at :math:`4n-3`. Now all but the second highest qubit :math:`2^0 = 1`
    of the :math:`cos` register is inverted with NOT gates and incremented, forming :math:`-1-32\cdot(x-0.5)^4`.
    The last part of the circuit is adding the :math:`|x''^2\rangle` state onto the highest qubits
    of the :math:`cos` register using :math:`FullAdd(2n-2)`. There is no carry-out required,
    the highest qubit reaches the sign qubit. In comparison to, the addition is shifted by
    the factor :math:`2^1` due to :math:`16\cdot(x-0.5)^2 \leq 4`.
    The :math:`cos` register now contains :math:`|11\rangle \otimes |0\rangle^{4n-5}` for :math:`x=0`
    because the inital state after first part is stil :math:`|0\rangle^{n}` so all multiplications
    resulted in :math:`0` and because of the :math:`cos` negation. This case is handled with multicontrol
    NOT gate, controlled by :math:`|x\rangle` and acting on the sign qubit, resulting in :math:`1`.

    Example circuit for num_state_qubits=2:
    .. parsed-literal::
            
                    ┌───┐┌─────────┐               ┌───────┐     ┌───┐                                       ┌───┐
          x_0: ─────┤ X ├┤1        ├──■────────────┤0      ├──■──┤ X ├────────────────────────────────────■──┤ X ├
               ┌───┐└─┬─┘│         │  │  ┌───┐┌───┐│       │  │  └───┘                                    │  ├───┤
          x_1: ┤ X ├──■──┤2 Ctrl+1 ├──┼──┤ X ├┤ X ├┤       ├──┼───────────────────────────────────────────■──┤ X ├
               └───┘     │         │┌─┴─┐└───┘└───┘│       │┌─┴─┐     ┌───────┐┌───┐     ┌─────┐          │  └───┘
        cos_0: ──────────┤0        ├┤ X ├──────────┤1      ├┤ X ├─────┤4      ├┤ X ├─────┤2    ├──────────┼───────
                         └─────────┘└───┘          │       │└───┘     │       │├───┤     │     │          │       
        cos_1: ────────────────────────────────────┤       ├──────────┤5      ├┤ X ├─────┤3    ├──────────┼───────
                                                   │       │          │       │├───┤     │     │          │       
        cos_2: ────────────────────────────────────┤  Mult ├──────────┤6      ├┤ X ├─────┤4    ├──────────┼───────
                                                   │       │          │       │└───┘     │     │┌──────┐  │       
        cos_3: ────────────────────────────────────┤       ├──────────┤       ├──────────┤5    ├┤3     ├──┼───────
                                                   │       │          │       │┌───┐     │     ││      │┌─┴─┐     
        cos_4: ────────────────────────────────────┤4      ├──────────┤7 Mult ├┤ X ├─────┤  +1 ├┤4     ├┤ X ├─────
                                                   │       │          │       │└───┘     │     ││      │└───┘     
        aux_0: ────────────────────────────────────┤2      ├──■───────┤0      ├──■───────┤     ├┤1     ├──────────
                                                   │       │  │       │       │  │       │     ││  Add │          
        aux_1: ────────────────────────────────────┤3      ├──┼────■──┤1      ├──┼────■──┤     ├┤2     ├──────────
                                                   └───────┘┌─┴─┐  │  │       │┌─┴─┐  │  │     ││      │          
        aux_2: ─────────────────────────────────────────────┤ X ├──┼──┤2      ├┤ X ├──┼──┤0    ├┤      ├──────────
                                                            └───┘┌─┴─┐│       │└───┘┌─┴─┐│     ││      │          
        aux_3: ──────────────────────────────────────────────────┤ X ├┤3      ├─────┤ X ├┤1    ├┤0     ├──────────
                                                                 └───┘└───────┘     └───┘└─────┘└──────┘          

    """

    def __init__(
        self, num_state_qubits: int, name: Optional[str] = "COS(2PIx)") -> None:
        r"""Approximate cosine calculation.
        
        Args:
            num_state_qubits: Number of state qubits, for a state with a binary
                representation less than 1, meaning the highest qubit represents :math:`2^{-1}`.
        Raises:
            ValueError: If ``num_state_qubits`` is smaller or equal to 1.
        """
        if num_state_qubits <= 1:
            raise ValueError("The number of state qubits must be greater than 1.")

        self._num_state_qubits = num_state_qubits

        super().__init__(name=name)

        # circuit blocks
        mcx = MCXVChain(num_state_qubits)
        incr = Incrementer(4*(num_state_qubits - 1) - 1)
        ctrl_incr = CtrlIncrementer(num_state_qubits - 1, has_cout=False)
        mult_1 = HRSCumulativeMultiplier(num_state_qubits - 1, name="Mult")
        mult_2 = HRSCumulativeMultiplier(2*(num_state_qubits - 1), num_result_qubits=4*(num_state_qubits - 1) - 1, name="Mult")
        full_add = FullAdd(2*(num_state_qubits - 1), has_cout=False, name="Add")

        num_ancillas = 6*(num_state_qubits - 1) - 2
        self._num_cosine_qubits = 4*(num_state_qubits - 1) + 1

        # circuit registers
        qr_x = QuantumRegister(num_state_qubits, "x")
        qr_cos = QuantumRegister(self._num_cosine_qubits, "cos")
        qr_aux = AncillaRegister(num_ancillas, "aux")
        circuit = QuantumCircuit(qr_x, qr_cos, qr_aux, name=name)
        self.add_register(qr_x, qr_cos, qr_aux)

        # negation for x >= 0.5
        circuit.x(qr_x[-1])
        circuit.cx(qr_x[-1], qr_x[:-1])
        circuit.append(ctrl_incr.to_gate(), 
                       qr_cos[:num_state_qubits - 1] + qr_x[:])
        circuit.x(qr_x[-1])


        # x_qubits contain 2^2 abs(x-0.5)
        x_qubits = qr_x[:-1]
        
        x_2_qubits = qr_aux[: 2*(num_state_qubits - 1)]
        x_2_cpy_qubits = qr_aux[2*(num_state_qubits - 1): 4*(num_state_qubits - 1)]
        
        # compute 2^4 abs(x-0.5)^2
        circuit.cx(x_qubits, qr_cos[:num_state_qubits - 1])
        circuit.append(mult_1.to_gate(),
                        x_qubits + qr_cos[:num_state_qubits - 1] + x_2_qubits + [qr_cos[-1]])
        circuit.cx(x_qubits, qr_cos[:num_state_qubits - 1])

        # compute 2^8 abs(x-0.5)^4 as 32 abs(x-0.5)^4 in qr_cos
        circuit.cx(x_2_qubits, x_2_cpy_qubits)
        circuit.append(mult_2.to_gate(),
                        x_2_qubits + x_2_cpy_qubits + qr_cos[:4*(num_state_qubits - 1)-1] + [qr_cos[-1]])
        circuit.cx(x_2_qubits, x_2_cpy_qubits)
        
        # compute - 1 - 32 abs(x-0.5)^4 in qr_cos
        circuit.x(qr_cos[:-2])
        circuit.x(qr_cos[-1:])
        circuit.append(incr.to_gate(), 
                       qr_aux[2*(num_state_qubits - 1):] + qr_cos[:-1])
        
        # add 2^4 abs(x-0.5)^2 as 16 abs(x-0.5)^2 to qr_cos
        circuit.append(full_add.to_gate(),
                        [qr_aux[-1]] +  x_2_qubits[:] + qr_cos[-2*(num_state_qubits - 1):])

        # special case x=0
        circuit.x(qr_x)
        circuit.append(mcx, qr_x[:] + [qr_cos[-1]] + qr_aux[3*(num_state_qubits - 1):][:num_state_qubits - 2])
        circuit.x(qr_x)

        self.append(circuit.to_gate(), self.qubits)

    @property
    def num_state_qubits(self) -> int:
        """Number of qubits for the state register.
        Returns:
            The number of state qubits.
        """
        return self._num_state_qubits

    @property
    def num_cosine_qubits(self) -> int:
        """Number of qubits for the cosine register.
        Returns:
            The number of cosine qubits.
        """
        return self._num_cosine_qubits
