"""Approximate calculation of eigenvalues of a symmetric, circulant matrix."""

from typing import Optional, List
from math import ceil, log2
from qiskit.circuit import QuantumRegister, AncillaRegister, QuantumCircuit
from ..divisors import CnstDivisor
from ..adders import FullAdd
from . import Cosine2PI

class SymCircEigenVal(QuantumCircuit):
    r"""Approximate calculation of eigenvalues of a symmetric, circulant matrix.

    To calculate the eigenvalues of :math:`A` with :math:`n_{EVAL}` qubits precision,
    the results of :math:`n_K = \lceil log_2 K\rceil, K := |\{k| a_k = 1\}|  = \mathcal{Poly}(n_{EVAL})`
    uses of :math:`COS2PI((n_{EVAL} - n_K + 3)/4)` for pre-calculated :math:`x` are accumulated.
    This means that only values :math:`n_{EVAL}: n_{EVAL} - n_K + 3 = 4 \cdot n'_{EVAL}`
    with :math:`n'_{EVAL} \in \mathbb{N}, n'_{EVAL} \geq 2` are allowed. With
    :math:`\lambda_j(A) = \sum_{0<k<\lfloor N/2\rfloor} 2 a_k\cdot \cos(2\pi jk/N)`,
    it follows  :math:`x(j) = j\cdot k/N`. Given a list of :math:`k` values, the
    implementation uses integer divisions with quotients :math:`Val(k) := \lfloor N/k \rfloor \geq 2`
    to approximate :math:`x(j)`. Each divison uses :math:`n'_{EVAL}` quotient qubits,
    with which the cosine approximation is calculated. The resulting approximation is
    added into the evaluation register of size :math:`n_{EVAL}` and uncomputed
    (except for the last :math:`k`). The largest reminder register contains
    :math:`n'_{rmd} := \max_{k \in K} n_{rmd}(k) = \max_{k \in K} 2^{\lceil \log_2(Val(k) + 1)\rceil}`
    qubits. Adding the number of quotient qubits, the required size for the state register
    is :math:`n'_{rmd} + n'_{EVAL}`. All divisions with less than :math:`n'_{rmd}`
    remainder qubits only act on the upper part of the state register. Because the cosine
    is calculated in the ancilla register, the number of ancillas required for the cosine
    approximation is qubits is :math:`4n'_{EVAL} - 3 + 6n'_{EVAL} -8`. The maximum ancilla
    number to contain the maximal negated :math:` \max_{k \in K} Val(k)` for the divisions
    is :math:`n'_{rmd}+1`.

    Example circuit for size=2, matrix_size=11, indices=[3,2]:
    .. parsed-literal::
            
                                                                                              ┌─────────────┐                           
        state_0: ─────────────────────────────────────────────────────────────────────────────┤4            ├───────────────────────────
                 ┌─────────────┐                                            ┌────────────────┐│             │                           
        state_1: ┤3            ├────────────────────────────────────────────┤3               ├┤5            ├───────────────────────────
                 │             │                                            │                ││             │                           
        state_2: ┤4            ├────────────────────────────────────────────┤4               ├┤6            ├───────────────────────────
                 │             │┌────────────┐             ┌───────────────┐│                ││             │┌────────────┐             
        state_3: ┤5            ├┤0           ├─────────────┤0              ├┤5               ├┤7            ├┤0           ├─────────────
                 │             ││            │             │               ││                ││             ││            │             
        state_4: ┤6            ├┤1           ├─────────────┤1              ├┤6               ├┤8            ├┤1           ├─────────────
                 │             ││            │┌───────────┐│               ││                ││             ││            │┌───────────┐
         eval_0: ┤             ├┤            ├┤6          ├┤               ├┤                ├┤             ├┤            ├┤6          ├
                 │             ││            ││           ││               ││                ││             ││            ││           │
         eval_1: ┤             ├┤            ├┤7          ├┤               ├┤                ├┤             ├┤            ├┤7          ├
                 │             ││            ││           ││               ││                ││             ││            ││           │
         eval_2: ┤  Div(3//11) ├┤            ├┤8          ├┤               ├┤  Div(3//11)_dg ├┤  Div(2//11) ├┤            ├┤8          ├
                 │             ││            ││           ││               ││                ││             ││            ││           │
         eval_3: ┤             ├┤            ├┤9          ├┤               ├┤                ├┤             ├┤            ├┤9          ├
                 │             ││            ││           ││               ││                ││             ││            ││           │
         eval_4: ┤             ├┤            ├┤10         ├┤               ├┤                ├┤             ├┤            ├┤10         ├
                 │             ││            ││           ││               ││                ││             ││            ││           │
         eval_5: ┤             ├┤            ├┤           ├┤               ├┤                ├┤             ├┤            ├┤11         ├
                 │             ││            ││           ││               ││                ││             ││            ││           │
          aux_0: ┤0            ├┤2  COS(2πx) ├┤1          ├┤2  COS(2πx)_dg ├┤0               ├┤0            ├┤2  COS(2πx) ├┤1          ├
                 │             ││            ││           ││               ││                ││             ││            ││           │
          aux_1: ┤1            ├┤3           ├┤2  FullAdd ├┤3              ├┤1               ├┤1            ├┤3           ├┤2  FullAdd ├
                 │             ││            ││           ││               ││                ││             ││            ││           │
          aux_2: ┤2            ├┤4           ├┤3          ├┤4              ├┤2               ├┤2            ├┤4           ├┤3          ├
                 └─────────────┘│            ││           ││               │└────────────────┘│             ││            ││           │
          aux_3: ───────────────┤5           ├┤4          ├┤5              ├──────────────────┤3            ├┤5           ├┤4          ├
                                │            ││           ││               │                  └─────────────┘│            ││           │
          aux_4: ───────────────┤6           ├┤5          ├┤6              ├─────────────────────────────────┤6           ├┤5          ├
                                │            ││           ││               │                                 │            ││           │
          aux_5: ───────────────┤7           ├┤           ├┤7              ├─────────────────────────────────┤7           ├┤           ├
                                │            ││           ││               │                                 │            ││           │
          aux_6: ───────────────┤8           ├┤           ├┤8              ├─────────────────────────────────┤8           ├┤           ├
                                │            ││           ││               │                                 │            ││           │
          aux_7: ───────────────┤9           ├┤           ├┤9              ├─────────────────────────────────┤9           ├┤           ├
                                │            ││           ││               │                                 │            ││           │
          aux_8: ───────────────┤10          ├┤0          ├┤10             ├─────────────────────────────────┤10          ├┤0          ├
                                └────────────┘└───────────┘└───────────────┘                                 └────────────┘└───────────┘

    """

    def __init__(
        self, size: int, matrix_size: int, indices: List[int], name: Optional[str] = "SymCircEV") -> None:
        r"""Approximate calculation of eigenvalues of a symmetric, circulant matrix.
        
        Args:
            size: Related to the number of eigenvalue qubits with
                ``num_eval_qubits`` = 4``size```- 3 + ceil(log2(K))
                for K indices.
            matrix_size: Size of the circulant, symmetric matrix.
            indices: List of indices in the first row or column of the matrix,
                where the matrix contains 1.
        Raises:
            ValueError: If ``size`` is smaller or equal to 1.
            ValueError: If ``indices`` is empty.
            ValueError: If ``indices`` contain an index equal to 0 or equal 
                to or greater than ``matrix_size`` devided by 2.
        """
        if size <= 1:
            raise ValueError("Size must be at least 2.")
        if len(indices) == 0:
            raise ValueError("No indices given.")
        if min(indices) == 0 or max(indices) >= matrix_size/2:
            raise ValueError(
                "The indices must be greater than 0 and smaller than the matrix size devided by 2.")
        
        num_higher_eval_qubits = ceil(log2(len(indices)))
        self._num_eval_qubits = 4*size - 3 + num_higher_eval_qubits       
        num_quotient_qubits = int((self._num_eval_qubits - num_higher_eval_qubits + 3)/4) 



        # circuit blocks
        divs = [CnstDivisor(int(matrix_size / k), num_quotient_qubits=num_quotient_qubits, name=f"Div({k}//{matrix_size})") for k in indices]
        num_ancillas = max(divs, key=lambda x:x.num_constant_qubits).num_constant_qubits
        self._num_state_qubits = num_ancillas - 1 + num_quotient_qubits
        cos2pi = Cosine2PI(num_quotient_qubits)
        num_ancillas = max(num_ancillas, cos2pi.num_ancillas + cos2pi.num_cosine_qubits)
        full_adds = []
        for idx, _ in enumerate(indices):
            if idx == 0:
                full_adds.append(FullAdd(cos2pi.num_cosine_qubits, has_cout=False))
            else:
                num_high_qubits = ceil(log2(idx + 1))
                full_adds.append(
                    FullAdd(cos2pi.num_cosine_qubits + num_high_qubits - 1, has_cout=True))

        super().__init__(name=name)
        
        # circuit registers
        qr_state = QuantumRegister(self._num_state_qubits, "state")
        qr_eval = QuantumRegister(self._num_eval_qubits, "eval")
        qr_aux = AncillaRegister(num_ancillas, "aux")
        circuit = QuantumCircuit(qr_state, qr_eval, qr_aux, name=name)
        self.add_register(qr_state, qr_eval, qr_aux)

        for i in range(len(indices)):
            if i > 0:
                # uncomputation
                circuit.append(cos2pi.inverse().to_gate(), 
                               qr_state[-div.num_quotient_qubits:] + qr_aux[:cos2pi.num_cosine_qubits + cos2pi.num_ancillas])
                circuit.append(div.inverse().to_gate(), 
                               qr_aux[:div.num_constant_qubits] + qr_state[-num_used_state_qubits:])
                
            # division
            div = divs[i]
            num_used_state_qubits = div.num_quotient_qubits + div.num_remainder_qubits
            circuit.append(div.to_gate(), 
                           qr_aux[:div.num_constant_qubits] + qr_state[-num_used_state_qubits:])
            # cosine approximation
            circuit.append(cos2pi.to_gate(), 
                           qr_state[-div.num_quotient_qubits:] + qr_aux[:cos2pi.num_cosine_qubits + cos2pi.num_ancillas])
            # adder
            full_add = full_adds[i]
            if i == 0:
                circuit.append(full_add.to_gate(),
                               [qr_aux[-1]] + qr_aux[:cos2pi.num_cosine_qubits] + qr_eval[:cos2pi.num_cosine_qubits])
            else:
                ancilla_qubits = qr_aux[-(full_add.num_state_qubits - cos2pi.num_cosine_qubits)-1:-1]
                circuit.append(full_add.to_gate(),
                                [qr_aux[-1]] + qr_aux[:cos2pi.num_cosine_qubits] + ancilla_qubits + qr_eval[:full_add.num_state_qubits + 1])

        self.append(circuit.to_gate(), self.qubits)

    @property
    def num_state_qubits(self) -> int:
        """Number of state qubits.
        Returns:
            The number of state qubits.
        """
        return self._num_state_qubits

    @property
    def num_eval_qubits(self) -> int:
        """Number of eigenvalue qubits.
        Returns:
            The number of eigenvalue qubits.
        """
        return self._num_eval_qubits
