"""Compute the lehmer code of a permutation."""

from typing import Optional
from math import log2, ceil
from qiskit.circuit import QuantumRegister, AncillaRegister, QuantumCircuit
from ..comparators import Comparator
from ..adders import CtrlIncrementer


class PermToLehmerCode(QuantumCircuit):
    r"""Out-of-place lehmer code calculation of a Permutation.
    
    The Lehmer code for a permutation on :math:`[n], \pi = \pi_0 \cdots \pi_{n-1}`
    is given by :math:`\pi \to d = (d_0 \cdots d_{n-1})` with
    :math:`d_i = \vert \{ j > i: \pi_j < \pi_i\}\vert`.
    The digits are bounded with :math:`0 \leq d_i < n-i`.
    The circuit implementation uses the :math:`D(i)` subcircuits for each digit
    proposed by Marsh and Wang in [1].

    Example circuit for num_states=3:
    .. parsed-literal::

                  ┌───────┐         
        perm_0_0: ┤0      ├─────────
                  │       │         
        perm_0_1: ┤1      ├─────────
                  │       │┌───────┐
        perm_1_0: ┤2      ├┤0      ├
                  │       ││       │
        perm_1_1: ┤3      ├┤1      ├
                  │       ││       │
        perm_2_0: ┤4      ├┤2      ├
                  │       ││       │
        perm_2_1: ┤5 D(0) ├┤3      ├
                  │       ││       │
       digit_0_0: ┤7      ├┤  D(1) ├
                  │       ││       │
       digit_0_1: ┤8      ├┤       ├
                  │       ││       │
         digit_1: ┤       ├┤5      ├
                  │       ││       │
             cmp: ┤6      ├┤4      ├
                  │       ││       │
             aux: ┤9      ├┤6      ├
                  └───────┘└───────┘

    **References:**

    [1] Marsh and Wang, Combinatorial optimisation via highly efficient quantum walks, 2019.
    `arXiv:1912.07353 <https://arxiv.org/abs/1912.07353>`_

    """

    def __init__(
        self, num_states: int, name: Optional[str] = "PtoLC") -> None:
        r"""Lehmer code computation.
        
        Args:
            num_states: Number of permuted states.
            name: The name of the circuit object.
        Raises:
            ValueError: If ``num_states`` is smaller or equal to 1.
        """
        if num_states <= 1:
            raise ValueError(
                "The number of permuted states must be greater than 1.")
        
        self._num_states = num_states
        self._num_state_qubits = ceil(log2(num_states))
        self._num_lehmer_digit_qubit_list = []
        for i in range(num_states - 1):
            self._num_lehmer_digit_qubit_list.append(ceil(log2(num_states - i)))
        self._num_permutation_qubits = num_states * self._num_state_qubits
        
        super().__init__(name=name)

        num_ancillas = 0
        lehmer_digits = []
        for digit_index in range(self._num_states - 1):
            lehmer_digit = self._lehmer_digit(digit_index)
            lehmer_digits.append(lehmer_digit)
            
            num_ancillas = max(num_ancillas, lehmer_digit.num_ancillas)
        
        # registers
        qr_perm_states = []
        for i in range(self.num_states):
            qr_perm_states.append(QuantumRegister(self.num_state_qubits, f"perm_{i}"))
        qr_digits = []
        for i, num_qubits in enumerate(self.num_lehmer_digit_qubit_list):
            qr_digits.append(QuantumRegister(num_qubits, f"digit_{i}"))
        qr_cmp = AncillaRegister(1, "cmp")
        qr_aux = AncillaRegister(num_ancillas, "aux")
        
        def _perm_state_qubits(min_index):
            nonlocal qr_perm_states
            return [qubit for qreg in qr_perm_states[min_index:] for qubit in qreg]
        
        # circuit
        circuit = QuantumCircuit(*qr_perm_states, *qr_digits, qr_cmp, qr_aux, name=name)
        self.add_register(*qr_perm_states, *qr_digits, qr_cmp, qr_aux)
        
        # compute lehmer digits
        for digit_index in range(num_states - 1):
            lehmer_digit = lehmer_digits[digit_index]
            circuit.append(lehmer_digit.to_gate(),
                           _perm_state_qubits(digit_index) + [qr_cmp[0]] + qr_digits[digit_index][:] +
                           qr_aux[:lehmer_digit.num_ancillas])
        
        self.append(circuit.to_gate(), self.qubits)


    @property
    def num_states(self) -> int:
        """The number of permuted states.
        Returns:
            The number of states.
        """
        return self._num_states
    
    @property
    def num_state_qubits(self) -> int:
        """Number of qubits for each permutation register.
        Returns:
            The number of state qubits.
        """
        return self._num_state_qubits
    
    @property
    def num_permutation_qubits(self) -> int:
        """Overall number of qubits for the permutation.
        Returns:
            The number of permutation qubits.
        """
        return self._num_permutation_qubits
    
    @property
    def num_lehmer_digit_qubit_list(self) -> list:
        """Number of qubits to store each Lehmer digit.
        Returns:
            The number of Lehmer digit qubits in a list.
        """
        return self._num_lehmer_digit_qubit_list

    def _lehmer_digit(self, digit_index: int) -> QuantumCircuit:
        """Computes the Lehmer digit at given index for the permutation.
        
        Compares all lower permutation states against the permutation state
        at the given index and accumulates the digit with controlled incrementers.
        the circuit is split for parellization.

        Args:
            digit_index (int): Index of the Lehmer digit.
        Returns:
            QuantumCircuit: The circuit.
        """
        # circuit blocks
        num_ancillas = 1
        cmp_incrementers = []
        for max_digit in range(self.num_states - digit_index - 1):
            cmp_incrementer = self._compartor_incrementer(
                self.num_state_qubits, self.num_state_qubits, max_digit, geq=False)
            cmp_incrementers.append(cmp_incrementer)
            num_ancillas = max(num_ancillas, cmp_incrementer.num_ancillas)
        
        # registers
        qr_perm_states = []
        for i in range(digit_index, self.num_states):
            qr_perm_states.append(QuantumRegister(self.num_state_qubits, f"perm_{i}"))
            
        qr_cmp = QuantumRegister(1, "cmp")
        qr_digit = QuantumRegister(self.num_lehmer_digit_qubit_list[digit_index], "digit")
        qr_aux = AncillaRegister(num_ancillas, "aux")
        
        # circuit
        circuit = QuantumCircuit(*qr_perm_states, qr_cmp, qr_digit, qr_aux, name=f"D({digit_index})")
        
        for i, cmp_incrementer in enumerate(cmp_incrementers):
            num_digit_qubits = cmp_incrementer.num_qubits - 2 * self.num_state_qubits - cmp_incrementer.num_ancillas - 1
            circuit.append(cmp_incrementer.to_gate(),
                           qr_perm_states[i + 1][:] + qr_perm_states[0][:] + [qr_cmp[0]] +
                           qr_digit[:num_digit_qubits] + qr_aux[:cmp_incrementer.num_ancillas])
        
        return circuit

    def _compartor_incrementer(self, num_a_qubits: int, num_b_qubits: int,
                               max_counter: int, geq: Optional[bool] = True) -> QuantumCircuit:
        """Compares two registers (a and b) and increments a counter accordingly.

        Args:
            num_a_qubits (int): Size of the a register.
            num_b_qubits (int): Size of the b register.
            max_counter (int): Maximum value in the counter before the possible increment.
            geq (bool): If the comparison is a >= b, otherwise a < b.

        Returns:
            QuantumCircuit: The circuit.
        """
        # circuit blocks
        num_cmp_qubits = max(num_a_qubits, num_b_qubits)
        cmp = Comparator(num_cmp_qubits, geq=geq)
        num_ancillas = cmp.num_ancillas + num_cmp_qubits - min(num_a_qubits, num_b_qubits)
        if max_counter == 0:
            # cmp is the incrementer, compare qubit is digit qubit
            num_counter_qubits = 1
        else:
            # ctrl incrementers num_state_qubits depends on the current maximal digit
            # carry-out only needed if next maximal digits needs more qubits
            num_ctrl_incr_state_qubits = ceil(log2(max_counter + 1))
            ctrl_incrementer = CtrlIncrementer(
                num_ctrl_incr_state_qubits, has_cout=num_ctrl_incr_state_qubits != ceil(log2(max_counter + 2)))
            num_ancillas = max(num_ancillas, ctrl_incrementer.num_ancillas)
            num_counter_qubits = ctrl_incrementer.num_qubits - ctrl_incrementer.num_ancillas - 1
        
        qr_a = QuantumRegister(num_a_qubits, "a")
        qr_b = QuantumRegister(num_b_qubits, "b")
        qr_cmp = QuantumRegister(1, "cmp")
        qr_cnt = QuantumRegister(num_counter_qubits, "cnt")
        qr_aux = AncillaRegister(num_ancillas, "aux")
        
        # circuit
        circuit = QuantumCircuit(qr_a, qr_b, qr_cmp, qr_cnt, qr_aux, name=cmp.name + ":+1")
        
        a_qubits = qr_a[:]
        b_qubits = qr_b[:]
        if num_a_qubits < num_b_qubits:
            a_qubits += qr_aux[num_a_qubits-num_b_qubits:]
        elif num_b_qubits < num_a_qubits:
            b_qubits += qr_aux[num_b_qubits-num_a_qubits:]
        
        if max_counter == 0:
            circuit.append(cmp.to_gate(),
                           qr_aux[:cmp.num_ancillas] + a_qubits[:] + b_qubits[:] + [qr_cnt[0]])
        else:
            circuit.append(cmp.to_gate(),
                           qr_aux[:cmp.num_ancillas] + a_qubits[:] + b_qubits[:] + [qr_cmp[0]])
            circuit.append(ctrl_incrementer.to_gate(),
                           qr_aux[:ctrl_incrementer.num_ancillas] + qr_cnt[:] + [qr_cmp[0]])
            circuit.append(cmp.to_gate(),
                           qr_aux[:cmp.num_ancillas] + a_qubits[:] + b_qubits[:] + [qr_cmp[0]])
        
        return circuit