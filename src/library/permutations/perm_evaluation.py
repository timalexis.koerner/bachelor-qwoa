"""Evaluation of a permutation."""

from typing import Optional
from math import log2, ceil
import numpy
from qiskit.circuit import QuantumRegister, AncillaRegister, QuantumCircuit
from ..comparators import PermPairComparator
from ..adders import CtrlCnstHalfAdd


class PermEvaluation(QuantumCircuit):
    r"""Evaluation of a permutation.

    Given a real :math:`n \times n` matrix :math:`Q` (zero based indexed)
    consisting of entries describing a cost
    :math:`Q_{ij}\geq 0` with :math:`\forall i: Q_{ii}=0`,
    a permutation over :math:`n` states :math:`\pi=\pi_0\dots\pi_{n-1}`
    is evaluated as
    :math:`Val_{\bar Q}(\pi)=\sum_{k < n} \bar Q_{\pi_k \pi_{k+1 \text{ mod } n}}`.
    :math:`\bar Q` is an adjusted version of :math:`Q`, so that
    :math:`\bar Q_{i,j} \in \mathbb{N}` and :math:`\max_\pi (Val_{\bar Q}(\pi)) < 2^q`
    for some :math:`q \in \mathbb{N^+}`. Using the sum of maximal entries column-
    and row-wise :math:`MAXSUM_{col}(Q) = \sum_{j<n} \max_i Q_{ij}` and
    :math:`MAXSUM_{row}(Q) = \sum_{i<n} \max_j Q_{ij}`, one representation
    of the adjusted matrix can be computed with
    :math:`\bar Q_{ij} = \left\lfloor \frac{Q_{ij} \cdot (2^q-1)}{\min(MAXSUM_{col}(Q), MAXSUM_{row}(Q))}\right\rfloor`.
    Because the states in the permutation are unique, :math:`Val_{Q}(\pi)`
    sums up entries which are column- and row-wise unique. This means
    :math:`\min(MAXSUM_{col}(Q), MAXSUM_{row}(Q)) \geq \max_\pi Val_{Q}(\pi)`
    and the evalution is bounded
    :math:`Val_{\bar Q}(\pi)=\sum_{k < n} \bar Q_{\pi_k \pi_{k+1 \text{ mod } n}} \leq \sum_{k < n} \frac{Q_{\pi_k \pi_{k+1 \text{ mod } n}} \cdot (2^q-1)}{\min(MAXSUM_{col}(Q), MAXSUM_{row}(Q))} \leq \frac{Val_{Q}(\pi)\cdot (2^q - 1)}{\max_\pi Val_{Q}(\pi)} < 2^q`
    as required. In the quantum circuit the hermitian cost operator
    :math:`\hat C` is represented by a
    :math:`n\lceil\log_2 n\rceil \times n\lceil\log_2 n\rceil` matrix :math:`C`
    (zero based indexed) with
    :math:`\forall \pi, \sum_{i<n} \pi_i \cdot \lceil \log_2 n\rceil^i = \Pi: C_{\Pi\Pi} = Val_{\bar Q}(\pi)`.
    In order to compute :math:`Val_{\bar Q}(\pi)` for a permutation
    :math:`|\Pi\rangle = \bigotimes_{i<n} |\pi_i\rangle`, adjacend permutation states
    are checked for all possible value pairs :math:`(i, j)`. If the pair is contained
    in the permuation, :math:`\bar Q_{ij}` is added to a sum in another register.
    Value pairs :math:`(i,j): \bar Q_{ij}=0` can be skipped. The resulting sum is
    :math:`\sum_{i,j,k<n \wedge (i,j) = (\pi_k, \pi_{k+1 \text{ mod } n})} \bar Q_{ij} = Val_{\bar Q}(\pi)`.

    Using the target of the previous comparators with :math:`(i,j)` as the control,
    the values :math:`\bar Q_{ij}` are added using a CtrlCnstHalfAdd in a register of size q.
    Afterwards the control is uncomputed and the next value pair is compared. After
    :math:`n^2 - n` iterations (or :math:`\frac{n^2 - n}{2}` iterations for symmetric
    :math:`\bar Q`) all value pairs are checked. The complete circuit computes
    :math:`EVAL |\Pi\rangle  \otimes |0\rangle = |\Pi\rangle \otimes |Val_{\bar Q}(\pi)\rangle`.

    Example (unsymmetric) circuit for num_states=3, num_eval_qubits=6, 
        Q=numpy.array([0, 19.2, 20.5], [10, 0, 3.23],  [2.4, 12, 0]]):
    .. parsed-literal::

                  ┌───────────────────┐              ┌──────────────────────┐┌───────────────────┐              ┌──────────────────────┐┌───────────────────┐              ┌──────────────────────┐┌───────────────────┐             ┌──────────────────────┐┌───────────────────┐              ┌──────────────────────┐┌───────────────────┐               ┌──────────────────────┐
        perm_0_0: ┤0                  ├──────────────┤0                     ├┤0                  ├──────────────┤0                     ├┤0                  ├──────────────┤0                     ├┤0                  ├─────────────┤0                     ├┤0                  ├──────────────┤0                     ├┤0                  ├───────────────┤0                     ├
                  │                   │              │                      ││                   │              │                      ││                   │              │                      ││                   │             │                      ││                   │              │                      ││                   │               │                      │
        perm_0_1: ┤1                  ├──────────────┤1                     ├┤1                  ├──────────────┤1                     ├┤1                  ├──────────────┤1                     ├┤1                  ├─────────────┤1                     ├┤1                  ├──────────────┤1                     ├┤1                  ├───────────────┤1                     ├
                  │                   │              │                      ││                   │              │                      ││                   │              │                      ││                   │             │                      ││                   │              │                      ││                   │               │                      │
        perm_1_0: ┤2                  ├──────────────┤2                     ├┤2                  ├──────────────┤2                     ├┤2                  ├──────────────┤2                     ├┤2                  ├─────────────┤2                     ├┤2                  ├──────────────┤2                     ├┤2                  ├───────────────┤2                     ├
                  │                   │              │                      ││                   │              │                      ││                   │              │                      ││                   │             │                      ││                   │              │                      ││                   │               │                      │
        perm_1_1: ┤3                  ├──────────────┤3                     ├┤3                  ├──────────────┤3                     ├┤3                  ├──────────────┤3                     ├┤3                  ├─────────────┤3                     ├┤3                  ├──────────────┤3                     ├┤3                  ├───────────────┤3                     ├
                  │                   │              │                      ││                   │              │                      ││                   │              │                      ││                   │             │                      ││                   │              │                      ││                   │               │                      │
        perm_2_0: ┤4                  ├──────────────┤4                     ├┤4                  ├──────────────┤4                     ├┤4                  ├──────────────┤4                     ├┤4                  ├─────────────┤4                     ├┤4                  ├──────────────┤4                     ├┤4                  ├───────────────┤4                     ├
                  │                   │              │                      ││                   │              │                      ││                   │              │                      ││                   │             │                      ││                   │              │                      ││                   │               │                      │
        perm_2_1: ┤5                  ├──────────────┤5                     ├┤5                  ├──────────────┤5                     ├┤5                  ├──────────────┤5                     ├┤5                  ├─────────────┤5                     ├┤5                  ├──────────────┤5                     ├┤5                  ├───────────────┤5                     ├
                  │                   │┌────────────┐│                      ││                   │┌────────────┐│                      ││                   │┌────────────┐│                      ││                   │┌───────────┐│                      ││                   │┌────────────┐│                      ││                   │┌─────────────┐│                      │
          eval_0: ┤                   ├┤2           ├┤                      ├┤                   ├┤3           ├┤                      ├┤                   ├┤3           ├┤                      ├┤                   ├┤3          ├┤                      ├┤                   ├┤5           ├┤                      ├┤                   ├┤4            ├┤                      ├
                  │                   ││            ││                      ││                   ││            ││                      ││                   ││            ││                      ││                   ││           ││                      ││                   ││            ││                      ││                   ││             ││                      │
          eval_1: ┤  PairCmp(=(0, 1)) ├┤3           ├┤  PairCmp(=(0, 1))_dg ├┤  PairCmp(=(0, 2)) ├┤4           ├┤  PairCmp(=(0, 2))_dg ├┤  PairCmp(=(1, 0)) ├┤4           ├┤  PairCmp(=(1, 0))_dg ├┤  PairCmp(=(1, 2)) ├┤4          ├┤  PairCmp(=(1, 2))_dg ├┤  PairCmp(=(2, 0)) ├┤6           ├┤  PairCmp(=(2, 0))_dg ├┤  PairCmp(=(2, 1)) ├┤5            ├┤  PairCmp(=(2, 1))_dg ├
                  │                   ││            ││                      ││                   ││            ││                      ││                   ││            ││                      ││                   ││           ││                      ││                   ││            ││                      ││                   ││             ││                      │
          eval_2: ┤                   ├┤4           ├┤                      ├┤                   ├┤5           ├┤                      ├┤                   ├┤5           ├┤                      ├┤                   ├┤5          ├┤                      ├┤                   ├┤7           ├┤                      ├┤                   ├┤6            ├┤                      ├
                  │                   ││            ││                      ││                   ││            ││                      ││                   ││            ││                      ││                   ││           ││                      ││                   ││            ││                      ││                   ││             ││                      │
          eval_3: ┤                   ├┤5           ├┤                      ├┤                   ├┤6           ├┤                      ├┤                   ├┤6           ├┤                      ├┤                   ├┤6          ├┤                      ├┤                   ├┤8           ├┤                      ├┤                   ├┤7            ├┤                      ├
                  │                   ││            ││                      ││                   ││            ││                      ││                   ││            ││                      ││                   ││           ││                      ││                   ││            ││                      ││                   ││             ││                      │
          eval_4: ┤                   ├┤6 Ctrl(+28) ├┤                      ├┤                   ├┤7           ├┤                      ├┤                   ├┤7           ├┤                      ├┤                   ├┤7          ├┤                      ├┤                   ├┤9           ├┤                      ├┤                   ├┤8            ├┤                      ├
                  │                   ││            ││                      ││                   ││  Ctrl(+30) ││                      ││                   ││  Ctrl(+14) ││                      ││                   ││  Ctrl(+4) ││                      ││                   ││            ││                      ││                   ││             ││                      │
          eval_5: ┤                   ├┤            ├┤                      ├┤                   ├┤8           ├┤                      ├┤                   ├┤8           ├┤                      ├┤                   ├┤8          ├┤                      ├┤                   ├┤10          ├┤                      ├┤                   ├┤9  Ctrl(+17) ├┤                      ├
                  │                   ││            ││                      ││                   ││            ││                      ││                   ││            ││                      ││                   ││           ││                      ││                   ││   Ctrl(+3) ││                      ││                   ││             ││                      │
            ctrl: ┤6                  ├┤7           ├┤6                     ├┤6                  ├┤9           ├┤6                     ├┤6                  ├┤9           ├┤6                     ├┤6                  ├┤9          ├┤6                     ├┤6                  ├┤11          ├┤6                     ├┤6                  ├┤10           ├┤6                     ├
                  │                   ││            ││                      ││                   ││            ││                      ││                   ││            ││                      ││                   ││           ││                      ││                   ││            ││                      ││                   ││             ││                      │
           aux_0: ┤7                  ├┤0           ├┤7                     ├┤7                  ├┤0           ├┤7                     ├┤7                  ├┤0           ├┤7                     ├┤7                  ├┤0          ├┤7                     ├┤7                  ├┤0           ├┤7                     ├┤7                  ├┤0            ├┤7                     ├
                  │                   ││            ││                      ││                   ││            ││                      ││                   ││            ││                      ││                   ││           ││                      ││                   ││            ││                      ││                   ││             ││                      │
           aux_1: ┤8                  ├┤1           ├┤8                     ├┤8                  ├┤1           ├┤8                     ├┤8                  ├┤1           ├┤8                     ├┤8                  ├┤1          ├┤8                     ├┤8                  ├┤1           ├┤8                     ├┤8                  ├┤1            ├┤8                     ├
                  └───────────────────┘└────────────┘└──────────────────────┘└───────────────────┘│            │└──────────────────────┘└───────────────────┘│            │└──────────────────────┘└───────────────────┘│           │└──────────────────────┘└───────────────────┘│            │└──────────────────────┘└───────────────────┘│             │└──────────────────────┘
           aux_2: ────────────────────────────────────────────────────────────────────────────────┤2           ├─────────────────────────────────────────────┤2           ├─────────────────────────────────────────────┤2          ├─────────────────────────────────────────────┤2           ├─────────────────────────────────────────────┤2            ├────────────────────────
                                                                                                  └────────────┘                                             └────────────┘                                             └───────────┘                                             │            │                                             │             │                        
           aux_3: ────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────┤3           ├─────────────────────────────────────────────┤3            ├────────────────────────
                                                                                                                                                                                                                                                                                  │            │                                             └─────────────┘                        
           aux_4: ────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────┤4           ├────────────────────────────────────────────────────────────────────────────────────
                                                                                                                                                                                                                                                                                  └────────────┘                                                                                   

    Example (symmetric) circuit for num_states=3, num_eval_qubits=6, 
        Q=numpy.array([0, 10, 20], [10, 0, 3],  [20, 3, 0]]):
    .. parsed-literal::

                  ┌───────────────────┐              ┌──────────────────────┐┌───────────────────┐               ┌──────────────────────┐┌───────────────────┐              ┌──────────────────────┐
        perm_0_0: ┤0                  ├──────────────┤0                     ├┤0                  ├───────────────┤0                     ├┤0                  ├──────────────┤0                     ├
                  │                   │              │                      ││                   │               │                      ││                   │              │                      │
        perm_0_1: ┤1                  ├──────────────┤1                     ├┤1                  ├───────────────┤1                     ├┤1                  ├──────────────┤1                     ├
                  │                   │              │                      ││                   │               │                      ││                   │              │                      │
        perm_1_0: ┤2                  ├──────────────┤2                     ├┤2                  ├───────────────┤2                     ├┤2                  ├──────────────┤2                     ├
                  │                   │              │                      ││                   │               │                      ││                   │              │                      │
        perm_1_1: ┤3                  ├──────────────┤3                     ├┤3                  ├───────────────┤3                     ├┤3                  ├──────────────┤3                     ├
                  │                   │              │                      ││                   │               │                      ││                   │              │                      │
        perm_2_0: ┤4                  ├──────────────┤4                     ├┤4                  ├───────────────┤4                     ├┤4                  ├──────────────┤4                     ├
                  │                   │              │                      ││                   │               │                      ││                   │              │                      │
        perm_2_1: ┤5                  ├──────────────┤5                     ├┤5                  ├───────────────┤5                     ├┤5                  ├──────────────┤5                     ├
                  │                   │┌────────────┐│                      ││                   │┌─────────────┐│                      ││                   │┌────────────┐│                      │
          eval_0: ┤                   ├┤1           ├┤                      ├┤                   ├┤4            ├┤                      ├┤                   ├┤5           ├┤                      ├
                  │                   ││            ││                      ││                   ││             ││                      ││                   ││            ││                      │
          eval_1: ┤                   ├┤2           ├┤                      ├┤                   ├┤5            ├┤                      ├┤                   ├┤6           ├┤                      ├
                  │                   ││            ││                      ││                   ││             ││                      ││                   ││            ││                      │
          eval_2: ┤                   ├┤3           ├┤                      ├┤                   ├┤6            ├┤                      ├┤                   ├┤7           ├┤                      ├
                  │                   ││            ││                      ││                   ││             ││                      ││                   ││            ││                      │
          eval_3: ┤                   ├┤4           ├┤                      ├┤                   ├┤7            ├┤                      ├┤                   ├┤8           ├┤                      ├
                  │                   ││  Ctrl(+12) ││                      ││                   ││             ││                      ││                   ││            ││                      │
          eval_4: ┤  PairCmp(={0, 1}) ├┤            ├┤  PairCmp(={0, 1})_dg ├┤  PairCmp(={0, 2}) ├┤8            ├┤  PairCmp(={0, 2})_dg ├┤  PairCmp(={1, 2}) ├┤9           ├┤  PairCmp(={1, 2})_dg ├
                  │                   ││            ││                      ││                   ││             ││                      ││                   ││            ││                      │
          eval_5: ┤                   ├┤            ├┤                      ├┤                   ├┤9  Ctrl(+25) ├┤                      ├┤                   ├┤10          ├┤                      ├
                  │                   ││            ││                      ││                   ││             ││                      ││                   ││   Ctrl(+3) ││                      │
            ctrl: ┤9                  ├┤5           ├┤9                     ├┤9                  ├┤10           ├┤9                     ├┤9                  ├┤11          ├┤9                     ├
                  │                   ││            ││                      ││                   ││             ││                      ││                   ││            ││                      │
           aux_0: ┤                   ├┤0           ├┤                      ├┤                   ├┤0            ├┤                      ├┤                   ├┤0           ├┤                      ├
                  │                   │└────────────┘│                      ││                   ││             ││                      ││                   ││            ││                      │
           aux_1: ┤                   ├──────────────┤                      ├┤                   ├┤1            ├┤                      ├┤                   ├┤1           ├┤                      ├
                  │                   │              │                      ││                   ││             ││                      ││                   ││            ││                      │
           aux_2: ┤                   ├──────────────┤                      ├┤                   ├┤2            ├┤                      ├┤                   ├┤2           ├┤                      ├
                  │                   │              │                      ││                   ││             ││                      ││                   ││            ││                      │
           aux_3: ┤                   ├──────────────┤                      ├┤                   ├┤3            ├┤                      ├┤                   ├┤3           ├┤                      ├
                  │                   │              │                      ││                   │└─────────────┘│                      ││                   ││            ││                      │
           aux_4: ┤                   ├──────────────┤                      ├┤                   ├───────────────┤                      ├┤                   ├┤4           ├┤                      ├
                  │                   │              │                      ││                   │               │                      ││                   │└────────────┘│                      │
           aux_5: ┤6                  ├──────────────┤6                     ├┤6                  ├───────────────┤6                     ├┤6                  ├──────────────┤6                     ├
                  │                   │              │                      ││                   │               │                      ││                   │              │                      │
           aux_6: ┤7                  ├──────────────┤7                     ├┤7                  ├───────────────┤7                     ├┤7                  ├──────────────┤7                     ├
                  │                   │              │                      ││                   │               │                      ││                   │              │                      │
           aux_7: ┤8                  ├──────────────┤8                     ├┤8                  ├───────────────┤8                     ├┤8                  ├──────────────┤8                     ├
                  └───────────────────┘              └──────────────────────┘└───────────────────┘               └──────────────────────┘└───────────────────┘              └──────────────────────┘

    """

    def __init__(
        self, num_states: int, Q: numpy.array, num_eval_qubits: int,
        name: Optional[str] = "EVAL") -> None:
        r"""Evaluation of a permutation.
        
        Args:
            num_states: Number of permuted states.
            Q: Cost matrix, will be adjusted.
            num_eval_qubits: Number of qubits for the evaluation register, 
                i.e. the evalutation precision.
            name: The name of the circuit object.
        Raises:
            ValueError: If ``num_states`` is smaller or equal to 1.
            ValueError: If ``num_eval_qubits`` is smaller or equal to 1.
            ValueError: If ``Q`` is not a real, ``num_states`` x ``num_states``
                dimensional matrix with entries greater or equal to 0 and
                all diagonal entries 0. 
        """
        if num_states <= 1:
            raise ValueError("The number of permuted states must be greater than 1.")
        if num_eval_qubits <= 1:
            raise ValueError("The number of evaluation qubits must be greater than 1.")
        if Q.ndim != 2:
            raise ValueError("Dimension of cost matrix must be 2.")
        if numpy.any(Q.real != Q):
            raise ValueError("Cost matrix must be real.")
        if Q.shape[0] != num_states or Q.shape[1] != num_states:
            raise ValueError("Cost matrix dimensions must have number of states as size.")
        if numpy.any(Q < 0):
            raise ValueError("Cost matrix must have positive or zero entries.")
        if numpy.any(Q.diagonal() != 0):
            raise ValueError("Cost matrix must be zero on the diagonal.")
        
        self._num_states = num_states
        self._num_state_qubits = ceil(log2(num_states))
        self._num_eval_qubits = num_eval_qubits
        self._Q_adj = self._adjust_costs(Q)
        self._is_symmetric = numpy.all(self._Q_adj.T == self._Q_adj)
        
        super().__init__(name=name)

        # circuit blocks
        if self._is_symmetric:
            index_pairs = [(i, j) for i in range(num_states) for j in range(i + 1, num_states)]
        else:
            index_pairs = [(i, j) for i in range(num_states) for j in range(num_states) if i != j]
        
        # ctrl adders
        ctrl_adders = []
        max_perm_eval = 0
        for index_pair in index_pairs:
            summand = self._Q_adj[index_pair[0], index_pair[1]]
            if summand == 0:
                continue
                        
            num_max_perm_eval_qubits = min(ceil(log2(max_perm_eval + 1)), num_eval_qubits - 1)
            num_summand_qubits = ceil(log2(summand + 1))
            num_propagation_qubits = max(0, num_max_perm_eval_qubits - num_summand_qubits)
            ctrl_adders.append(
                CtrlCnstHalfAdd(summand, 
                                  num_state_qubits=num_summand_qubits,
                                  num_propagation_qubits=num_propagation_qubits,
                                  has_cout=max_perm_eval != 0,
                                  name=f"Ctrl(+{summand})"))
            max_perm_eval += summand
        num_ancillas = max(ctrl_adders, key=lambda x:x.num_ancillas).num_ancillas
        
        # check if ancillas need to be borrowed, all pair comparators have same amount of ancillas
        test_ppc = PermPairComparator(num_states, index_pairs[0],
                                      has_borrowed_ancillas=True,
                                      is_symmetric=self._is_symmetric)
        has_borrowed_ancillas = test_ppc.num_ancillas > num_ancillas
        num_ancillas = max(num_ancillas, test_ppc.num_ancillas - self._num_eval_qubits)
        
        # pair comparators
        perm_pair_cmps = []
        for index_pair in index_pairs:
            if self._Q_adj[index_pair[0], index_pair[1]] == 0:
                continue
            perm_pair_cmps.append(
                PermPairComparator(num_states, index_pair,
                                   has_borrowed_ancillas=has_borrowed_ancillas,
                                   is_symmetric=self._is_symmetric))
        
        if self._is_symmetric:
            # pre-compare register needs zeroed ancillas
            num_ancillas += num_states
        
        # circuit registers
        qr_perm_states = []
        for i in range(self.num_states):
            qr_perm_states.append(QuantumRegister(self.num_state_qubits, f"perm_{i}"))
        qr_eval = QuantumRegister(self.num_eval_qubits, "eval")
        qr_ctrl = AncillaRegister(1, "ctrl")
        qr_aux = AncillaRegister(num_ancillas, "aux")
        
        # circuit
        circuit = QuantumCircuit(*qr_perm_states, qr_eval, qr_ctrl, qr_aux, name=name)
        self.add_register(*qr_perm_states, qr_eval, qr_ctrl, qr_aux)

        perm_state_qubits = [q for qr_perm_state in qr_perm_states for q in qr_perm_state] 
        for idx, (perm_pair_cmp, ctrl_adder) in enumerate(zip(perm_pair_cmps, ctrl_adders)):
            # index compare
            if idx == 0:
                # before evaluation zeroed ancillas
                temp = perm_pair_cmp
                perm_pair_cmp = PermPairComparator(num_states, index_pairs[0],
                                                   has_borrowed_ancillas=False,
                                                   is_symmetric=self._is_symmetric)
            if self._is_symmetric:
                circuit.append(perm_pair_cmp.to_gate(),
                               perm_state_qubits[:] + qr_aux[-num_states:] + [qr_ctrl[0]] +\
                               (qr_aux[:-num_states] + qr_eval[:])[:perm_pair_cmp.num_ancillas])
            else:
                circuit.append(perm_pair_cmp.to_gate(),
                               perm_state_qubits[:] + [qr_ctrl[0]] +\
                               (qr_aux[:] + qr_eval[:])[:perm_pair_cmp.num_ancillas])
            
            # ctrl add
            num_sum_qubits = ctrl_adder.num_qubits - ctrl_adder.num_ancillas - 1
            circuit.append(ctrl_adder.to_gate(),
                           qr_aux[:ctrl_adder.num_ancillas] + qr_eval[:num_sum_qubits] + [qr_ctrl[0]])
        
            # uncompute index compare
            if idx == 0:
                # after evaluation
                perm_pair_cmp = temp
                
            if self._is_symmetric:
                circuit.append(perm_pair_cmp.inverse().to_gate(),
                               perm_state_qubits[:] + qr_aux[-num_states:] + [qr_ctrl[0]] +\
                               (qr_aux[:-num_states] + qr_eval[:])[:perm_pair_cmp.num_ancillas])
            else:
                circuit.append(perm_pair_cmp.inverse().to_gate(),
                               perm_state_qubits[:] + [qr_ctrl[0]] +\
                               (qr_aux[:] + qr_eval[:])[:perm_pair_cmp.num_ancillas])
        
        self.append(circuit.to_gate(), self.qubits)


    @property
    def num_states(self) -> int:
        """The number of permuted states.
        Returns:
            The number of states.
        """
        return self._num_states
    
    @property
    def num_state_qubits(self) -> int:
        """Number of qubits for each permutation register.
        Returns:
            The number of state qubits.
        """
        return self._num_state_qubits
    
    @property
    def num_permutation_qubits(self) -> int:
        """Overall number of qubits for the permutation.
        Returns:
            The number of permutation qubits.
        """
        return self._num_permutation_qubits
    
    @property
    def num_eval_qubits(self) -> int:
        """Number of qubits for the evaluation of the permutation.
        Returns:
            The number of evaluation qubits.
        """
        return self._num_eval_qubits
    
    @property
    def adjusted_costs(self) -> numpy.array:
        """Number of qubits to store each Lehmer digit.
        Returns:
            The number of Lehmer digit qubits in a list.
        """
        return self._Q_adj
    
    @property
    def is_symmetric(self) -> bool:
        """Checks if the adjusted cost matrix is symmetric.
        Returns:
            If adjusted cost matrix symmetric.
        """
        return self._is_symmetric
    
    def _adjust_costs(self, Q: numpy.array) -> numpy.array:
        """Adjust the cost matrix Q to fit the maximal permutation evaluation
        in ``num_eval_qubits`` and only allow integer values.
        Args:
            Q (numpy.array): Original cost matrix.
        Returns:
            numpy.array: Adjusted cost matrix.
        """    
        maxsum_col = sum(numpy.max(Q[:, i]) for i in range(Q.shape[0]))
        maxsum_row = sum(numpy.max(Q[i, :]) for i in range(Q.shape[0]))
        aprx_perm_max = min(maxsum_col, maxsum_row)
        
        Q_adj = numpy.empty(Q.shape, dtype=int)
        for i in range(Q.shape[0]):
            for j in range(Q.shape[1]):
                Q_adj[i,j] = int(Q[i,j] * (2**(self.num_eval_qubits) - 1) / aprx_perm_max)
        
        return Q_adj
