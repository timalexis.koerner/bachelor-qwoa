"""Compute an index of a permutation out-of-place."""

from typing import Optional, Tuple
from math import log2, ceil, factorial
from qiskit.circuit import QuantumRegister, AncillaRegister, QuantumCircuit
from ..comparators import Comparator
from ..adders import CtrlIncrementer, FullAdd
from ..multipliers import InPlaceCnstMult


class OOPPermIndex(QuantumCircuit):
    r"""Out-of-place index calculation of a Permutation.
    
    Using the circuit proposed by Marsh and Wang in [1], each permutation on
    :math:`[n], \pi = \pi_0 \cdots \pi_{n-1}` is given a unique index :math:`id(\pi)`.
    The index is based on the Lehmer code, where :math:`\pi \to d = (d_0 \cdots d_{n-1})`
    with :math:`d_i = \vert \{ j > i: \pi_j < \pi_i\}\vert`. Because
    :math:`0 \leq d_i < n-i`, the index can be expressed in the factorial number
    system using the mixed-radix form (see OEIS A007623)
    :math:`id(\pi) = \sum_{i<n} d_i(n-i)! = (\dots(d_0 \cdot (n-1) + d_1) \cdot (n-2) + \cdots + d_{n-2}) \cdot 1`.
    The circuit calculates each :math:`d_i` by comparing and counting via controlled
    incrementation according to the Lehmer code. The :math:`d_i`'s are calculated
    successively by subcircuits called :math:`D(i)`, starting from :math:`d_0`,
    and are interleaved with additions on the index register, in-place multiplication
    by :math:`(n-i)` and uncomputation. Using seperate additions to add the digits
    to the index register reduces the width of the digit Incrementers, using two
    alternating registers to store :math:`d` allows uncomputating :math:`d_i` and
    computating :math:`d_{i+1}` in parallel (only one register usage collision for
    :math:`i` even at :math:`\pi_{k}` with :math:`k=i + 1 + \frac{n-i}{2}`).
    
    In order to properly interleave the instructions in qiskit, the each subscircuit
    :math:`D(i)` is split into :math:`D(i)_0` and :math:`D(i)_1`

    Example circuit for num_states=3:
    .. parsed-literal::

                  ┌─────────┐┌─────────┐                                                                      
        perm_0_0: ┤0        ├┤0        ├──────────────────────────────────────────────────────────────────────
                  │         ││         │                                                                      
        perm_0_1: ┤1        ├┤1        ├──────────────────────────────────────────────────────────────────────
                  │         ││         │┌─────────┐            ┌─────────┐        ┌────────────┐┌────────────┐
        perm_1_0: ┤2        ├┤2        ├┤0        ├────────────┤0        ├────────┤0           ├┤0           ├
                  │         ││         ││         │            │         │        │            ││            │
        perm_1_1: ┤3        ├┤3        ├┤1        ├────────────┤1        ├────────┤1           ├┤1           ├
                  │         ││         ││         │            │         │        │            ││            │
        perm_2_0: ┤4        ├┤4        ├┤2        ├────────────┤2        ├────────┤2           ├┤2           ├
                  │         ││         ││         │            │         │        │            ││            │
        perm_2_1: ┤5        ├┤5        ├┤3        ├────────────┤3        ├────────┤3           ├┤3           ├
                  │         ││         ││         │┌──────────┐│         │┌──────┐│            ││            │
         index_0: ┤7        ├┤7        ├┤         ├┤0         ├┤         ├┤4     ├┤            ├┤            ├
                  │  D(0)_0 ││  D(0)_1 ││         ││          ││         ││      ││            ││            │
         index_1: ┤8        ├┤8        ├┤         ├┤1 Mult(2) ├┤         ├┤5     ├┤            ├┤            ├
                  │         ││         ││  D(1)_0 ││          ││  D(1)_1 ││      ││            ││            │
         index_2: ┤         ├┤         ├┤         ├┤2         ├┤         ├┤6     ├┤  D(1)_1_dg ├┤  D(1)_0_dg ├
                  │         ││         ││         │└──────────┘│         ││      ││            ││            │
         digit_0: ┤         ├┤         ├┤5        ├────────────┤5        ├┤1     ├┤5           ├┤5           ├
                  │         ││         ││         │            │         ││      ││            ││            │
         digit_1: ┤         ├┤         ├┤         ├────────────┤         ├┤2 Add ├┤            ├┤            ├
                  │         ││         ││         │            │         ││      ││            ││            │
           cmp_0: ┤6        ├┤6        ├┤4        ├────────────┤4        ├┤      ├┤4           ├┤4           ├
                  │         ││         ││         │            │         ││      ││            ││            │
           cmp_1: ┤         ├┤         ├┤         ├────────────┤         ├┤      ├┤            ├┤            ├
                  │         ││         ││         │            │         ││      ││            ││            │
           aux_0: ┤9        ├┤9        ├┤6        ├────────────┤6        ├┤0     ├┤            ├┤            ├
                  └─────────┘└─────────┘└─────────┘            └─────────┘│      ││            ││            │
           aux_1: ────────────────────────────────────────────────────────┤3     ├┤6           ├┤6           ├
                                                                          └──────┘└────────────┘└────────────┘

    **References:**

    [1] Marsh and Wang, Combinatorial optimisation via highly efficient quantum walks, 2019.
    `arXiv:1912.07353 <https://arxiv.org/abs/1912.07353>`_

    """

    def __init__(
        self, num_states: int, name: Optional[str] = "OOPPID") -> None:
        r""" Permutation index computation.
        
        Args:
            num_states: Number of permuted states.
            name: The name of the circuit object.
        Raises:
            ValueError: If ``num_states`` is smaller or equal to 1.
        """
        if num_states <= 1:
            raise ValueError(
                "The number of permuted states must be greater than 1.")
        
        self._num_states = num_states
        self._num_state_qubits = ceil(log2(num_states))
        self._num_lehmer_digit_qubits = ceil(log2(num_states - 1))
        self._num_permutation_qubits = num_states * self._num_state_qubits
        
        super().__init__(name=name)

        num_ancillas = 0
        lehmer_digits = []
        lehmer_digit_qubit_nums = []
        adders = []
        mults = []
        
        max_index = 0
        for digit_index in range(self._num_states - 1):
            lehmer_digit_circuits = self._lehmer_digit(digit_index)
            lehmer_digits.append(lehmer_digit_circuits)
            lehmer_digit_qubit_nums.append(ceil(log2(self.num_states - digit_index)))
            
            num_ancillas = max(num_ancillas, lehmer_digit_circuits[0].num_ancillas)
            if digit_index > 2:
                num_ancillas = max(num_ancillas, lehmer_digit_circuits[0].num_ancillas + lehmer_digits[-2][0].num_ancillas)
            
            if digit_index > 0:                
                num_add_state_qubits = ceil(log2(max_index + 1))
                num_add_result_qubits = ceil(log2(max_index + self._num_states - digit_index))
                adder = FullAdd(num_add_state_qubits,
                                  has_cout=num_add_state_qubits != num_add_result_qubits,
                                  name="Add")
                adders.append(adder)
                # adder has carry-in and uses current digit register as state
                # with high 0-qubits borrowed from the other digit register
                num_ancillas = max(num_ancillas, 
                                   1 + max(0, num_add_state_qubits - 2 * self.num_lehmer_digit_qubits))
                
            max_index += self._num_states - 1 - digit_index
            
            if digit_index < self._num_states - 2:
                num_factor_qubits = ceil(log2(max_index + 1))
                num_product_qubits = ceil(log2(max_index * (self._num_states - 1 - digit_index) + 1))
                num_mult_product_qubits = num_factor_qubits + ceil(log2(self._num_states - digit_index))
                mult = InPlaceCnstMult(self._num_states - digit_index - 1, 
                                             num_factor_qubits=num_factor_qubits,
                                             omit_highest_qubit=num_product_qubits != num_mult_product_qubits,
                                             name=f"Mult({self._num_states - digit_index - 1})")
                mults.append(mult)
                # mult uses a digit register for additional aux qubits
                num_ancillas = max(num_ancillas, mult.num_ancillas - self.num_lehmer_digit_qubits)
                
            max_index *= self._num_states - 1 - digit_index
        
        # registers
        qr_perm_states = []
        for i in range(self.num_states):
            qr_perm_states.append(QuantumRegister(self.num_state_qubits, f"perm_{i}"))
        qr_index = QuantumRegister(ceil(log2(factorial(self.num_states))), "index")
        qr_digits = []
        qr_cmps = []
        for i in range(2):
            qr_cmps.append(AncillaRegister(1, f"cmp_{i}"))
            qr_digits.append(AncillaRegister(self.num_lehmer_digit_qubits, f"digit_{i}"))
        qr_aux = AncillaRegister(num_ancillas, "aux")
        
        def _perm_state_qubits(min_index):
            nonlocal qr_perm_states
            return [qubit for qreg in qr_perm_states[min_index:] for qubit in qreg]
        
        # circuit
        circuit = QuantumCircuit(*qr_perm_states, qr_index, *qr_digits, *qr_cmps, qr_aux, name=name)
        self.add_register(*qr_perm_states, qr_index, *qr_digits, *qr_cmps, qr_aux)
        
        # compute first lehmer digit
        lehmer_digit_circ_0, lehmer_digit_circ_1 = lehmer_digits[0]      
        circuit.append(lehmer_digit_circ_0.to_gate(),
                       _perm_state_qubits(0) + [qr_cmps[0][0]] + qr_index[:lehmer_digit_qubit_nums[0]] +
                       qr_aux[:lehmer_digit_circ_0.num_ancillas])
        circuit.append(lehmer_digit_circ_1.to_gate(),
                       _perm_state_qubits(0) + [qr_cmps[0][0]] + qr_index[:lehmer_digit_qubit_nums[0]] +
                       qr_aux[:lehmer_digit_circ_1.num_ancillas])
        
        for digit_index in range(1, self._num_states - 1):
            lehmer_reg_no = (digit_index - 1) % 2

            # multiplication
            mult = mults[digit_index - 1]
            mult_ancilla_qubits = (qr_digits[lehmer_reg_no][:] + qr_aux[:])[:mult.num_ancillas]
            circuit.append(mult.to_gate(),
                           mult_ancilla_qubits + qr_index[:mult.num_product_qubits])

            # parallel lehmer computing and uncomputing with split circuits
            lehmer_digit_circ_0, _ = lehmer_digits[digit_index]
            circuit.append(lehmer_digit_circ_0.to_gate(),
                          _perm_state_qubits(digit_index) + [qr_cmps[lehmer_reg_no][0]] + 
                          qr_digits[lehmer_reg_no][:lehmer_digit_qubit_nums[digit_index]] +
                          qr_aux[:lehmer_digit_circ_0.num_ancillas])
            
            if digit_index > 1:
                _, prev_lehmer_digit_circ_1 = lehmer_digits[digit_index - 1]
                circuit.append(prev_lehmer_digit_circ_1.inverse().to_gate(),
                               _perm_state_qubits(digit_index - 1) + [qr_cmps[1 - lehmer_reg_no][0]] +
                               qr_digits[1 - lehmer_reg_no][:lehmer_digit_qubit_nums[digit_index - 1]] +
                               qr_aux[-prev_lehmer_digit_circ_1.num_ancillas:])
                
            _, lehmer_digit_circ_1 = lehmer_digits[digit_index]
            circuit.append(lehmer_digit_circ_1.to_gate(),
                           _perm_state_qubits(digit_index) + [qr_cmps[lehmer_reg_no][0]] + 
                           qr_digits[lehmer_reg_no][:lehmer_digit_qubit_nums[digit_index]] +
                           qr_aux[:lehmer_digit_circ_1.num_ancillas])
            
            if digit_index > 1:
                lehmer_digit_circ_0, _ = lehmer_digits[digit_index - 1]
                circuit.append(lehmer_digit_circ_0.inverse().to_gate(),
                               _perm_state_qubits(digit_index - 1) + [qr_cmps[1 - lehmer_reg_no][0]] +
                               qr_digits[1 - lehmer_reg_no][:lehmer_digit_qubit_nums[digit_index - 1]] +
                               qr_aux[-lehmer_digit_circ_0.num_ancillas:])
            
            # addition
            adder = adders[digit_index - 1]
            adder_state_qubits = (qr_digits[lehmer_reg_no][:] + qr_digits[1 - lehmer_reg_no][:] + qr_aux[1:])[:adder.num_state_qubits]
            circuit.append(adder.to_gate(),
                           [qr_aux[0]] + adder_state_qubits + qr_index[:adder.num_qubits - adder.num_state_qubits - 1])
        
        # uncompute last lehmer digit
        if self._num_states > 2:
            digit_index = self._num_states - 1
            lehmer_reg_no = (digit_index - 1) % 2
            
            prev_lehmer_digit_circ_0, prev_lehmer_digit_circ_1 = lehmer_digits[digit_index - 1]
            circuit.append(prev_lehmer_digit_circ_1.inverse().to_gate(),
                           _perm_state_qubits(digit_index - 1) + [qr_cmps[1 - lehmer_reg_no][0]] +
                           qr_digits[1 - lehmer_reg_no][:lehmer_digit_qubit_nums[digit_index - 1]] +
                           qr_aux[-prev_lehmer_digit_circ_1.num_ancillas:])
            circuit.append(prev_lehmer_digit_circ_0.inverse().to_gate(),
                           _perm_state_qubits(digit_index - 1) + [qr_cmps[1 - lehmer_reg_no][0]] +
                           qr_digits[1 - lehmer_reg_no][:lehmer_digit_qubit_nums[digit_index - 1]] +
                           qr_aux[-prev_lehmer_digit_circ_0.num_ancillas:])
        
        self.append(circuit.to_gate(), self.qubits)


    @property
    def num_states(self) -> int:
        """The number of permuted states.
        Returns:
            The number of states.
        """
        return self._num_states
    
    @property
    def num_state_qubits(self) -> int:
        """Number of qubits for each permutation register.
        Returns:
            The number of state qubits.
        """
        return self._num_state_qubits
    
    @property
    def num_permutation_qubits(self) -> int:
        """Overall number of qubits for the permutation.
        Returns:
            The number of permutation qubits.
        """
        return self._num_permutation_qubits
    
    @property
    def num_lehmer_digit_qubits(self) -> int:
        """Number of qubits to store a (single) Lehmer digit.
        Returns:
            The number of Lehmer digit qubits.
        """
        return self._num_lehmer_digit_qubits

    def _lehmer_digit(self, digit_index: int) -> Tuple[QuantumCircuit, QuantumCircuit]:
        """Computes the Lehmer digit at given index for the permutation.
        
        Compares all lower permutation states against the permutation state
        at the given index and accumulates the digit with controlled incrementers.
        the circuit is split for parellization.

        Args:
            digit_index (int): Index of the Lehmer digit.
        Returns:
            QuantumCircuit, QuantumCircuit: The first and second half of the circuit.
        """
        # circuit blocks
        num_ancillas = 0
        cmp_incrementers = []
        for max_digit in range(self.num_states - digit_index - 1):
            cmp_incrementer = self._compartor_incrementer(max_digit)
            cmp_incrementers.append(cmp_incrementer)
            num_ancillas = max(num_ancillas, cmp_incrementer.num_ancillas)
        
        # registers
        qr_perm_states = []
        for i in range(digit_index, self.num_states):
            qr_perm_states.append(QuantumRegister(self.num_state_qubits, f"perm_{i}"))
            
        qr_cmp = QuantumRegister(1, "cmp")
        qr_digit = QuantumRegister(ceil(log2(self.num_states - digit_index)), "digit")
        qr_aux = AncillaRegister(num_ancillas, "aux")
        
        # circuit
        circuit_0 = QuantumCircuit(*qr_perm_states, qr_cmp, qr_digit, qr_aux, name=f"D({digit_index})_0")
        circuit_1 = QuantumCircuit(*qr_perm_states, qr_cmp, qr_digit, qr_aux, name=f"D({digit_index})_1")
        
        instructions = []
        for i, cmp_incrementer in enumerate(cmp_incrementers):
            num_digit_qubits = cmp_incrementer.num_qubits - 2 * self.num_state_qubits - cmp_incrementer.num_ancillas - 1
            instructions.append((cmp_incrementer.to_gate(),
                                 qr_perm_states[0][:] + qr_perm_states[i+1][:] + [qr_cmp[0]] +
                                 qr_digit[:num_digit_qubits] + qr_aux[:cmp_incrementer.num_ancillas]))
        
        for instruction in instructions[:int(len(instructions)/2)]:
            circuit_0.append(*instruction)
            
        for instruction in instructions[int(len(instructions)/2):]:
            circuit_1.append(*instruction)
        
        return circuit_0, circuit_1

    def _compartor_incrementer(self, max_counter: int) -> QuantumCircuit:
        """Compares two permutation registers (a and b) and increments
        a counter if the value in b is smaller than the value in a.

        Args:
            max_counter (int): Maximum value in the counter before the possible increment.

        Returns:
            QuantumCircuit: The circuit.
        """
        
        # circuit blocks
        cmp = Comparator(self.num_state_qubits, geq=False)
        num_ancillas = cmp.num_ancillas
        if max_counter == 0:
            # cmp is the incrementer, compare qubit is digit qubit
            num_counter_qubits = 1
        else:
            # ctrl incrementers num_state_qubits depends on the current maximal digit
            # carry-out only needed if next maximal digits needs more qubits
            num_ctrl_incr_state_qubits = ceil(log2(max_counter + 1))
            ctrl_incrementer = CtrlIncrementer(
                num_ctrl_incr_state_qubits, has_cout=num_ctrl_incr_state_qubits != ceil(log2(max_counter + 2)))
            num_ancillas = max(num_ancillas, ctrl_incrementer.num_ancillas)
            num_counter_qubits = ctrl_incrementer.num_qubits - ctrl_incrementer.num_ancillas - 1
        
        qr_perm_state_a = QuantumRegister(self.num_state_qubits, "perm_a")
        qr_perm_state_b = QuantumRegister(self.num_state_qubits, "perm_b")
        qr_cmp = QuantumRegister(1, "cmp")
        qr_cnt = QuantumRegister(num_counter_qubits, "cnt")
        qr_aux = AncillaRegister(num_ancillas, "aux")
        
        # circuit
        circuit = QuantumCircuit(qr_perm_state_a, qr_perm_state_b, qr_cmp, qr_cnt, qr_aux, name="Cmp(>):+1")
        
        if max_counter == 0:
            circuit.append(cmp.to_gate(),
                           qr_aux[:cmp.num_ancillas] + qr_perm_state_b[:] + qr_perm_state_a[:] + [qr_cnt[0]])
        else:
            circuit.append(cmp.to_gate(),
                           qr_aux[:cmp.num_ancillas] + qr_perm_state_b[:] + qr_perm_state_a[:] + [qr_cmp[0]])
            circuit.append(ctrl_incrementer.to_gate(),
                           qr_aux[:ctrl_incrementer.num_ancillas] + qr_cnt[:] + [qr_cmp[0]])
            circuit.append(cmp.to_gate(),
                           qr_aux[:cmp.num_ancillas] + qr_perm_state_b[:] + qr_perm_state_a[:] + [qr_cmp[0]])
        
        return circuit