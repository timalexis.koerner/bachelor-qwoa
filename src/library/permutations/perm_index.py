"""Compute the index of a permutation."""

from typing import Optional
from math import log2, ceil
from qiskit.circuit import QuantumRegister, AncillaRegister, QuantumCircuit
from ..divisors import CnstDivisor
from . import PermToLehmerCode, LehmerCodeToPerm


class PermIndex(QuantumCircuit):
    r"""Index calculation of a Permutation.
    
    By combining the circuits PtoLC and the inverse of LCtoP, the permutation
    registers are uncomputed and only the Lehmer-Code digits remain. These
    are pairwise combined with inverted integer divisions with remainder.
    For each inverted integer division :math:`DIV_i` the parameters consist
    of the quotient :math:`qt_i`, the remainder :math:`rmd_i = d_{i+1} < n-i-1`
    and the multiplication constant :math:`c_i=n-i-1`, resulting in the dividend
    :math:`dvd_i = qt_i \cdot c_i + rmd_i`. The first quotient is :math:`qt_0 = d_0`,
    afterwards the dividend from the previous inverse division is used as the
    quotient :math:`\forall i>1: qt_i = dvd_{i-1}`. Repeating the procdedure
    until all digits are used results in the permutation index
    :math:`dvd_{n-2} = id(\pi) = \sum_{i<n} d_i \cdot (n-i-1)! = (\dots(d_0 \cdot (n-1) + d_1) \cdot (n-2) + \cdots + d_{n-2}) \cdot 1`.

    Swaps before the division are necessary to conform to remainder, quotient input order.

    Example circuit for num_states=3:
    .. parsed-literal::

                 ┌─────────┐┌────────────┐          ┌───┐               ┌────────────┐
        index_0: ┤0        ├┤0           ├──────────┤ X ├───────■───────┤3           ├
                 │         ││            │          └─┬─┘       │       │            │
        index_1: ┤1        ├┤1           ├────────────┼─────────┼───────┤4           ├
                 │         ││            │┌───┐       │         │       │            │
        index_2: ┤2        ├┤2           ├┤ X ├───────┼─────────┼────■──┤5           ├
                 │         ││            │└─┬─┘┌───┐  │         │    │  │            │
          aux_0: ┤3        ├┤3           ├──┼──┤ X ├──┼────■────┼────┼──┤6 Div(2)_dg ├
                 │         ││            │  │  └─┬─┘  │    │    │    │  │            │
          aux_1: ┤4        ├┤4           ├──┼────┼────┼────┼────┼────┼──┤0           ├
                 │         ││            │  │    │    │    │    │    │  │            │
          aux_2: ┤5  PtoLC ├┤5  LCtoP_dg ├──┼────┼────┼────┼────┼────┼──┤1           ├
                 │         ││            │  │    │    │    │    │  ┌─┴─┐│            │
          aux_3: ┤6        ├┤6           ├──■────┼────┼────┼────┼──┤ X ├┤2           ├
                 │         ││            │       │    │  ┌─┴─┐  │  └───┘└────────────┘
          aux_4: ┤7        ├┤7           ├───────■────┼──┤ X ├──┼─────────────────────
                 │         ││            │            │  └───┘┌─┴─┐                   
          aux_5: ┤8        ├┤8           ├────────────■───────┤ X ├───────────────────
                 │         ││            │                    └───┘                   
          aux_6: ┤9        ├┤9           ├────────────────────────────────────────────
                 │         ││            │                                            
          aux_7: ┤10       ├┤10          ├────────────────────────────────────────────
                 └─────────┘└────────────┘                                            

    """

    def __init__(
        self, num_states: int, name: Optional[str] = "PID") -> None:
        r"""Index computation of a permutation.
        
        Args:
            num_states: Number of permuted states.
            name: The name of the circuit object.
        Raises:
            ValueError: If ``num_states`` is smaller or equal to 1.
        """
        if num_states <= 1:
            raise ValueError(
                "The number of permuted states must be greater than 1.")
        
        self._num_states = num_states
        self._num_state_qubits = ceil(log2(num_states))
        self._num_lehmer_digit_qubit_list = []
        for i in range(num_states - 1):
            self._num_lehmer_digit_qubit_list.append(ceil(log2(num_states - i)))
        self._num_permutation_qubits = num_states * self._num_state_qubits
        
        super().__init__(name=name)

        # circuit blocks
        perm_to_lehmer_code = PermToLehmerCode(num_states)
        lehmer_code_to_perm = LehmerCodeToPerm(num_states)
        num_ancillas = max(perm_to_lehmer_code.num_ancillas, lehmer_code_to_perm.num_ancillas)
        
        divs = []
        max_index = num_states - 1
        num_free_digit_qubits = self.num_state_qubits
        for i in range(num_states - 2):
            num_index_qubits = ceil(log2(max_index + 1))
            div = CnstDivisor(num_states - i - 1, num_quotient_qubits=num_index_qubits, 
                              name=f"Div({num_states - i - 1})")
            divs.append(div)
            
            num_free_perm_state_qubits = self.num_permutation_qubits - (div.num_qubits - div.num_ancillas)
            num_free_digit_qubits += self.num_lehmer_digit_qubit_list[i + 1]
            
            num_div_ancillas = max(0, div.num_ancillas - num_free_perm_state_qubits - num_free_digit_qubits)
            num_ancillas = max(num_ancillas, num_div_ancillas)
            
            max_index = max_index * (num_states - i - 1) + (num_states - i - 2)
        
        self._num_index_qubits = ceil(log2(max_index + 1))
            
        
        # circuit registers
        qr_perm_states = []
        for i in range(self.num_states):
            qr_perm_states.append(QuantumRegister(self.num_state_qubits, f"perm_{i}"))
        qr_digits = []
        for i, num_qubits in enumerate(self.num_lehmer_digit_qubit_list):
            qr_digits.append(QuantumRegister(num_qubits, f"digit_{i}"))
        qr_aux = AncillaRegister(num_ancillas, "aux")
        
        # circuit
        circuit = QuantumCircuit(*qr_perm_states, *qr_digits, qr_aux, name=name)
        
        # own registers
        self_qr_index = QuantumRegister(self.num_index_qubits, name="index")
        self_qr_aux = AncillaRegister(circuit.num_qubits - self.num_index_qubits, name="aux")
        self.add_register(self_qr_index, self_qr_aux)
        
        # compute lehmer code with uncomputation of permutation
        all_perm_state_qubits = [q for qr_perm_state in qr_perm_states for q in qr_perm_state]
        all_digit_qubits = [q for qr_digit in qr_digits for q in qr_digit]
        circuit.append(perm_to_lehmer_code.to_gate(),
                       all_perm_state_qubits[:] + all_digit_qubits[:] + qr_aux[:perm_to_lehmer_code.num_ancillas])
        circuit.append(lehmer_code_to_perm.inverse().to_gate(),
                       all_perm_state_qubits[:] + all_digit_qubits[:] + qr_aux[:lehmer_code_to_perm.num_ancillas])
        
        if num_states == 2:
            # half swap first digit as index
            circuit.cx(qr_digits[0], qr_perm_states[0])
            circuit.cx(qr_perm_states[0], qr_digits[0])
        else:
            # half swap first digit as quotient
            div = divs[0]
            for j in range(self.num_state_qubits):
                circuit.cx(qr_digits[0][j], j + div.num_remainder_qubits)
                circuit.cx(j + div.num_remainder_qubits, qr_digits[0][j])
        
            max_index = num_states - 1
            num_free_digit_qubits = self.num_state_qubits
            
            for i in range(num_states - 2):         
                div = divs[i]
                
                if i > 0:
                    # half swap last dividend as quotient
                    num_index_qubits = ceil(log2(max_index + 1))
                    for j in reversed(range(num_index_qubits)):
                        circuit.cx(j, j + div.num_remainder_qubits)
                        circuit.cx(j + div.num_remainder_qubits, j)
                
                # half swap next digit as remainder
                circuit.cx(qr_digits[i + 1], all_perm_state_qubits[:len(qr_digits[i + 1])])
                circuit.cx(all_perm_state_qubits[:len(qr_digits[i + 1])], qr_digits[i + 1])
                
                num_free_perm_state_qubits = self.num_permutation_qubits - (div.num_qubits - div.num_ancillas)
                num_free_digit_qubits += self.num_lehmer_digit_qubit_list[i + 1]
                free_qubits = all_perm_state_qubits[self.num_permutation_qubits - num_free_perm_state_qubits:] +\
                    all_digit_qubits[:num_free_digit_qubits] + qr_aux[:]
                
                # multiplication with additive               
                circuit.append(div.inverse().to_gate(),
                            free_qubits[:div.num_ancillas] + all_perm_state_qubits[:div.num_remainder_qubits + div.num_quotient_qubits])
                
                max_index = max_index * (num_states - i - 1) + (num_states - i - 2)
        
        self.append(circuit.to_gate(), self.qubits)


    @property
    def num_states(self) -> int:
        """The number of permuted states.
        Returns:
            The number of states.
        """
        return self._num_states
    
    @property
    def num_index_qubits(self) -> int:
        """Number of qubits for the index of the permutation.
        Returns:
            The number of index qubits.
        """
        return self._num_index_qubits
    
    @property
    def num_state_qubits(self) -> int:
        """Number of qubits for each permutation register.
        Returns:
            The number of state qubits.
        """
        return self._num_state_qubits
    
    @property
    def num_permutation_qubits(self) -> int:
        """Overall number of qubits for the permutation.
        Returns:
            The number of permutation qubits.
        """
        return self._num_permutation_qubits
    
    @property
    def num_lehmer_digit_qubit_list(self) -> list:
        """Number of qubits to store each Lehmer digit.
        Returns:
            The number of Lehmer digit qubits in a list.
        """
        return self._num_lehmer_digit_qubit_list
