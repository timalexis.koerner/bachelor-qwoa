"""Compute the permutation of a lehmer code."""

from typing import Optional
from math import log2, ceil
from qiskit.circuit import QuantumRegister, AncillaRegister, QuantumCircuit
from ..comparators import Comparator
from ..adders import CtrlIncrementer


class LehmerCodeToPerm(QuantumCircuit):
    r"""Out-of-place permutation calculation of a lehmer code.
    
    The creation of a permutation from the corresponding Lehmer-code
    can be computed by copying the lehmer digit at each index in to
    the permutation at the index. Then, starting from the highest
    index and counting down, look at state :math:`\pi'` at the index
    and increase all permutated states with with a higher index
    if the state is smaller to or equal to  :math:`\pi'`. For the
    quantum algorithm, the initalisation :math:`\pi_i \leftarrow d_i`
    is done with CNOT gates and the comparison can be uncomputed
    after the conditional incrementer regardless of the result since
    :math:`\pi_i \leq \pi_j \Rightarrow \pi_i \leq \pi_j + 1`.

    Example circuit for num_states=3:
    .. parsed-literal::

                       ┌───┐                            ┌────────────────┐┌────────────────┐
             perm_0_0: ┤ X ├────────────────────────────┤2               ├┤2               ├
                       └─┬─┘┌───┐                       │                ││                │
             perm_0_1: ──┼──┤ X ├───────────────────────┤3               ├┤3               ├
                         │  └─┬─┘┌───┐┌────────────────┐│                ││                │
             perm_1_0: ──┼────┼──┤ X ├┤2               ├┤0               ├┤                ├
                         │    │  └─┬─┘│                ││                ││                │
             perm_1_1: ──┼────┼────┼──┤3               ├┤1               ├┤                ├
                         │    │    │  │                ││                ││                │
             perm_2_0: ──┼────┼────┼──┤0               ├┤                ├┤0               ├
                         │    │    │  │                ││                ││                │
             perm_2_1: ──┼────┼────┼──┤1               ├┤  Cmp(a>=b):a++ ├┤1 Cmp(a>=b):a++ ├
                         │    │    │  │                ││                ││                │
            digit_0_0: ──■────┼────┼──┤  Cmp(a>=b):a++ ├┤                ├┤                ├
                              │    │  │                ││                ││                │
            digit_0_1: ───────■────┼──┤                ├┤                ├┤                ├
                                   │  │                ││                ││                │
              digit_1: ────────────■──┤                ├┤                ├┤                ├
                                      │                ││                ││                │
                  cmp: ───────────────┤4               ├┤4               ├┤4               ├
                                      │                ││                ││                │
                  aux: ───────────────┤5               ├┤5               ├┤5               ├
                                      └────────────────┘└────────────────┘└────────────────┘

    """

    def __init__(
        self, num_states: int, name: Optional[str] = "LCtoP") -> None:
        r"""Permutation computaton from Lehmer code.
        
        Args:
            num_states: Number of permuted states.
            name: The name of the circuit object.
        Raises:
            ValueError: If ``num_states`` is smaller or equal to 1.
        """
        if num_states <= 1:
            raise ValueError(
                "The number of permuted states must be greater than 1.")
        
        self._num_states = num_states
        self._num_state_qubits = ceil(log2(num_states))
        self._num_lehmer_digit_qubit_list = []
        for i in range(num_states - 1):
            self._num_lehmer_digit_qubit_list.append(ceil(log2(num_states - i)))
        self._num_permutation_qubits = num_states * self._num_state_qubits
        
        super().__init__(name=name)

        cmp_incrementer = self._in_place_compartor_incrementer()
        num_ancillas = cmp_incrementer.num_ancillas
        
        # registers
        qr_perm_states = []
        for i in range(self.num_states):
            qr_perm_states.append(QuantumRegister(self.num_state_qubits, f"perm_{i}"))
        qr_digits = []
        for i, num_qubits in enumerate(self.num_lehmer_digit_qubit_list):
            qr_digits.append(QuantumRegister(num_qubits, f"digit_{i}"))
        qr_cmp = AncillaRegister(1, "cmp")
        qr_aux = AncillaRegister(num_ancillas, "aux")
        
        # circuit
        circuit = QuantumCircuit(*qr_perm_states, *qr_digits, qr_cmp, qr_aux, name=name)
        self.add_register(*qr_perm_states, *qr_digits, qr_cmp, qr_aux)
        
        # compute permutuation
        for i in reversed(range(num_states - 1)):
            # psuedo adder
            circuit.cx(qr_digits[i], qr_perm_states[i][:len(qr_digits[i])])
            # comparator incrementers
            for j in range(i + 1, num_states):
                circuit.append(cmp_incrementer.to_gate(),
                               qr_perm_states[j][:] + qr_perm_states[i][:] + [qr_cmp[0]] +
                               qr_aux[:cmp_incrementer.num_ancillas])
        
        self.append(circuit.to_gate(), self.qubits)


    @property
    def num_states(self) -> int:
        """The number of permuted states.
        Returns:
            The number of states.
        """
        return self._num_states
    
    @property
    def num_state_qubits(self) -> int:
        """Number of qubits for each permutation register.
        Returns:
            The number of state qubits.
        """
        return self._num_state_qubits
    
    @property
    def num_permutation_qubits(self) -> int:
        """Overall number of qubits for the permutation.
        Returns:
            The number of permutation qubits.
        """
        return self._num_permutation_qubits
    
    @property
    def num_lehmer_digit_qubit_list(self) -> list:
        """Number of qubits to store each Lehmer digit.
        Returns:
            The number of Lehmer digit qubits in a list.
        """
        return self._num_lehmer_digit_qubit_list

    def _in_place_compartor_incrementer(self) -> QuantumCircuit:
        """Compares two registers (a >= b) and increments register a by 1 if true.
        Returns:
            QuantumCircuit: The circuit.
        """
        # circuit blocks
        cmp = Comparator(self.num_state_qubits)
        num_state_qubits = max(1, self.num_state_qubits - 1)
        ctrl_incrementer = CtrlIncrementer(num_state_qubits, has_cout=self.num_state_qubits != 1)
        num_ancillas = max(cmp.num_ancillas, ctrl_incrementer.num_ancillas)
        
        qr_a = QuantumRegister(self.num_state_qubits, "a")
        qr_b = QuantumRegister(self.num_state_qubits, "b")
        qr_cmp = QuantumRegister(1, "cmp")
        qr_aux = AncillaRegister(num_ancillas, "aux")
        
        # circuit
        circuit = QuantumCircuit(qr_a, qr_b, qr_cmp, qr_aux, name="Cmp(a>=b):a++")
        
        circuit.append(cmp.to_gate(),
                        qr_aux[:cmp.num_ancillas] + qr_a[:] + qr_b[:] + [qr_cmp[0]])
        circuit.append(ctrl_incrementer.to_gate(),
                        qr_aux[:ctrl_incrementer.num_ancillas] + qr_a[:] + [qr_cmp[0]])
        circuit.append(cmp.to_gate(),
                        qr_aux[:cmp.num_ancillas] + qr_a[:] + qr_b[:] + [qr_cmp[0]])
        
        return circuit