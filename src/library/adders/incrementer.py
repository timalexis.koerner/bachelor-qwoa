"""Incrementer and controlled incrementer increasing by 1."""

from typing import Optional
from qiskit.circuit import QuantumCircuit, QuantumRegister, AncillaRegister
from qiskit.circuit.library.arithmetic.adders.adder import Adder
from .cnst_half_add import CnstHalfAdd

class Incrementer(Adder):
    r"""An incrementer reusing the consant optimized half adder based on [1].
    The size of the constant register :math:`|csnt\rangle` is always on less
    than the sum register :math:`|s\rangle`.
    
    There are two versions, with and without the carry-out qubit.
    
    Example circuit for num_state_qubits=3:
    .. parsed-literal::
    
                ┌──────────────┐
        cnst_0: ┤0             ├
                │              │
        cnst_1: ┤1             ├
                │              │
           s_0: ┤2             ├
                │  CnstHalfAdd │
           s_1: ┤3             ├
                │              │
           s_2: ┤4             ├
                │              │
          cout: ┤5             ├
                └──────────────┘

    **References:**

    [1] Thapliyal et al., Design of Efficient Reversible Logic-Based Binary and BCD Adder Circuits, 2017.
    `arXiv:1712.02630 <https://arxiv.org/pdf/arXiv:1712.02630.pdf>`_
    
    """
    
    def __init__(
        self, num_state_qubits: int, has_cout: Optional[bool] = True, name: Optional[str] = "+1"
    ) -> None:
        r"""
        Args:
            num_state_qubits: The number of qubits in the :math:`|s\rangle` register.
            has_cout: If the adder should include a carry-out.
            name: The name of the circuit object.
        Raises:
            ValueError: If ``num_state_qubits`` is less than 1.
        """
        if num_state_qubits < 1:
            raise ValueError("The number of state qubits must be at least 1.")


        super().__init__(num_state_qubits, name=name)
        circuit = QuantumCircuit(name=name)
        
        registers = []
        if num_state_qubits > 1:
            registers.append(AncillaRegister(num_state_qubits - 1, "cnst"))
        registers.append(QuantumRegister(num_state_qubits, "s"))
        if has_cout:
            registers.append(QuantumRegister(1, "cout"))
        self.add_register(*registers)
        circuit.add_register(*registers)
        
        #build circuit
        circuit.append(
            CnstHalfAdd(1, num_state_qubits , has_cout=has_cout).to_gate(),
            self.qubits)
        
        self.append(circuit.to_gate(), self.qubits)
    
    @property
    def num_constant_qubits(self) -> int:
        """The constant register size.
        Returns:
            The number of qubits in the constant register.
        """
        return self.num_state_qubits - 1


class CtrlIncrementer(Adder):
    r"""A controlled :math:`num_state_qubits` incrementer implemented as a 
    :math:`num_state_qubits + 1`-qubit incrementer, with the control :math:`|ctrl\rangle` as the 
    least significant qubit, followed by a NOT gate on :math:`|ctrl\rangle`. For :math:`ctrl=0`
    the incrementation only sets in :math:`ctrl \to 1`, which is reset by the NOT gate.
    For :math:`ctrl=1` the incrementation acts on all qubits while setting :math:`ctrl \to 1`,
    which is again reset by the NOT gate.
    
    There are two versions, with and without the carry-out qubit.
    
    Example circuit for num_state_qubits=3:
    .. parsed-literal::
                ┌──────────────┐     
        cnst_0: ┤0             ├─────
                │              │     
        cnst_1: ┤1             ├─────
                │              │     
        cnst_2: ┤2             ├─────
                │              │     
           s_0: ┤4             ├─────
                │  CnstHalfAdd │     
           s_1: ┤5             ├─────
                │              │     
           s_2: ┤6             ├─────
                │              │     
          cout: ┤7             ├─────
                │              │┌───┐
          ctrl: ┤3             ├┤ X ├
                └──────────────┘└───┘

    **References:**

    [1] Thapliyal et al., Design of Efficient Reversible Logic-Based Binary and BCD Adder Circuits, 2017.
    `arXiv:1712.02630 <https://arxiv.org/pdf/arXiv:1712.02630.pdf>`_
    
    """
    
    def __init__(
        self, num_state_qubits: int, has_cout: Optional[bool] = True, 
        name: Optional[str] = "Ctrl+1"
    ) -> None:
        r"""
        Args:
            num_state_qubits: The number of qubits in the :math:`|s\rangle` register.
            has_cout: If the adder should include a carry-out.
            name: The name of the circuit object.
        Raises:
            ValueError: If ``num_state_qubits`` is less than 1.
        """
        if num_state_qubits < 1:
            raise ValueError("The number of state qubits must be at least 1.")

        self._has_cout = has_cout
        super().__init__(num_state_qubits, name=name)
        circuit = QuantumCircuit(name=name)
        
        qr_cnst = AncillaRegister(num_state_qubits, "cnst")
        qr_s = QuantumRegister(num_state_qubits, "s")
        self.add_register(qr_cnst, qr_s)
        circuit.add_register(qr_cnst, qr_s)
        if has_cout:
            qr_cout = QuantumRegister(1, "cout")
            self.add_register(qr_cout)
            circuit.add_register(qr_cout)
        qr_ctrl = QuantumRegister(1, "ctrl")
        self.add_register(qr_ctrl)
        circuit.add_register(qr_ctrl)
        
        #build circuit
        adder_qubits = qr_cnst[:] + qr_ctrl[:] + qr_s[:]
        if has_cout:
            adder_qubits += qr_cout[:]

        circuit.append(
            CnstHalfAdd(1, num_state_qubits + 1, has_cout=has_cout).to_gate(),
            adder_qubits)
        circuit.x(qr_ctrl[0])
        
        self.append(circuit.to_gate(), self.qubits)
    
    @property
    def num_constant_qubits(self) -> int:
        """The constant register size.
        Returns:
            The number of qubits in the constant register.
        """
        return self.num_state_qubits
    
    @property
    def has_cout(self) -> bool:
        """Checks if the adder has a carry-out.
        Returns:
            bool: If the adder has a carry_out.
        """
        return self._has_cout