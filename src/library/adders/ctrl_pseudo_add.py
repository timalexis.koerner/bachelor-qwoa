"""Controlled constant pseudo-adder build from CNOT gates."""

from typing import Optional
from math import log2, ceil
from qiskit.circuit import QuantumCircuit, QuantumRegister
from qiskit.circuit.library.arithmetic.adders.adder import Adder


class CtrlPseudoAdd(Adder):
    r"""A controlled constant pseudo-adder, mimicking other controlled adders.
    
    The pseudo-adder should only be used for :math:`|s\rangle = |0\rangle` and
    adds by setting :math:`|0\rangle \to |cnst\rangle` with CNOTs.

    Example circuit for cnst=6:
    .. parsed-literal::              
                                                          
         s_0: ──────────
              ┌───┐     
         s_1: ┤ X ├─────
              └─┬─┘┌───┐
         s_2: ──┼──┤ X ├
                │  └─┬─┘
        ctrl: ──■────■──
                                                                
    """

    def __init__(
        self, constant: int, num_state_qubits: Optional[int] = None, 
        name: Optional[str] = "CtrlPseudoAdd"
    ) -> None:
        r"""
        Args:
            constant: The constant number to add.
            num_state_qubits: The number of qubits in the :math:`|s\rangle` register.
            name: The name of the circuit object.
        Raises:
            ValueError: If ``constant`` is lower than 1.
            ValueError: If ``num_state_qubits`` is not big enough to hold the constant.
        """
        if constant < 1:
            raise ValueError("The constant number to add must be at least 1.")

        min_num_sq = ceil(log2(constant + 1))
        if num_state_qubits is None:
            num_state_qubits = min_num_sq
        elif num_state_qubits < min_num_sq:
            raise ValueError(
                f"The number of state qubits must be at least {min_num_sq} to fit the constant.")

        self._constant = constant
        super().__init__(num_state_qubits, name=name)
        
        # build adder circuit
        circuit = QuantumCircuit(name=name)
        qr_s = QuantumRegister(num_state_qubits, "s")
        qr_ctrl = QuantumRegister(1, "ctrl")
        circuit.add_register(qr_s, qr_ctrl)
        self.add_register(qr_s, qr_ctrl)

        for idx, bit in enumerate(reversed(bin(constant)[2:])):
            if bit == "1":
                circuit.cx(qr_ctrl, qr_s[idx])
        self.append(circuit.to_gate(), self.qubits)

    @property
    def constant(self) -> int:
        """The constant number to add.
        Returns:
            The constant.
        """
        return self._constant

    @property
    def num_constant_qubits(self) -> int:
        """The constant register size (always 0).
        Returns:
            The number of qubits in the constant register.
        """
        return 0

    @property
    def has_cout(self) -> bool:
        """Checks if the adder has a carry-out (always False).

        Returns:
            bool: If the adder has a carry_out.
        """
        return False
