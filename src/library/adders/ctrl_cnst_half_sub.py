"""Constant half subtractor, using a half ripple-carry adder and bitwise complements."""

from typing import Optional, Tuple
import numpy
from .ctrl_cnst_half_add import CtrlCnstHalfAdd


class CtrlCnstHalfSub(CtrlCnstHalfAdd):
    r"""A constant optimized subtractor version of the controlled half ripple-carry 
    adder proposed by Thapliyal and Ranganathan.
    
    For constant optimization see CnstHalfSub documentation, for controlled
    conversion see CtrlHalfAdd documentation.
    
    There are two versions, with and without the carry-out qubit.
    
    Example circuit for cnst=6:
    .. parsed-literal:: 
                                        
                   ┌───┐                         ┌───┐     
        cnst: ─────┤ X ├───────■─────────■────■──┤ X ├─────
              ┌───┐└─┬─┘┌───┐  │         │    │  └─┬─┘     
         s_0: ┤ X ├──┼──┤ X ├──┼─────────┼────┼────┼───────
              └───┘  │  └───┘┌─┴─┐     ┌─┴─┐  │    │  ┌───┐
         s_1: ───────■────■──┤ X ├──■──┤ X ├──┼────■──┤ X ├
                          │  └─┬─┘  │  └─┬─┘┌─┴─┐     └─┬─┘
         s_2: ────────────┼────■────┼────■──┤ X ├───────┼──
                   ┌───┐┌─┴─┐     ┌─┴─┐     └─┬─┘       │  
        cout: ─────┤ X ├┤ X ├─────┤ X ├───────┼─────────┼──
                   └─┬─┘└─┬─┘     └─┬─┘       │         │  
        ctrl: ───────■────■─────────■─────────■─────────■──
                                                        

    **References:**

    [1] Thapliyal et al., Quantum Circuit Designs of Integer Division Optimizing T-count and T-depth, 2018.
    `arXiv:1809.09732 <https://arxiv.org/pdf/arXiv:1809.09732.pdf>`_

    [2] Thapliyal et al., Design of Efficient Reversible Logic-Based Binary and BCD Adder Circuits, 2017.
    `arXiv:1712.02630 <https://arxiv.org/pdf/arXiv:1712.02630.pdf>`_
    
    """
    
    def __init__(
        self, constant: int, num_state_qubits: Optional[int] = None, 
        num_propagation_qubits: Optional[int] = 0, has_cout: Optional[bool] = True, 
        enforce_incrementer: Optional[bool] = False, name: Optional[str] = "CtrlCnstHalfSub"
    ) -> None:
        r"""
        Args:
            constant: The constant number to add.
            num_state_qubits: The number of qubits in the :math:`|s\rangle` register.
            num_propagation_qubits: The number of additional qubits for the sum
                in the :math:`|ext\rangle` register.
            has_cout: If the adder should include a carry-out.
            enforce_incrementer: If the adder should always use the incrementer for 
                propation qubits (may result in higher depths).
            name: The name of the circuit object.
        Raises:
            ValueError: If ``constant`` is lower than 1.
            ValueError: If ``num_state_qubits`` is not big enough to hold the constant.
            ValueError: If ``num_propagation_qubits`` is lower than 0.
        """
        super().__init__(
            constant, 
            num_state_qubits=num_state_qubits, 
            num_propagation_qubits=num_propagation_qubits,
            has_cout=has_cout,
            enforce_incrementer=enforce_incrementer,
            name=name)
        
    def _precalc_xgates(self) -> Tuple[numpy.ndarray, numpy.ndarray]:
        r"""Constant adding simplifies the ripple-carry adder in the 
        sections before the Toffoli gates and after the Peres gates.
        The subtraction adds additional NOT gates on the :math:`s` 
        register, pre and post core circuit.

        Returns:
            Tuple[numpy.ndarray, numpy.ndarray]: Binary arrays, representing NOT gate positions.
        """
        
        pre_xgates, _ = super()._precalc_xgates()

        # qr_s and qr_s ext additonal NOTs due to subtraction
        for idx in range(self._num_original_state_qubits, len(pre_xgates) - 1):
            pre_xgates[idx] = 1 - pre_xgates[idx]

        post_xgates = pre_xgates.copy()

        # post: no qr_cout (only for pre)
        post_xgates[-1] = 0
        
        return pre_xgates, post_xgates
