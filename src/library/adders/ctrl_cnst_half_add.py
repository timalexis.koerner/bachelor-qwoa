"""Controlled constant half ripple-carry adder based on a constant half adder."""

from typing import Optional, Tuple
from math import log2, ceil
import numpy
from qiskit.circuit import QuantumCircuit, QuantumRegister, AncillaRegister
from qiskit.circuit.library.arithmetic.adders.adder import Adder
from .incrementer import CtrlIncrementer


class CtrlCnstHalfAdd(Adder):
    r"""A constant optimized version of the controlled half ripple-carry adder proposed
    by Thapliyal and Ranganathan.

    For constant optimization see CnstHalfAdd documentation. At the position of the lowest 
    nonzero :math:`cnst` bit, the NOT gate of the :math:`|s\rangle` register is turned into a CNOT.
    
    The controlled version is derived by describing each Peres gate as a CCNOT gate followed
    by a CNOT gate on the previous controls. Since each Peres gate (except for :math:`cout`) is morrored
    by a CCNOT gate, controlling this CNOT with an additional control register :math:`ctrl` enables and
    disables the core part of the circuit. The possible initial NOT on :math:`cout` can be turned into a 
    CNOT (controlled by :math:`ctrl`) and the center Peres gate is turned into a CCCNOT and a CCNOT.
    The CCCNOT is implemented by borrowing the second highest :math:`s` qubit and using toggle detection 
    and uncomputing, resulting in 4 CCNOT gates.
    
    The number of state qubits can be further extended with the parameter ``num_propagation_qubits``,
    allowing for constant adders adding on high state qubit numbers. For :math:`ctrl >= 2` 
    (or :math:`ctrl >= 4` for no carry-out) a controlled incrementer is used to propagate the original 
    carry-out to archive lower depths.
    
    There are two versions, with and without the carry-out qubit.

    Example circuit for cnst=6:
    .. parsed-literal::              
                                                          
                     ┌───┐                         ┌───┐          
        cnst_0: ─────┤ X ├───────■─────────■────■──┤ X ├──────────
                     └─┬─┘       │         │    │  └─┬─┘          
           s_0: ───────┼─────────┼─────────┼────┼────┼────────────
                ┌───┐  │       ┌─┴─┐     ┌─┴─┐  │    │  ┌───┐┌───┐
           s_1: ┤ X ├──■────■──┤ X ├──■──┤ X ├──┼────■──┤ X ├┤ X ├
                ├───┤       │  └─┬─┘  │  └─┬─┘┌─┴─┐┌───┐└─┬─┘└───┘
           s_2: ┤ X ├───────┼────■────┼────■──┤ X ├┤ X ├──┼───────
                ├───┤     ┌─┴─┐     ┌─┴─┐     └─┬─┘└───┘  │       
          cout: ┤ X ├─────┤ X ├─────┤ X ├───────┼─────────┼───────
                └─┬─┘     └─┬─┘     └─┬─┘       │         │       
          ctrl: ──■─────────■─────────■─────────■─────────■───────
                                                                
    **References:**

    [1] Thapliyal et al., Quantum Circuit Designs of Integer Division Optimizing T-count and T-depth, 2018.
    `arXiv:1809.09732 <https://arxiv.org/pdf/arXiv:1809.09732.pdf>`_

    """

    def __init__(
        self, constant: int, num_state_qubits: Optional[int] = None, 
        num_propagation_qubits: Optional[int] = 0, has_cout: Optional[bool] = True, 
        enforce_incrementer: Optional[bool] = False, name: Optional[str] = "CtrlCnstHalfAdd"
    ) -> None:
        r"""
        Args:
            constant: The constant number to add.
            num_state_qubits: The number of qubits in the :math:`|s\rangle` register.
            num_propagation_qubits: The number of additional qubits for the sum
                in the :math:`|ext\rangle` register.
            has_cout: If the adder should include a carry-out.
            enforce_incrementer: If the adder should always use the incrementer for
                propation qubits (may result in higher depths).
            name: The name of the circuit object.
        Raises:
            ValueError: If ``constant`` is lower than 1.
            ValueError: If ``num_state_qubits`` is not big enough to hold the constant.
            ValueError: If ``num_propagation_qubits`` is lower than 0.
        """
        if constant < 1:
            raise ValueError("The constant number to add must be at least 1.")
        if num_propagation_qubits < 0:
            raise ValueError("The number of propagator qubits must be at least 0.")

        min_num_sq = ceil(log2(constant + 1))
        if num_state_qubits is None:
            num_state_qubits = min_num_sq
        elif num_state_qubits < min_num_sq:
            raise ValueError(
                f"The number of state qubits must be at least {min_num_sq} to fit the constant.")

        self._constant = constant
        self._has_cout = has_cout
        
        # only more than 2 (4 for no carry-out) propagator qubits bring improvements
        if not enforce_incrementer and \
                ((has_cout and num_propagation_qubits < 2) or (not has_cout and num_propagation_qubits < 4)):
            num_state_qubits += num_propagation_qubits
            num_propagation_qubits = 0
        self._num_original_state_qubits = num_state_qubits
        super().__init__(self._num_original_state_qubits + num_propagation_qubits, name=name)

        # precalc NOT gates
        pre_xgates, post_xgates = self._precalc_xgates()
        # commuting NOT gates for no cout and no propagation
        if not has_cout and num_propagation_qubits == 0:
            pre_xgates[-2] = 0
            post_xgates[-2] = 0

        # find lowest set cnst bit
        lowest_set_bit = pre_xgates.argmax()
        # reduced cnst register
        pre_xgates = pre_xgates[lowest_set_bit + 1:]
        post_xgates = post_xgates[lowest_set_bit + 1:]
        num_reduced_constant_qubits = self._num_original_state_qubits - lowest_set_bit - 1
        if num_propagation_qubits > 0:
            self._num_constant_qubits = num_reduced_constant_qubits + num_propagation_qubits + 1
        else:
            self._num_constant_qubits = num_reduced_constant_qubits
        
        # build adder circuit
        circuit = QuantumCircuit(name=name)
        
        qr_cnst = AncillaRegister(num_reduced_constant_qubits, "cnst")
        qr_s = QuantumRegister(self._num_original_state_qubits, "s")
        if num_propagation_qubits > 0:
            qr_cnst_ext = AncillaRegister(num_propagation_qubits + 1, "cnst_ext")
            qr_s_ext = QuantumRegister(num_propagation_qubits, "s_ext")
            circuit.add_register(qr_cnst, qr_cnst_ext, qr_s, qr_s_ext)
        else:
            circuit.add_register(qr_cnst, qr_s)
        self.add_register(
            AncillaRegister(self._num_constant_qubits, "cnst"),
            QuantumRegister(self._num_state_qubits, "s"))
        
        if has_cout:
            qr_cout = QuantumRegister(1, "cout")
            circuit.add_register(qr_cout)
            self.add_register(qr_cout)
        # pre_cout can be temporary cout before incrementer
        if num_propagation_qubits > 0:
            cout_ctrl_qubit = qr_cnst_ext[-1]
        elif has_cout:
            cout_ctrl_qubit = qr_cout[0]
            
        qr_ctrl = QuantumRegister(1, "ctrl")
        circuit.add_register(qr_ctrl)
        self.add_register(qr_ctrl)

        # pre NOT gates
        not_gate_qubits = qr_cnst[:] + qr_s[:]
        if num_propagation_qubits > 0:
            not_gate_qubits += qr_s_ext[:]
        for idx, has_gate in enumerate(pre_xgates[:-1]):
            if has_gate:
                circuit.x(not_gate_qubits[idx])
        # pre NOT gate on pre_cout turned into CNOT gate
        if (has_cout or num_propagation_qubits > 0) and pre_xgates[-1]:
            circuit.cx(qr_ctrl, cout_ctrl_qubit)

        # main ripple-carry part
        # start from the lowest set bit
        if lowest_set_bit < self._num_original_state_qubits - 1:
            circuit.cx(qr_s[lowest_set_bit], qr_cnst[0])
        for i in range(lowest_set_bit + 1, self._num_original_state_qubits - 1):
            circuit.ccx(qr_cnst[i - lowest_set_bit - 1], qr_s[i], qr_cnst[i - lowest_set_bit])

        # if cout exists or the adder has propagation,
        # append CPeres gate (CCCNOT on cout + CCNOT) otherwise only append CCNOT
        if has_cout or num_propagation_qubits > 0:
            if lowest_set_bit == self._num_original_state_qubits - 1:
                circuit.ccx(qr_ctrl, qr_s[-1], cout_ctrl_qubit)
            else:
                # CCCNOT part of CPeres gate
                circuit.ccx(qr_ctrl, qr_s[-2], cout_ctrl_qubit)
                circuit.ccx(qr_cnst[-1], qr_s[-1], qr_s[-2])
                circuit.ccx(qr_ctrl, qr_s[-2], cout_ctrl_qubit)
                circuit.ccx(qr_cnst[-1], qr_s[-1], qr_s[-2])
                # CCNOT part of CPeres gate, can be delayed to after incrementer
                if num_propagation_qubits == 0:
                    circuit.ccx(qr_ctrl, qr_cnst[-1], qr_s[-1])
        elif lowest_set_bit != self._num_original_state_qubits - 1:
            circuit.ccx(qr_ctrl, qr_cnst[-1], qr_s[-1])
                        
        if num_propagation_qubits > 0:
            if has_cout:
                circuit.append(
                    CtrlIncrementer(num_state_qubits=num_propagation_qubits, has_cout=True).to_gate(),
                    qr_cnst_ext[:-1] + qr_s_ext[:] + qr_cout[:] + [cout_ctrl_qubit])
            else:
                circuit.append(
                    CtrlIncrementer(num_state_qubits=num_propagation_qubits, has_cout=False).to_gate(),
                    qr_cnst_ext[:-1] + qr_s_ext[:] + [cout_ctrl_qubit])
            
            # uncompute pre cout
            if lowest_set_bit == self._num_original_state_qubits - 1:
                circuit.ccx(qr_ctrl, qr_s[-1], cout_ctrl_qubit)
            else:
                # reversed CCCNOT part of CPeres gate
                circuit.ccx(qr_cnst[-1], qr_s[-1], qr_s[-2])
                circuit.ccx(qr_ctrl, qr_s[-2], cout_ctrl_qubit)
                circuit.ccx(qr_cnst[-1], qr_s[-1], qr_s[-2])
                circuit.ccx(qr_ctrl, qr_s[-2], cout_ctrl_qubit)
                # delayed CCNOT part of CPeres gate
                circuit.ccx(qr_ctrl, qr_cnst[-1], qr_s[-1])
            
            # uncompute pre CNOT gate on pre_cout
            if pre_xgates[-1] == 1:
                circuit.cx(qr_ctrl, cout_ctrl_qubit)
  
        # end at the lowest set bit
        for i in reversed(range(lowest_set_bit + 1, self._num_original_state_qubits - 1)):
            circuit.ccx(qr_cnst[i - lowest_set_bit - 1], qr_s[i], qr_cnst[i - lowest_set_bit])
            circuit.ccx(qr_ctrl, qr_cnst[i - lowest_set_bit - 1], qr_s[i])
        if lowest_set_bit < self._num_original_state_qubits - 1:
            circuit.cx(qr_s[lowest_set_bit], qr_cnst[0])

        # s gets a CNOT at the lowest set bit
        circuit.cx(qr_ctrl, qr_s[lowest_set_bit])
        # post NOT gates
        for idx, has_gate in enumerate(post_xgates):
            if has_gate == 1:
                circuit.x(not_gate_qubits[idx])

        self.append(circuit.to_gate(), self.qubits)

    @property
    def constant(self) -> int:
        """The constant number to add.
        Returns:
            The constant.
        """
        return self._constant

    @property
    def num_constant_qubits(self) -> int:
        """The constant register size.
        Returns:
            The number of qubits in the constant register.
        """
        return self._num_constant_qubits

    @property
    def has_cout(self) -> bool:
        """Checks if the adder has a carry-out.

        Returns:
            bool: If the adder has a carry_out.
        """
        return self._has_cout

    def _precalc_xgates(self) -> Tuple[numpy.ndarray, numpy.ndarray]:
        """Constant adding simplifies the ripple-carry adder in the 
        sections before the Toffoli gates and after the Peres gates.

        Returns:
            Tuple[numpy.ndarray, numpy.ndarray]: Binary arrays, representing NOT gate positions.
        """

        bin_repr = bin(self._constant)[2:]
        bin_repr = "0" * (self._num_original_state_qubits - len(bin_repr)) + bin_repr
        # size for full qr_cnst, qr_s and qr_s_ext
        pre_xgates = numpy.zeros(self._num_original_state_qubits + self.num_state_qubits + 1, dtype=int)

        # qr_s[1] to qr_s[-1]
        for idx, bit in enumerate(reversed(bin_repr[:-1])):
            pre_xgates[idx + self._num_original_state_qubits + 1] = int(bit)

        # qr_cnst[0] and qr_cnst[1]
        pre_xgates[0] = int(bin_repr[-1])
        if self._num_original_state_qubits > 1:
            pre_xgates[1] = int(bin_repr[-2])

        # qr_cnst[2] to qr_cnst[-1]
        for idx in range(2, self._num_original_state_qubits):
            pre_xgates[idx] = (pre_xgates[idx + self._num_original_state_qubits - 1]
                               + pre_xgates[idx + self._num_original_state_qubits]) % 2

        post_xgates = pre_xgates.copy()

        # pre: qr_cout
        pre_xgates[-1] = pre_xgates[2 * self._num_original_state_qubits - 1]

        return pre_xgates, post_xgates
