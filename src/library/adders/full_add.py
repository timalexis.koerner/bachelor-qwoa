"""Full ripple-carry adder."""

from typing import Optional
from qiskit.circuit import QuantumCircuit, QuantumRegister
from qiskit.circuit.library.arithmetic.adders.adder import Adder
from ..gates.peres import PeresGate, PeresdgGate

class FullAdd(Adder):
    r"""The full ripple-carry adder proposed by Thapliyal and Ranganathan, 
    which is closely related to the adder proposed by Curccaro et al..
    
    There are two versions, with and without the carry-out qubit, carry-in is always included.
    
    Example circuit for 3 qubits, split into sections for later constant optimization:
    .. parsed-literal::

                     ┌───┐                ░                                ┌───────┐ ░           ┌───┐     
           cin: ─────┤ X ├────────────────░───■────────────────────────────┤0      ├─░───────────┤ X ├─────
                     └─┬─┘┌───┐           ░ ┌─┴─┐                 ┌───────┐│       │ ░      ┌───┐└─┬─┘     
           a_0: ──■────■──┤ X ├───────────░─┤ X ├──■──────────────┤0      ├┤2      ├─░──────┤ X ├──■────■──
                  │       └─┬─┘┌───┐      ░ └─┬─┘┌─┴─┐     ┌─────┐│       ││       │ ░ ┌───┐└─┬─┘       │  
           a_1: ──┼────■────■──┤ X ├──────░───┼──┤ X ├─────┤0    ├┤2      ├┤  Psdg ├─░─┤ X ├──■────■────┼──
                  │    │       └─┬─┘      ░   │  └─┬─┘     │     ││       ││       │ ░ └─┬─┘       │    │  
           a_2: ──┼────┼────■────■────■───░───┼────┼───────┤     ├┤  Psdg ├┤       ├─░───■────■────┼────┼──
                ┌─┴─┐  │    │         │   ░   │    │  ┌───┐│     ││       ││       │ ░ ┌───┐  │    │  ┌─┴─┐
           s_0: ┤ X ├──┼────┼─────────┼───░───■────┼──┤ X ├┤     ├┤       ├┤1      ├─░─┤ X ├──┼────┼──┤ X ├
                └───┘┌─┴─┐  │         │   ░        │  ├───┤│  Ps ││       │└───────┘ ░ ├───┤  │  ┌─┴─┐└───┘
           s_1: ─────┤ X ├──┼─────────┼───░────────■──┤ X ├┤     ├┤1      ├──────────░─┤ X ├──┼──┤ X ├─────
                     └───┘┌─┴─┐       │   ░           └───┘│     │└───────┘          ░ └───┘┌─┴─┐└───┘     
           s_2: ──────────┤ X ├───────┼───░────────────────┤1    ├───────────────────░──────┤ X ├──────────
                          └───┘     ┌─┴─┐ ░                │     │                   ░      └───┘          
          cout: ────────────────────┤ X ├─░────────────────┤2    ├───────────────────░─────────────────────
                                    └───┘ ░                └─────┘                   ░                     

    **References:**

    [1] Thapliyal et al., Design of Efficient Reversible Logic-Based Binary and BCD Adder Circuits, 2017.
    `arXiv:1712.02630 <https://arxiv.org/pdf/arXiv:1712.02630.pdf>`_

    [2] Cuccaro et al., A new quantum ripple-carry addition circuit, 2004.
    `arXiv:quant-ph/0410184 <https://arxiv.org/pdf/quant-ph/0410184.pdf>`_
    
    """
    
    def __init__(
        self, num_state_qubits: int, has_cout: Optional[bool] = True, 
        name: Optional[str] = "FullAdd"
    ) -> None:
        r"""
        Args:
            num_state_qubits: The number of qubits in either input register for
                state :math:`|a\rangle` or :math:`|s\rangle`. The two input
                registers must have the same number of qubits.
            has_cout: If the adder should include a carry-out.
            name: The name of the circuit object.
        Raises:
            ValueError: If ``num_state_qubits`` is lower than 1.
        """
        if num_state_qubits < 1:
            raise ValueError("The number of qubits must be greater than zero.")

        self._has_cout = has_cout
        super().__init__(num_state_qubits, name=name)
        
        qr_cin = QuantumRegister(1, "cin")
        qr_a = QuantumRegister(num_state_qubits, "a")
        qr_s = QuantumRegister(num_state_qubits, "s")
        self.add_register(qr_cin, qr_a, qr_s)
        
        if has_cout:
            qr_cout = QuantumRegister(1, "cout")
            self.add_register(qr_cout)
            
        # build adder circuit
        circuit = QuantumCircuit(*self.qregs, name=name)        

        # pre CNOT gates
        for i in range(num_state_qubits):
            circuit.cx(qr_a[i], qr_s[i])
        circuit.cx(qr_a[0], qr_cin)
        for i in range(num_state_qubits - 1):
            circuit.cx(qr_a[i+1], qr_a[i])
        if has_cout:
            circuit.cx(qr_a[-1], qr_cout)

        # main ripple-carry part
        if num_state_qubits > 1:
            circuit.ccx(qr_cin, qr_s[0], qr_a[0])
        for i in range(num_state_qubits - 2):
            circuit.ccx(qr_a[i], qr_s[i+1], qr_a[i+1])

        for i in range(num_state_qubits-1):
            circuit.x(qr_s[i])
        if num_state_qubits == 1:
            if has_cout:
                circuit.append(PeresGate(), [qr_cin, qr_s, qr_cout])
            else:
                circuit.cx(qr_cin, qr_s)
        else:
            if has_cout:
                circuit.append(PeresGate(), [qr_a[-2], qr_s[-1], qr_cout])
            else:
                circuit.cx(qr_a[-2], qr_s[-1])

        for i in reversed(range(0, num_state_qubits-2)):
            circuit.append(PeresdgGate(), [qr_a[i], qr_s[i+1], qr_a[i+1]])
        if num_state_qubits > 1:
            circuit.append(PeresdgGate(), [qr_cin, qr_s[0], qr_a[0]])

        # post CNOT gates
        for i in range(num_state_qubits-1):
            circuit.x(qr_s[i])
        for i in reversed(range(num_state_qubits - 1)):
            circuit.cx(qr_a[i+1], qr_a[i])
        circuit.cx(qr_a[0], qr_cin)
        for i in reversed(range(num_state_qubits)):
            circuit.cx(qr_a[i], qr_s[i])
            
        self.append(circuit.to_gate(), self.qubits)

    @property
    def has_cout(self) -> bool:
        """Checks if the adder has a carry-out.
        Returns:
            bool: If the adder has a carry_out.
        """
        return self._has_cout
