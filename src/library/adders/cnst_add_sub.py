"""Constant adder subtractor decided by a control, based on a full adder and using 
bitwise complements."""

from qiskit.circuit import QuantumCircuit, QuantumRegister, AncillaRegister
from qiskit.circuit.library.arithmetic.adders.adder import Adder
from ..gates.peres import PeresGate, PeresdgGate
from typing import Optional, Tuple
from math import log2, ceil
import numpy


class CnstAddSub(Adder):
    r"""A constant optimized version of the adder-subtractor proposed by 
    Thapliyal (approach 3 in the paper), based on a full adder. 
    The carry-in is used as a control for implicitly negating the :math:`cnst` 
    (and :math:`cout`) register(s), as well as serving as a +1 for the subtracton: 
    :math:`s + \overline{cnst} + 1 = s - cnst`.

    Constant optimization follows from converting the intial and last sections 
    to CNOT gates on the :math:`s` register and NOT gates.

    There are two main versions, with and without the carry-out qubit.
    Additionally, if the adder is part of a ripple chain (integer devision),
    the previous CNOT ending section and current CNOT inital section can be 
    combined into a single CNOT section if the previous carry-in differs 
    from the current carry-in or none at all if they are equal (expection being
    the first and last qubit of the :math:`s` register).

    Example circuit for cnst=6 with carry-out:
    .. parsed-literal::

           cin: ───────■─────────■─────────■─────────■───────────────────────────────────────────────────────────────■────■────■────■──
                ┌───┐  │  ┌───┐  │         │         │                                 ┌───────┐     ┌───────┐┌───┐  │    │    │    │  
        cnst_0: ┤ X ├──┼──┤ X ├──┼─────────┼────■────┼─────────────────────────────────┤0      ├─────┤2      ├┤ X ├──┼────┼────┼────┼──
                └───┘  │  └─┬─┘  │         │  ┌─┴─┐  │                        ┌───────┐│       │     │       │└───┘  │    │    │    │  
        cnst_1: ───────┼────┼────┼─────────┼──┤ X ├──┼─────────■──────────────┤0      ├┤2      ├─────┤       ├───────┼────┼────┼────┼──
                ┌───┐  │    │    │         │  └─┬─┘  │       ┌─┴─┐     ┌─────┐│       ││       │┌───┐│       │       │    │    │    │  
        cnst_2: ┤ X ├──┼────┼────┼─────────┼────┼────┼───────┤ X ├─────┤0    ├┤2      ├┤       ├┤ X ├┤  Psdg ├───────┼────┼────┼────┼──
                └───┘  │    │    │         │    │    │       └─┬─┘     │     ││       ││  Psdg │└───┘│       │       │    │    │    │  
        cnst_3: ───────┼────■────┼─────────┼────┼────┼─────────┼───────┤     ├┤       ├┤       ├─────┤0      ├───────┼────┼────┼────┼──
                     ┌─┴─┐  │    │  ┌───┐  │    │    │         │       │     ││  Psdg ││       │     │       │┌───┐┌─┴─┐  │    │    │  
           s_0: ─────┤ X ├──■────┼──┤ X ├──┼────┼────┼─────────┼───────┤     ├┤       ├┤       ├─────┤1      ├┤ X ├┤ X ├──┼────┼────┼──
                     └───┘     ┌─┴─┐├───┤  │    │    │  ┌───┐  │       │     ││       ││       │     └───────┘└───┘└───┘┌─┴─┐  │    │  
           s_1: ───────────────┤ X ├┤ X ├──┼────■────┼──┤ X ├──┼───────┤  Ps ├┤       ├┤1      ├────────────────────────┤ X ├──┼────┼──
                               └───┘└───┘┌─┴─┐┌───┐  │  └───┘  │  ┌───┐│     ││       │└───────┘                        └───┘┌─┴─┐  │  
           s_2: ─────────────────────────┤ X ├┤ X ├──┼─────────■──┤ X ├┤     ├┤1      ├──────────────────────────────────────┤ X ├──┼──
                                         └───┘└───┘┌─┴─┐          └───┘│     │└───────┘                                      └───┘┌─┴─┐
           s_3: ───────────────────────────────────┤ X ├───────────────┤1    ├────────────────────────────────────────────────────┤ X ├
                                                   └───┘               │     │                                                    └───┘
          cout: ───────────────────────────────────────────────────────┤2    ├─────────────────────────────────────────────────────────
                                                                       └─────┘                                                         

    **References:**

    [1] Thapliyal, Mapping of Subtractor and Adder-Subtractor Circuits on Reversible Quantum Gates, 2016. 
    `dx.doi.org/10.1007/978-3-662-50412-3_2 <http://dx.doi.org/10.1007/978-3-662-50412-3_2>`_

    [2] Thapliyal et al., Design of Efficient Reversible Logic-Based Binary and BCD Adder Circuits, 2017.
    `arXiv:1712.02630 <https://arxiv.org/pdf/arXiv:1712.02630.pdf>`_

    [3] Thapliyal et al., Quantum Circuit Designs of Integer Division Optimizing T-count and T-depth, 2018.
    arXiv:1809.09732 <https://arxiv.org/pdf/arXiv:1809.09732.pdf>`_

    """

    def __init__(
        self, constant: int, num_state_qubits: Optional[int] = None,
        has_cout: Optional[bool] = True, chain: Optional[str] = None,
        upward_chain: Optional[bool] = True,  name: Optional[str] = "CnstAddSub"
    ) -> None:
        r"""
        Args:
            constant: The constant number to add or subtract, depending on the carry-in.
            num_state_qubits: The number of qubits in either input register for
                state :math:`|cnst\rangle` or :math:`|s\rangle`. The two input
                registers must have the same number of qubits.
            has_cout: If the adder-subtractor should include a carry-out.
            chain: Position on the chain if the adder-subtractor is part of a ripple 
                chain of multiple adder-subtractors. Can be None if not on a chain 
                or ``start``, ``inbetween``and ``end`` if on the chain.
            upward_chain: If the chain is moving upward in relation to the qubit order.
            name: The name of the circuit object.
        Raises:
            ValueError: If ``constant`` is lower than 1.
            ValueError: If ``num_state_qubits`` is not big enough to hold the constant.
        """
        if constant < 1:
            raise ValueError("The constant number to add / subtract must be at least 1.")

        min_num_sq = ceil(log2(constant + 1))
        if num_state_qubits is None:
            num_state_qubits = min_num_sq
        elif num_state_qubits < min_num_sq:
            raise ValueError(
                f"The number of qubits must be at least {min_num_sq} to fit the constant.")

        self._constant = constant
        self._has_cout = has_cout
        super().__init__(num_state_qubits, name=name)

        xgate_offset = 0
        if chain == "inbetween" or chain == "end":
            qr_pcin = QuantumRegister(1, "pcin")
            self.add_register(qr_pcin)
            xgate_offset = 1

        qr_cin = QuantumRegister(1, "cin")
        qr_cnst = AncillaRegister(num_state_qubits, "cnst")
        qr_s = QuantumRegister(num_state_qubits, "s")
        self.add_register(qr_cin, qr_cnst, qr_s)

        if has_cout:
            qr_cout = QuantumRegister(1, "cout")
            self.add_register(qr_cout)

        # build adder circuit
        circuit = QuantumCircuit(*self.qregs, name=name)
        pre_xgates, post_xgates = self._precalc_xgates()

        # pre CNOT gates
        if chain == "inbetween" or chain == "end":
            if upward_chain:
                new_index = 0
            else:
                new_index = -1
            # combined CNOT gates with previous ending segmenent
            circuit.cx(qr_pcin, qr_s[new_index])
            circuit.cx(qr_cin, qr_s)
            circuit.cx(qr_pcin, qr_cin)
        else:
            circuit.cx(qr_cin, qr_s)

        # 2 CNOT gates on qr_cout controlled by qr_cin cancel each other out here

        # pre NOT gates
        for idx in range(self.num_qubits):
            if pre_xgates[idx] == 1:
                circuit.x(idx + xgate_offset)

        # main ripple-carry part, using the last bit in qr_cnst instead of cin
        if num_state_qubits > 1:
            circuit.ccx(qr_cnst[-1], qr_s[0], qr_cnst[0])
        for i in range(num_state_qubits - 2):
            circuit.ccx(qr_cnst[i], qr_s[i + 1], qr_cnst[i + 1])

        for i in range(num_state_qubits - 1):
            circuit.x(qr_s[i])

        # if cout exists, append Peres gate (CCNOT on cout + CNOT) otherwise only append CNOT
        if has_cout:
            if num_state_qubits == 1:
                circuit.append(PeresGate(), [qr_cnst, qr_s, qr_cout])
            else:
                circuit.append(PeresGate(), [qr_cnst[-2], qr_s[-1], qr_cout])
        else:
            if num_state_qubits == 1:
                circuit.cx(qr_cnst, qr_s)
            else:
                circuit.cx(qr_cnst[-2], qr_s[-1])

        for i in reversed(range(0, num_state_qubits-2)):
            circuit.append(PeresdgGate(), [
                           qr_cnst[i], qr_s[i + 1], qr_cnst[i + 1]])
        if num_state_qubits > 1:
            circuit.append(PeresdgGate(), [qr_cnst[-1], qr_s[0], qr_cnst[0]])

        # post NOT gates
        for idx in range(self.num_qubits):
            if post_xgates[idx] == 1:
                circuit.x(idx + xgate_offset)

        # post CNOT gates
        if chain is None or chain == "end":
            circuit.cx(qr_cin, qr_s)

        self.append(circuit.to_gate(), self.qubits)

    @property
    def constant(self) -> int:
        """The constant number to add or subtract.
        Returns:
            The constant.
        """
        return self._constant

    @property
    def has_cout(self) -> bool:
        """Checks if the adder has a carry-out.
        Returns:
            bool: If the adder has a carry_out.
        """
        return self._has_cout

    def _precalc_xgates(self) -> Tuple[numpy.ndarray, numpy.ndarray]:
        """Constant adding simplifies the ripple-carry adder in the 
        sections before the Toffoli gates and after the inverted Peres gates.
        The highest qubit of the cnst register gets set to the lowest bit of the constant.

        Returns:
            Tuple[numpy.ndarray, numpy.ndarray]: Binary arrays, representing NOT gate positions.
        """

        bin_repr = bin(self._constant)[2:]
        bin_repr = "0" * (self.num_state_qubits - len(bin_repr)) + bin_repr
        pre_xgates = numpy.zeros(2*self.num_state_qubits + 2, dtype=int)

        # qr_s
        for idx, bit in enumerate(reversed(bin_repr)):
            pre_xgates[idx + self.num_state_qubits + 1] = int(bit)

        # qr_cnst[0] to qr_cnst[-2]
        for idx in range(self.num_state_qubits - 1):
            pre_xgates[idx + 1] = (pre_xgates[idx + self.num_state_qubits + 1]
                                   + pre_xgates[idx + self.num_state_qubits + 2]) % 2

        # qr_cnst_n-1
        pre_xgates[self.num_state_qubits] = pre_xgates[self.num_state_qubits + 1]

        post_xgates = pre_xgates.copy()

        # pre: qr_cout
        pre_xgates[-1] = pre_xgates[-2]

        # post: qr_s[0] to qr_s[-2] (partial inversion)
        for idx in range(self.num_state_qubits - 1):
            post_xgates[idx + self.num_state_qubits + 1] = 1 - \
                post_xgates[idx + self.num_state_qubits + 1]

        return pre_xgates, post_xgates
