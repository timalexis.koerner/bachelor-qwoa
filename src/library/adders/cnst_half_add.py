"""Constant half ripple-carry adder."""

from typing import Optional, Tuple
from math import log2, ceil
import numpy
from qiskit.circuit import QuantumCircuit, QuantumRegister, AncillaRegister
from qiskit.circuit.library.arithmetic.adders.adder import Adder
from ..gates.peres import PeresGate


class CnstHalfAdd(Adder):
    r"""A constant optimized version of the half ripple-carry adder proposed by 
    Thapliyal and Ranganathan.
    
    Constant optimization follows from converting the intial and last sections to 
    only NOT gates and starting/ending the core ripple-carry part only below the 
    first nonzero bit of the constant. At the first nonzero bit, CNOT and CNOT+NOT
    gates are used instead of CCNOT and Peres gates. If there is no carry-out, 
    the highest bit Peres gate will become a CNOT gate, commuting with the possible 
    NOT gates on the highest qubit of :math:`s` (thereby cancelling them).
    All unused qubits of the :math:`cnst` register are removed.
    
    There are two versions, with and without the carry-out qubit.
    
    Example circuit for cnst=6:
    .. parsed-literal::
                                    
                   ┌───┐┌─────┐┌───┐
        cnst: ─────┤ X ├┤0    ├┤ X ├
                   └─┬─┘│     │└─┬─┘
         s_0: ───────┼──┤     ├──┼──
              ┌───┐  │  │     │  │  
         s_1: ┤ X ├──■──┤  Ps ├──■──
              ├───┤     │     │┌───┐
         s_2: ┤ X ├─────┤1    ├┤ X ├
              ├───┤     │     │└───┘
        cout: ┤ X ├─────┤2    ├─────
              └───┘     └─────┘     
            
    Unoptimized (non constant) version, split into sections:
    .. parsed-literal::

                                     ░                         ┌─────┐ ░                
        cnst_0: ─────────────────────░───■─────────────────────┤0    ├─░────────────────
                                     ░ ┌─┴─┐            ┌─────┐│     │ ░                
        cnst_1: ──■──────────────■───░─┤ X ├──■─────────┤0    ├┤2    ├─░───■────■───────
                  │            ┌─┴─┐ ░ └─┬─┘┌─┴─┐┌─────┐│     ││  Ps │ ░ ┌─┴─┐  │       
        cnst_2: ──┼────■────■──┤ X ├─░───┼──┤ X ├┤0    ├┤2    ├┤     ├─░─┤ X ├──┼────■──
                  │    │    │  └───┘ ░   │  └─┬─┘│     ││  Ps ││     │ ░ └───┘  │    │  
           s_0: ──┼────┼────┼────────░───■────┼──┤     ├┤     ├┤1    ├─░────────┼────┼──
                ┌─┴─┐  │    │        ░        │  │     ││     │└─────┘ ░      ┌─┴─┐  │  
           s_1: ┤ X ├──┼────┼────────░────────■──┤  Ps ├┤1    ├────────░──────┤ X ├──┼──
                └───┘┌─┴─┐  │        ░           │     │└─────┘        ░      └───┘┌─┴─┐
           s_2: ─────┤ X ├──┼────────░───────────┤1    ├───────────────░───────────┤ X ├
                     └───┘┌─┴─┐      ░           │     │               ░           └───┘
          cout: ──────────┤ X ├──────░───────────┤2    ├───────────────░────────────────
                          └───┘      ░           └─────┘               ░                

    **References:**

    [1] Thapliyal et al., Design of Efficient Reversible Logic-Based Binary and BCD Adder Circuits, 2017.
    `arXiv:1712.02630 <https://arxiv.org/pdf/arXiv:1712.02630.pdf>`_
    
    """
    
    def __init__(
        self, constant: int, num_state_qubits: Optional[int] = None, 
        has_cout: Optional[bool] = True, name: Optional[str] = "CnstHalfAdd"
    ) -> None:
        r"""
        Args:
            constant: The constant number to add. 
            num_state_qubits: The number of qubits in the :math:`|s\rangle` register.
            has_cout: If the adder should include a carry-out.
            name: The name of the circuit object.
        Raises:
            ValueError: If ``constant`` is lower than 1.
            ValueError: If ``num_state_qubits`` is not big enough to hold the constant.
        """
        if constant < 1:
            raise ValueError("The constant number must be at least 1.")

        min_num_sq = ceil(log2(constant + 1))
        if num_state_qubits is None:
            num_state_qubits = min_num_sq
        elif num_state_qubits < min_num_sq:
            raise ValueError(
                f"The number of state qubits must be at least {min_num_sq} to fit the constant.")

        self._constant = constant
        self._has_cout = has_cout
        super().__init__(num_state_qubits, name=name)
        
        # precalc NOT gates
        pre_xgates, post_xgates = self._precalc_xgates()
        # commuting NOT gates for no cout
        if not has_cout:
            pre_xgates[-2] = 0
            post_xgates[-2] = 0
        
        # find lowest set cnst bit
        lowest_set_bit = pre_xgates.argmax()
        # s gets another NOT at the lowest set bit (later check only for equal 1)
        post_xgates[lowest_set_bit + num_state_qubits] += 1
        # reduced cnst register
        self._num_constant_qubits = num_state_qubits - lowest_set_bit - 1
        pre_xgates = pre_xgates[lowest_set_bit + 1:]
        post_xgates = post_xgates[lowest_set_bit + 1:]
        
        # define registers
        qr_cnst = AncillaRegister(self._num_constant_qubits, "cnst")
        qr_s = QuantumRegister(num_state_qubits, "s")
        self.add_register(qr_cnst, qr_s)
        
        if has_cout:
            qr_cout = QuantumRegister(1, "cout")
            self.add_register(qr_cout)
            
        # build adder circuit
        circuit = QuantumCircuit(*self.qregs, name=name)

        # pre NOT gates
        for idx in range(self.num_qubits):
            if pre_xgates[idx] == 1:
                circuit.x(idx)
        
        # main ripple-carry part
        # start from the lowest set bit
        if lowest_set_bit < num_state_qubits - 1:
            circuit.cx(qr_s[lowest_set_bit], qr_cnst[0])
        for i in range(lowest_set_bit + 1, num_state_qubits - 1):
            circuit.ccx(qr_cnst[i - lowest_set_bit - 1], qr_s[i], qr_cnst[i - lowest_set_bit])

        # if cout exists, append Peres gate (CCNOT on cout + CNOT) otherwise only append CNOT
        if has_cout:
            if lowest_set_bit == num_state_qubits - 1:
                circuit.cx(qr_s[-1], qr_cout)
            else:
                circuit.append(PeresGate(), [qr_cnst[-1], qr_s[-1], qr_cout])
        elif lowest_set_bit != num_state_qubits - 1:
            circuit.cx(qr_cnst[-1], qr_s[-1])

        # end at the lowest set bit
        for i in reversed(range(lowest_set_bit + 1, num_state_qubits - 1)):
            circuit.append(PeresGate(), [qr_cnst[i - lowest_set_bit - 1], qr_s[i], qr_cnst[i - lowest_set_bit]])
        if lowest_set_bit < num_state_qubits - 1:
            circuit.cx(qr_s[lowest_set_bit], qr_cnst[0])
        
        # post NOT gates
        for idx in range(self.num_qubits):
            if post_xgates[idx] == 1:
                circuit.x(idx)
                
        self.append(circuit.to_gate(), self.qubits)
        
    @property
    def constant(self) -> int:
        """The constant number.
        Returns:
            The constant.
        """
        return self._constant
    
    @property
    def num_constant_qubits(self) -> int:
        """The constant register size.
        Returns:
            The number of qubits in the constant register.
        """
        return self._num_constant_qubits
    
    @property
    def has_cout(self) -> bool:
        """Checks if the adder has a carry-out.
        Returns:
            bool: If the adder has a carry_out.
        """
        return self._has_cout

        
    def _precalc_xgates(self) -> Tuple[numpy.ndarray, numpy.ndarray]:
        """Constant adding simplifies the ripple-carry adder in the 
        sections before the Toffoli gates and after the Peres gates.

        Returns:
            Tuple[numpy.ndarray, numpy.ndarray]: Binary arrays, representing NOT gate positions.
        """
        
        bin_repr = bin(self._constant)[2:]
        bin_repr = "0" * (self.num_state_qubits - len(bin_repr)) + bin_repr
        pre_xgates = numpy.zeros(2*self.num_state_qubits + 1, dtype=int)
        
        # qr_s[1] to qr_s[-1]
        for idx, bit in enumerate(reversed(bin_repr[:-1])):
            pre_xgates[idx + self.num_state_qubits + 1] = int(bit)
            
        # qr_cnst[0] and qr_cnst[1]
        pre_xgates[0] = int(bin_repr[-1])
        if self.num_state_qubits > 1:
            pre_xgates[1] = int(bin_repr[-2])
        
        # qr_cnst[2] to qr_cnst[-1]
        for idx in range(2, self.num_state_qubits):
            pre_xgates[idx] = (pre_xgates[idx + self.num_state_qubits - 1] 
                                   + pre_xgates[idx + self.num_state_qubits]) % 2

        post_xgates = pre_xgates.copy()

        # pre: qr_cout
        pre_xgates[-1] = pre_xgates[-2]
        
        return pre_xgates, post_xgates
