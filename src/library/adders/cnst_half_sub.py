"""Constant half subtractor, using a half ripple-carry adder and bitwise complements."""

from typing import Optional, Tuple
import numpy
from .cnst_half_add import CnstHalfAdd


class CnstHalfSub(CnstHalfAdd):
    r"""A constant optimized subtractor version of the half ripple-carry adder 
    proposed by Thapliyal and Ranganathan.
    
    For constant optimization see CnstHalfAdd documentation. 
    
    The subtractor is build from an adder by using the bitwise complement for 
    signed integers, i.e NOT gates on the :math:`s` register before and after 
    the adder circuit. While the carry-out should also gain these NOT gates, 
    they commute with the Peres gate and therefor cancel each other.
        
    There are two versions, with and without the carry-out qubit.
    
    Example circuit for cnst=6:
    .. parsed-literal::                   
                                        
                     ┌───┐     ┌─────┐┌───┐     
        cnst_0: ─────┤ X ├─────┤0    ├┤ X ├─────
                ┌───┐└─┬─┘┌───┐│     │└─┬─┘     
           s_0: ┤ X ├──┼──┤ X ├┤     ├──┼───────
                └───┘  │  └───┘│     │  │  ┌───┐
           s_1: ───────■───────┤  Ps ├──■──┤ X ├
                               │     │     └───┘
           s_2: ───────────────┤1    ├──────────
                     ┌───┐     │     │          
          cout: ─────┤ X ├─────┤2    ├──────────
                     └───┘     └─────┘          

    **References:**

    [1] Thapliyal et al., Design of Efficient Reversible Logic-Based Binary and BCD Adder Circuits, 2017.
    `arXiv:1712.02630 <https://arxiv.org/pdf/arXiv:1712.02630.pdf>`_
    
    """
    
    def __init__(
        self, constant: int, num_state_qubits: Optional[int] = None, 
        has_cout: Optional[bool] = True, name: Optional[str] = "CnstHalfSub"
    ) -> None:
        r"""
        Args:
            constant: The constant number to subtract. 
            num_state_qubits: The number of qubits in the :math:`|s\rangle` register.
            has_cout: If the subtractor should include a carry-out.
            name: The name of the circuit object.
        Raises:
            ValueError: If ``constant`` is lower than 1.
            ValueError: If ``num_state_qubits`` is not big enough to hold the constant.
        """
        super().__init__(
            constant, 
            num_state_qubits=num_state_qubits, 
            has_cout=has_cout, 
            name=name
        )

        
    def _precalc_xgates(self) -> Tuple[numpy.ndarray, numpy.ndarray]:
        r"""Constant adding simplifies the ripple-carry adder in the 
        sections before the Toffoli gates and after the Peres gates.
        The subtraction adds additional NOT gates on the :math:`s` 
        register, pre and post core circuit.

        Returns:
            Tuple[numpy.ndarray, numpy.ndarray]: Binary arrays, representing NOT gate positions.
        """
        
        pre_xgates, _ = super()._precalc_xgates()

        # qr_s additonal NOTs due to subtraction
        for idx in range(self.num_state_qubits, 2*self.num_state_qubits):
            pre_xgates[idx] = 1 - pre_xgates[idx]

        post_xgates = pre_xgates.copy()

        # post: no qr_cout (only for pre)
        post_xgates[-1] = 0
        
        return pre_xgates, post_xgates
