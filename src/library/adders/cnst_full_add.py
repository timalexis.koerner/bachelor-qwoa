"""Constant full ripple-carry adder."""

from qiskit.circuit import QuantumCircuit, QuantumRegister, AncillaRegister
from qiskit.circuit.library.arithmetic.adders.adder import Adder
from ..gates.peres import PeresGate, PeresdgGate
from typing import Optional, Tuple
from math import log2, ceil
import numpy


class CnstFullAdd(Adder):
    r"""A constant optimized version of the full ripple-carry adder proposed by 
    Thapliyal and Ranganathan, which is closely related to the adder proposed by 
    Curccaro et al..

    Constant optimization follows from converting the intial and last sections to 
    only NOT gates.

    There are two versions, with and without the carry-out qubit, carry-in is always included.

    Example circuit for cnst=6:
    .. parsed-literal::

                                                         ┌───────┐     
           cin: ───────■─────────────────────────────────┤0      ├─────
                ┌───┐┌─┴─┐                      ┌───────┐│       │┌───┐
        cnst_0: ┤ X ├┤ X ├───────■──────────────┤0      ├┤2      ├┤ X ├
                └───┘└─┬─┘     ┌─┴─┐     ┌─────┐│       ││       │└───┘
        cnst_1: ───────┼───────┤ X ├─────┤0    ├┤2      ├┤  Psdg ├─────
                       │       └─┬─┘     │     ││       ││       │     
        cnst_2: ───────┼─────────┼───────┤     ├┤  Psdg ├┤       ├─────
                       │  ┌───┐  │       │     ││       ││       │┌───┐
           s_0: ───────■──┤ X ├──┼───────┤     ├┤       ├┤1      ├┤ X ├
                ┌───┐     └───┘  │  ┌───┐│  Ps ││       │└───────┘└───┘
           s_1: ┤ X ├────────────■──┤ X ├┤     ├┤1      ├──────────────
                ├───┤               └───┘│     │└─┬───┬─┘              
           s_2: ┤ X ├────────────────────┤1    ├──┤ X ├────────────────
                ├───┤                    │     │  └───┘                
          cout: ┤ X ├────────────────────┤2    ├───────────────────────
                └───┘                    └─────┘                       

    Unoptimized (non constant) version, split into sections:
    .. parsed-literal::

                     ┌───┐                ░                                ┌───────┐ ░           ┌───┐     
           cin: ─────┤ X ├────────────────░───■────────────────────────────┤0      ├─░───────────┤ X ├─────
                     └─┬─┘┌───┐           ░ ┌─┴─┐                 ┌───────┐│       │ ░      ┌───┐└─┬─┘     
        cnst_0: ──■────■──┤ X ├───────────░─┤ X ├──■──────────────┤0      ├┤2      ├─░──────┤ X ├──■────■──
                  │       └─┬─┘┌───┐      ░ └─┬─┘┌─┴─┐     ┌─────┐│       ││       │ ░ ┌───┐└─┬─┘       │  
        cnst_1: ──┼────■────■──┤ X ├──────░───┼──┤ X ├─────┤0    ├┤2      ├┤  Psdg ├─░─┤ X ├──■────■────┼──
                  │    │       └─┬─┘      ░   │  └─┬─┘     │     ││       ││       │ ░ └─┬─┘       │    │  
        cnst_2: ──┼────┼────■────■────■───░───┼────┼───────┤     ├┤  Psdg ├┤       ├─░───■────■────┼────┼──
                ┌─┴─┐  │    │         │   ░   │    │  ┌───┐│     ││       ││       │ ░ ┌───┐  │    │  ┌─┴─┐
           s_0: ┤ X ├──┼────┼─────────┼───░───■────┼──┤ X ├┤     ├┤       ├┤1      ├─░─┤ X ├──┼────┼──┤ X ├
                └───┘┌─┴─┐  │         │   ░        │  ├───┤│  Ps ││       │└───────┘ ░ ├───┤  │  ┌─┴─┐└───┘
           s_1: ─────┤ X ├──┼─────────┼───░────────■──┤ X ├┤     ├┤1      ├──────────░─┤ X ├──┼──┤ X ├─────
                     └───┘┌─┴─┐       │   ░           └───┘│     │└───────┘          ░ └───┘┌─┴─┐└───┘     
           s_2: ──────────┤ X ├───────┼───░────────────────┤1    ├───────────────────░──────┤ X ├──────────
                          └───┘     ┌─┴─┐ ░                │     │                   ░      └───┘          
          cout: ────────────────────┤ X ├─░────────────────┤2    ├───────────────────░─────────────────────
                                    └───┘ ░                └─────┘                   ░                     

    **References:**

    [1] Thapliyal et al., Design of Efficient Reversible Logic-Based Binary and BCD Adder Circuits, 2017.
    `arXiv:1712.02630 <https://arxiv.org/pdf/arXiv:1712.02630.pdf>`_

    [2] Cuccaro et al., A new quantum ripple-carry addition circuit, 2004.
    `arXiv:quant-ph/0410184 <https://arxiv.org/pdf/quant-ph/0410184.pdf>`_

    """

    def __init__(
        self, constant: int, num_state_qubits: Optional[int] = None,
        has_cout: Optional[bool] = True, name: Optional[str] = "CnstFullAdd"
    ) -> None:
        r"""
        Args:
            constant: The constant number to add. 
            num_state_qubits: The number of qubits in either input register for
                state :math:`|cnst\rangle` or :math:`|s\rangle`. The two input
                registers must have the same number of qubits.
            has_cout: If the adder should include a carry-out.
            name: The name of the circuit object.
        Raises:
            ValueError: If ``constant`` is lower than 0.
            ValueError: If ``num_state_qubits`` is not big enough to hold the constant.
        """
        if constant < 0:
            raise ValueError("The constant number to add must be at least 0.")

        min_num_sq = ceil(log2(constant + 1))
        if num_state_qubits is None:
            num_state_qubits = min_num_sq
        elif num_state_qubits < min_num_sq:
            raise ValueError(
                f"The number of qubits must be at least {min_num_sq} to fit the constant.")

        self._constant = constant
        self._has_cout = has_cout
        super().__init__(num_state_qubits, name=name)

        qr_cin = QuantumRegister(1, "cin")
        self.add_register(qr_cin)
        if num_state_qubits != 0:
            qr_cnst = AncillaRegister(num_state_qubits, "cnst")
            qr_s = QuantumRegister(num_state_qubits, "s")
            self.add_register(qr_cnst, qr_s)
        if has_cout:
            qr_cout = QuantumRegister(1, "cout")
            self.add_register(qr_cout)

        if num_state_qubits == 0:
            if has_cout:
                self.cx(qr_cin, qr_cout)
            return

        # build adder circuit
        circuit = QuantumCircuit(*self.qregs, name=name)
        pre_xgates, post_xgates = self._precalc_xgates()

        # pre NOT gates
        for idx in range(self.num_qubits):
            if pre_xgates[idx] == 1:
                circuit.x(idx)

        # main ripple-carry part
        if num_state_qubits > 1:
            circuit.ccx(qr_cin, qr_s[0], qr_cnst[0])
        for i in range(num_state_qubits - 2):
            circuit.ccx(qr_cnst[i], qr_s[i + 1], qr_cnst[i + 1])

        for i in range(num_state_qubits - 1):
            circuit.x(qr_s[i])

        # if cout exists, append Peres gate (CCNOT on cout + CNOT) otherwise only append CNOT
        if has_cout:
            if num_state_qubits == 1:
                circuit.append(PeresGate(), [qr_cin, qr_s, qr_cout])
            else:
                circuit.append(PeresGate(), [qr_cnst[-2], qr_s[-1], qr_cout])
        else:
            if num_state_qubits == 1:
                circuit.cx(qr_cin, qr_s)
            else:
                circuit.cx(qr_cnst[-2], qr_s[-1])

        for i in reversed(range(0, num_state_qubits-2)):
            circuit.append(PeresdgGate(),
                           [qr_cnst[i], qr_s[i + 1], qr_cnst[i + 1]])
        if num_state_qubits > 1:
            circuit.append(PeresdgGate(), [qr_cin, qr_s[0], qr_cnst[0]])

        # post NOT gates
        for idx in range(self.num_qubits):
            if post_xgates[idx] == 1:
                circuit.x(idx)

        self.append(circuit.to_gate(), self.qubits)

    @property
    def constant(self) -> int:
        """The constant number to add.
        Returns:
            The constant.
        """
        return self._constant
    
    @property
    def has_cout(self) -> bool:
        """Checks if the adder has a carry-out.
        Returns:
            bool: If the adder has a carry_out.
        """
        return self._has_cout

    def _precalc_xgates(self) -> Tuple[numpy.ndarray, numpy.ndarray]:
        """Constant adding simplifies the ripple-carry adder in the 
        sections before the Toffoli gates and after the inverted Peres gates.

        Returns:
            Tuple[numpy.ndarray, numpy.ndarray]: Binary arrays, representing NOT gate positions.
        """

        bin_repr = bin(self._constant)[2:]
        bin_repr = "0" * (self.num_state_qubits - len(bin_repr)) + bin_repr
        pre_xgates = numpy.zeros(2*self.num_state_qubits + 2, dtype=int)

        # qr_s
        for idx, bit in enumerate(reversed(bin_repr)):
            pre_xgates[idx + self.num_state_qubits + 1] = int(bit)

        # qr_cnst[0] to qr_cnst[-2]
        for idx in range(self.num_state_qubits - 1):
            pre_xgates[idx + 1] = (pre_xgates[idx + self.num_state_qubits + 1]
                                   + pre_xgates[idx + self.num_state_qubits + 2]) % 2

        # qr_cin
        pre_xgates[0] = pre_xgates[self.num_state_qubits + 1]

        post_xgates = pre_xgates.copy()

        # pre: qr_cout
        pre_xgates[-1] = pre_xgates[-2]

        # post: qr_s[0] to qr_s[-2] (partial inversion)
        for idx in range(self.num_state_qubits - 1):
            post_xgates[idx + self.num_state_qubits + 1] = 1 - \
                post_xgates[idx + self.num_state_qubits + 1]

        return pre_xgates, post_xgates
