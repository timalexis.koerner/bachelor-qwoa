"""Multicontrolled V-chain with borrowed qubits"""
 
from qiskit import QuantumCircuit
from qiskit.circuit.quantumregister import QuantumRegister, AncillaRegister
from qiskit.circuit.gate import Gate
from typing import Optional
import numpy


class MCXBorrowedVChain(Gate):
    r"""Creates a multicontrolled NOT gate with vertical
    CCNOT chaining.
    
    The gate is presented by Gidney in [1] and uses
    :math:`n-2` borrowed qubits for :math:`n` controls.
    The underlying circuit consists of a toggle detection
    reverse V-chain and uncomputation of the borrowed qubits.
    
    Example circuit for num_control_qubits=4:
    .. parsed-literal::
                                                    
            ctrl_0: ────────────■───────────────────■───────
                                │                   │       
            ctrl_1: ────────────■───────────────────■───────
                                │                   │       
            ctrl_2: ───────■────┼────■─────────■────┼────■──
                           │    │    │         │    │    │  
            ctrl_3: ──■────┼────┼────┼────■────┼────┼────┼──
                    ┌─┴─┐  │    │    │  ┌─┴─┐  │    │    │  
            target: ┤ X ├──┼────┼────┼──┤ X ├──┼────┼────┼──
                    └─┬─┘  │  ┌─┴─┐  │  └─┬─┘  │  ┌─┴─┐  │  
        borrowed_0: ──┼────■──┤ X ├──■────┼────■──┤ X ├──■──
                      │  ┌─┴─┐└───┘┌─┴─┐  │  ┌─┴─┐└───┘┌─┴─┐
        borrowed_1: ──■──┤ X ├─────┤ X ├──■──┤ X ├─────┤ X ├
                         └───┘     └───┘     └───┘     └───┘
      
    **References:**
    
    [1] Gidney, Reversible logic and quantum computers, 1985. 
    `Constructing Large Controlled Nots <https://algassert.com/circuits/2015/06/05/Constructing-Large-Controlled-Nots.html>`_
    
    """

    def __init__(self, num_control_qubits: int, label: Optional[str] = None):
        """Create a new MCX borrowed V-Chain."""
        if num_control_qubits < 1:
            raise ValueError("Number of control qubits should be at least 1.")
        
        self._num_control_qubits = num_control_qubits
        self._num_borrowed_qubits = max(0, num_control_qubits - 2)
        super().__init__("MCXBV", num_control_qubits + 1 + self._num_borrowed_qubits, [], label=label)

    def _define(self):
        """Multicontrol with toggle-detection and uncomputing."""
        regs = []
        if self._num_control_qubits > 0:
            qr_ctrl = QuantumRegister(self._num_control_qubits, "ctrl")
            regs.append(qr_ctrl)
        qr_target = QuantumRegister(1, "target")
        regs.append(qr_target)
        if self._num_borrowed_qubits > 0:
            qr_borrowed = AncillaRegister(self._num_borrowed_qubits, "borrowed")
            regs.append(qr_borrowed)
        qc = QuantumCircuit(*regs, name=self.name)
        
        if self._num_control_qubits == 0:
            qc.x(qr_target[0])
        elif self._num_control_qubits == 1:
            qc.cx(qr_ctrl[0], qr_target[0])
        else:       
            # interleaved qubit layout
            qubits = qr_ctrl[:2]
            for i in range(self._num_borrowed_qubits):
                qubits.append(qr_borrowed[i])
                qubits.append(qr_ctrl[2 + i])
            qubits.append(qr_target[0])
            
            # grouping for CCNOT gates of the form (control0, control1, target)
            qubit_groups = [qubits[2*i: 2*i + 3] for i in range(self._num_control_qubits - 1)]
            
            # toggle detection MCX
            for control0, control1, target in reversed(qubit_groups):
                qc.ccx(control0, control1, target)
            for control0, control1, target in qubit_groups[1:]:
                qc.ccx(control0, control1, target)
            
            # uncomputation of borrowed qubits
            for control0, control1, target in reversed(qubit_groups[:-1]):
                qc.ccx(control0, control1, target)
            for control0, control1, target in qubit_groups[1:-1]:
                qc.ccx(control0, control1, target)
        
        self.definition = qc

    def inverse(self):
        """Self inverse."""
        return MCXBorrowedVChain(self._num_control_qubits)

    def __array__(self, dtype=None):
        """Return a numpy.array for the MCX gate."""
        mcx_array = numpy.identity(2**(self._num_control_qubits + 1), dtype=dtype)
        switch_indices = (2**self._num_control_qubits - 1, -1)
        mcx_array[switch_indices[0], switch_indices[0]] = 0
        mcx_array[switch_indices[1], switch_indices[0]] = 1
        mcx_array[switch_indices[0], switch_indices[1]] = 1
        mcx_array[switch_indices[1], switch_indices[1]] = 0
        borrowed_q_array = numpy.identity(2**self._num_borrowed_qubits, dtype=dtype)        
        return numpy.kron(borrowed_q_array, mcx_array)
    
    @property
    def num_control_qubits(self) -> int:
        """The number of control qubits.
        Returns:
            The number of control qubits.
        """
        return self._num_control_qubits
    
    @property
    def num_borrowed_qubits(self) -> int:
        """Number of borrowed ancilla qubits.
        Returns:
            The number of borrowed qubits.
        """
        return self._num_borrowed_qubits