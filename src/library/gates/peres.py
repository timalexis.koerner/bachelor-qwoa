"""Implementation of the Peres gate and its inverse."""

from qiskit import QuantumCircuit
from qiskit.circuit.quantumregister import QuantumRegister
from qiskit.circuit.gate import Gate
from qiskit.circuit.library.standard_gates import HGate, CXGate, TGate, TdgGate
from typing import Optional
import numpy


class PeresGate(Gate):
    r"""Peres gate, identical to a Toffoli gate followed by an CNOT 
    gate on the two Toffoli control qubits.

    Effect:
      ┌────┐
    a ┤    ├ a
      |    |
    b ┤ Ps ├ a ⊕ b
      |    |
    c ┤    ├ ab ⊕ c
      └────┘
      
    **References:**
    
    [1] Peres, Reversible logic and quantum computers, 1985. 
    `doi.org/10.1103/PhysRevA.32.3266 <https://doi.org/10.1103/PhysRevA.32.3266>`_
    
    """

    def __init__(self, label: Optional[str] = None):
        """Create new Peres gate."""
        super().__init__("ps", 3, [], label=label)

    def _define(self):
        """
        gate ps a,b,c
        {
        h c; cx b,c; tdg c; cx a,c;
        t c; cx b,c; tdg c; cx a,c;
        t b; t c; h c; cx a,b;
        t a; tdg b;}
        """
        #                                                        ┌───┐
        # q_0: ───────────────────■─────────────────────■────■───┤ T ├─
        #                         │             ┌───┐   │  ┌─┴─┐┌┴───┴┐
        # q_1: ───────■───────────┼─────────■───┤ T ├───┼──┤ X ├┤ Tdg ├
        #      ┌───┐┌─┴─┐┌─────┐┌─┴─┐┌───┐┌─┴─┐┌┴───┴┐┌─┴─┐├───┤└┬───┬┘
        # q_2: ┤ H ├┤ X ├┤ Tdg ├┤ X ├┤ T ├┤ X ├┤ Tdg ├┤ X ├┤ T ├─┤ H ├─
        #      └───┘└───┘└─────┘└───┘└───┘└───┘└─────┘└───┘└───┘ └───┘
        q = QuantumRegister(3, "q")
        qc = QuantumCircuit(q, name=self.name)
        rules = [
            (HGate(), [q[2]], []),
            (CXGate(), [q[1], q[2]], []),
            (TdgGate(), [q[2]], []),
            (CXGate(), [q[0], q[2]], []),
            (TGate(), [q[2]], []),
            (CXGate(), [q[1], q[2]], []),
            (TdgGate(), [q[2]], []),
            (CXGate(), [q[0], q[2]], []),
            (TGate(), [q[1]], []),
            (TGate(), [q[2]], []),
            (HGate(), [q[2]], []),
            (CXGate(), [q[0], q[1]], []),
            (TGate(), [q[0]], []),
            (TdgGate(), [q[1]], [])
        ]
        for instr, qargs, cargs in rules:
            qc._append(instr, qargs, cargs)

        self.definition = qc

    def inverse(self):
        """Return a TR gate."""
        return PeresdgGate()

    def __array__(self, dtype=None):
        """Return a numpy.array for the Peres gate."""
        return numpy.array(
            [[1, 0, 0, 0, 0, 0, 0, 0],
             [0, 0, 0, 0, 0, 0, 0, 1],
             [0, 0, 1, 0, 0, 0, 0, 0],
             [0, 1, 0, 0, 0, 0, 0, 0],
             [0, 0, 0, 0, 1, 0, 0, 0],
             [0, 0, 0, 1, 0, 0, 0, 0],
             [0, 0, 0, 0, 0, 0, 1, 0],
             [0, 0, 0, 0, 0, 1, 0, 0]], dtype=dtype
        )


class PeresdgGate(Gate):
    r"""PeresdgGate (inverse Peres) gate, identical to a CNOT gate followed by a 
    Toffoli gate controlled by the two qubits used for the CNOT.

    Effect:
      ┌──────┐
    a ┤      ├ a
      |      |
    b ┤ Psdg ├ a ⊕ b
      |      |
    c ┤      ├ a(¬b) ⊕ c
      └──────┘
    
    **References:**
    
    [1] Peres, Reversible logic and quantum computers, 1985. 
    `doi.org/10.1103/PhysRevA.32.3266 <https://doi.org/10.1103/PhysRevA.32.3266>`_
    
    """

    def __init__(self, label: Optional[str] = None):
        """Create new inverse Peres gate."""
        super().__init__("psdg", 3, [], label=label)

    def _define(self):
        """
          gate psdg a,b,c
          {
          t b; tdg a;
          cx a,b; h c; tdg c; tdg b;
          cx a,c; t c; cx b,c; tdg c;
          cx a,c; t c; cx b,c; h c;}
        """
        #      ┌─────┐
        # q_0: ┤ Tdg ├───■─────■───────────────────────■─────────────────
        #      └┬───┬┘ ┌─┴─┐   │  ┌─────┐              │
        # q_1: ─┤ T ├──┤ X ├───┼──┤ Tdg ├──■───────────┼─────────■───────
        #       ├───┤ ┌┴───┴┐┌─┴─┐└┬───┬┘┌─┴─┐┌─────┐┌─┴─┐┌───┐┌─┴─┐┌───┐
        # q_2: ─┤ H ├─┤ Tdg ├┤ X ├─┤ T ├─┤ X ├┤ Tdg ├┤ X ├┤ T ├┤ X ├┤ H ├
        #       └───┘ └─────┘└───┘ └───┘ └───┘└─────┘└───┘└───┘└───┘└───┘
        q = QuantumRegister(3, "q")
        qc = QuantumCircuit(q, name=self.name)
        rules = [
            (TGate(), [q[1]], []),
            (TdgGate(), [q[0]], []),
            (CXGate(), [q[0], q[1]], []),
            (HGate(), [q[2]], []),
            (TdgGate(), [q[2]], []),
            (CXGate(), [q[0], q[2]], []),
            (TdgGate(), [q[1]], []),
            (TGate(), [q[2]], []),
            (CXGate(), [q[1], q[2]], []),
            (TdgGate(), [q[2]], []),
            (CXGate(), [q[0], q[2]], []),
            (TGate(), [q[2]], []),
            (CXGate(), [q[1], q[2]], []),
            (HGate(), [q[2]], [])
        ]
        for instr, qargs, cargs in rules:
            qc._append(instr, qargs, cargs)

        self.definition = qc

    def inverse(self):
        """Return a Peres gate."""
        return PeresGate()

    def __array__(self, dtype=None):
        """Return a numpy.array for the inverse Peres gate."""
        return numpy.array(
            [[1, 0, 0, 0, 0, 0, 0, 0],
             [0, 0, 0, 1, 0, 0, 0, 0],
             [0, 0, 1, 0, 0, 0, 0, 0],
             [0, 0, 0, 0, 0, 1, 0, 0],
             [0, 0, 0, 0, 1, 0, 0, 0],
             [0, 0, 0, 0, 0, 0, 0, 1],
             [0, 0, 0, 0, 0, 0, 1, 0],
             [0, 1, 0, 0, 0, 0, 0, 0]], dtype=dtype
        )
