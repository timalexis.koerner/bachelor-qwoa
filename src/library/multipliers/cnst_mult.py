"""Compute the product of two qubit registers using classical multiplication approach."""

from typing import Optional, Union
from math import log2, ceil
from qiskit.circuit import QuantumRegister, AncillaRegister, QuantumCircuit
from qiskit.circuit.library.arithmetic import IntegerComparator
from ..adders import CtrlCnstHalfAdd, CtrlPseudoAdd


class CnstMult(QuantumCircuit):
    r"""A multiplication circuit to store product of two input registers out-of-place.

    Multiplication in this circuit is implemented in a classical approach by performing
    a series of shifted additions [1] with a controlled constant half adder derived 
    from [2] using one of the input registers while the qubits from the other input 
    register act as control qubits for the adders.
    
    If given a float constant, for the :math:`k`-th adder the unused lower qubits of 
    the product register can be used to add :math:`\lfloor 2^k cnst \rfloor` instead 
    of :math:`2^k \lfloor cnst \rfloor`. There is no need for additional qubits for 
    the :math:`prd` register since :math:`2^n-1\to 2^n-\epsilon,\epsilon >0 \Rightarrow`
    :math:`cnst\cdot fct < 2^n\cdot (2^{n_{fct}}-1) < 2^{n+n_{fct}}-1 = 2^{n_{prd}}-1`.
    
    The :math:`k`-th qubit in the factor register can be uncomputed after the
    corresponding controlled adder by comparing the product register: 
    :math:`prd \geq \lfloor 2^k cnst \rfloor`. This is only possible for 
    :math:`cnst \geq 1`, otherwise :math:`prd \geq \lfloor 2^0 cnst \rfloor = 0`
    which is always true, regardless of the :math:`k`-th qubit.

    Example circuit for constant=6:
    .. parsed-literal::

               ┌──────────────────┐┌──────────────────┐┌──────────────────┐
         cnst: ┤0                 ├┤0                 ├┤0                 ├
               │                  ││                  ││                  │
        fct_0: ┤4                 ├┤                  ├┤                  ├
               │                  ││                  ││                  │
        fct_1: ┤                  ├┤4                 ├┤                  ├
               │                  ││                  ││                  │
        fct_2: ┤                  ├┤                  ├┤4                 ├
               │  CtrlCnstHalfAdd ││                  ││                  │
        prd_0: ┤                  ├┤  CtrlCnstHalfAdd ├┤                  ├
               │                  ││                  ││  CtrlCnstHalfAdd │
        prd_1: ┤1                 ├┤                  ├┤                  ├
               │                  ││                  ││                  │
        prd_2: ┤2                 ├┤1                 ├┤                  ├
               │                  ││                  ││                  │
        prd_3: ┤3                 ├┤2                 ├┤1                 ├
               └──────────────────┘│                  ││                  │
        prd_4: ────────────────────┤3                 ├┤2                 ├
                                   └──────────────────┘│                  │
        prd_5: ────────────────────────────────────────┤3                 ├
                                                       └──────────────────┘

    **References:**

    [1] Häner et al., Optimizing Quantum Circuits for Arithmetic, 2018.
    `arXiv:1805.12445 <https://arxiv.org/pdf/1805.12445.pdf>`_
    
    [2] Thapliyal et al., Quantum Circuit Designs of Integer Division Optimizing T-count and T-depth, 2018.
    `arXiv:1809.09732 <https://arxiv.org/pdf/arXiv:1809.09732.pdf>`_

    """

    def __init__(
        self, constant: Union[int, float], num_factor_qubits: int, num_additive_qubits: Optional[int] = None, 
        uncompute: Optional[bool] = False, omit_highest_qubit: Optional[bool] = False,
        name: Optional[str] = "CnstMult"
    ) -> None:
        r"""
        Args:
            constant: Number to multiply with.
            num_factor_qubits: Size of the register :math:`fct` which is treated as the factor.
            num_additive_qubits: Size of an already initialized summand in the product register.
            uncompute: If the register :math:`fct` should be uncomputed.
            omit_highest_qubit: If the multiplicator should restrict the product 
                register size to the sum of ``num_factor_qubits`` and  number of bits 
                needed to represent the constant, minus 1.
            name: The name of the circuit object.
        Raises:
            ValueError: If ``constant`` is smaller or equal to 0.
            ValueError: If ``num_factor_qubits`` is smaller than 1.
            ValueError: If ``num_additive_qubits`` is smaller than 0.
            ValueError: If ``constant`` is too close to 0.
            ValueError: If ``constant`` is smaller than 1 and uncompute is True.
            NotImplementedError: If ``constant`` is not an integer and ``num_additive_qubits`` 
                is set to a nonzero value.
            NotImplementedError: If ``uncompute`` is True and ``num_additive_qubits`` 
                is set to a nonzero value.
        """
        if constant <= 0:
            raise ValueError(
                "The constant number to multiply must be greater than 0.")
            
        if num_factor_qubits < 1:
            raise ValueError("Size of the factor register must be at least 1.")
        elif num_factor_qubits == 1:
            omit_highest_qubit = True
        
        if num_additive_qubits is not None and num_additive_qubits < 0:
            raise ValueError("The number of additive state qubits must be at least 0.")
        
        if int(2**(num_factor_qubits - 1) * constant) == 0:
            raise ValueError("Constant is to close to 0, product will be 0.")

        if constant < 1 and uncompute:
            raise ValueError("Constant must be at least 1 for uncomputation.")
        
        has_no_additive = num_additive_qubits in [0, None]
        if constant != int(constant) and not has_no_additive:
            raise NotImplementedError("Additives not implemented for non-integer constants.")
        
        if uncompute and not has_no_additive:
            raise NotImplementedError("Additives not implemented for uncomputation.")

        min_num_cq = ceil(log2(int(constant) + 1))
        if num_additive_qubits is None or num_additive_qubits < min_num_cq:
            num_additive_qubits = min_num_cq

        # define adders
        ctrl_adders = []
        comperators = []
        for step in range(num_factor_qubits):
            num_float_qubits = step
            cnst = int(2**step * constant)
            
            if cnst == 0:
                ctrl_adders.append(None)
                continue
            
            while cnst % 2 == 0:
                cnst //= 2
                num_float_qubits -= 1
            
            if has_no_additive:
                has_no_additive = False
                ctrl_adders.append(CtrlPseudoAdd(
                    cnst, num_state_qubits=min_num_cq + num_float_qubits))
            else:
                has_cout = not (omit_highest_qubit and (step == num_factor_qubits - 1))
                ctrl_adders.append(CtrlCnstHalfAdd(
                    cnst, num_state_qubits=min_num_cq + num_float_qubits,
                    num_propagation_qubits=num_additive_qubits - min_num_cq, has_cout=has_cout))
                
            if uncompute:
                num_qubits = ctrl_adders[-1].num_state_qubits
                if ctrl_adders[-1].has_cout:
                    num_qubits += 1
                comperators.append(IntegerComparator(
                    num_qubits, cnst, name=f"IntCmp(>={cnst})"))
            

        self._constant = constant
        self._num_constant_qubits = ctrl_adders[-1].num_constant_qubits
        if uncompute:
            for comparator in comperators:
                self._num_constant_qubits = max(self._num_constant_qubits, comparator.num_ancillas)
        
        self._num_factor_qubits = num_factor_qubits
        self._num_product_qubits = num_additive_qubits + num_factor_qubits
        if omit_highest_qubit:
            self._num_product_qubits -= 1
        super().__init__(name=name)

        # build circuit
        qr_cnst = AncillaRegister(self._num_constant_qubits, name="cnst")
        qr_fct = QuantumRegister(num_factor_qubits, name="fct")
        qr_prd = QuantumRegister(self._num_product_qubits, name="prd")
        circuit = QuantumCircuit(qr_cnst, qr_fct, qr_prd, name=name)
        self.add_register(qr_cnst, qr_fct, qr_prd)

        # cumulative adders
        adder_range_stop = self._num_product_qubits - num_factor_qubits
        if omit_highest_qubit:
            adder_range_stop += 1
        for step in range(num_factor_qubits):
            adder_range_stop += 1
            ctrl_adder = ctrl_adders[step]
            
            if ctrl_adder is not None:
                prd_start = adder_range_stop - ctrl_adder._num_state_qubits - 1
                prd_stop = adder_range_stop
                if not ctrl_adder.has_cout:
                    prd_stop -= 1
                cnst_qubits = []
                if ctrl_adder.num_constant_qubits > 0:
                    cnst_qubits = qr_cnst[-ctrl_adder.num_constant_qubits:]

                circuit.append(
                    ctrl_adder.to_gate(),
                    cnst_qubits + qr_prd[prd_start : prd_stop] + [qr_fct[step]])
                
                if uncompute:
                    comperator = comperators[step]
                    circuit.append(
                        comperator.to_gate(), 
                        qr_prd[prd_start : prd_stop] + [qr_fct[step]] + qr_cnst[:comperator.num_ancillas])

        self.append(circuit.to_gate(), self.qubits)

    @property
    def constant(self) -> int:
        """The constant number to multiply.
        Returns:
            The constant.
        """
        return self._constant
    
    @property
    def num_constant_qubits(self) -> int:
        """The constant register size.
        Returns:
            The number of qubits in the constant register.
        """
        return self._num_constant_qubits
    
    @property
    def num_factor_qubits(self) -> int:
        """The factor register size.
        Returns:
            The number of qubits in the factor register.
        """
        return self._num_factor_qubits
    
    @property
    def num_product_qubits(self) -> int:
        """The product register size.
        Returns:
            The number of qubits in the product register.
        """
        return self._num_product_qubits
