"""Compute the product of two qubit registers using classical multiplication approach."""

from typing import Optional
from math import log2, ceil
from qiskit.circuit import QuantumRegister, AncillaRegister, QuantumCircuit
from ..adders import CtrlPseudoAdd, CtrlCnstHalfAdd


class InPlaceCnstMult(QuantumCircuit):
    r"""A multiplication circuit to store product of two input registers in-place.

    Implementation uses an original approach, based again on shifted additons [1].
    With a constant represented in binary as
    :math:`cnst = (cnst_{n-1} \dots cnst_{k+1}cnst_{k} 0 \dots 0)_2`, where
    :math:`cnst_k` is the lowest nonzero bit, define the reduced constant as
    :math:`cnst' = \lfloor cnst/2^{k+1} \rfloor = (cnst_{n-1} \dots cnst_{k+1})_2`.
    This constant can now be added in the product register :math:`prd`, controlled
    by the qubits in the same register, going from the highest to the lowest qubit.
    For each control :math:`prd_i = 1`, :math:`cnst'` is added on the qubits directly
    higher than :math:`prd_i`, resulting in :math:`(cnst_{n-1} \dots cnst_{k+1}cnst_{k})_2`
    being written to :math:`prd_{n-1+i-k} \dots prd_i` if :math:`prd` is empty above
    qubit :math:`i`, since :math:`cnst_{k} = prd_i = 1`. For :math:`prd_i = 0` no
    addition is performed.  The first controlled addition acts only on zero qubits
    and can therfore be performed by CNOTs (pseudo adder). The lowest :math:`k` bits 
    cutoff by the integer shift are included by (optionally) padding the lower end of
    the :math:`prd` register with unused qubits. The input can (optionally) be shifted,
    if output qubit reordering is not possible or useful.
    
    If the product is assured to use one qubit less then standard, one can omit the
    most significant qubit and simplify the adders.

    Example circuit for constant=6:
    .. parsed-literal::
    
                      ┌──────────────────┐┌──────────────────┐
         cnst_0: ─────┤0                 ├┤0                 ├
                      │                  ││                  │
         cnst_1: ─────┤1                 ├┤1                 ├
                      │                  ││                  │
         cnst_2: ─────┤                  ├┤2                 ├
                      │                  ││                  │
        prd_pad: ─────┤                  ├┤                  ├
                      │                  ││                  │
          prd_0: ─────┤  CtrlCnstHalfAdd ├┤7 CtrlCnstHalfAdd ├
                      │                  ││                  │
          prd_1: ─────┤5                 ├┤3                 ├
                      │                  ││                  │
          prd_2: ──■──┤2                 ├┤4                 ├
                 ┌─┴─┐│                  ││                  │
          prd_3: ┤ X ├┤3                 ├┤5                 ├
                 └───┘│                  ││                  │
          prd_4: ─────┤4                 ├┤6                 ├
                      └──────────────────┘└──────────────────┘
                                
    **References:**

    [1] Häner et al., Optimizing Quantum Circuits for Arithmetic, 2018.
    `arXiv:1805.12445 <https://arxiv.org/pdf/1805.12445.pdf>`_

    """

    def __init__(
        self, constant: int, num_factor_qubits: int, has_padding_qubits: Optional[bool] = True,
        do_swaps: Optional[bool] = True, omit_highest_qubit: Optional[bool] = False,
        name: Optional[str] = "InPlaceCnstMult"
    ) -> None:
        r"""
        Args:
            constant: Number to multiply with.
            num_factor_qubits: Size of the register in which the factor is stored.
            has_padding_qubits: If the least significant zero qubits should be padded
                at the start of the product register.
            do_swaps: If the input should be shifted up by the number of padding qubits.
            omit_highest_qubit: If the multiplicator should restrict the unpadded product 
                register size to the sum of ``num_factor_qubits`` and  number of bits 
                needed to represent the constant, minus 1.
            name: The name of the circuit object.
        Raises:
            ValueError: If ``constant`` is smaller than 2.
            ValueError: If ``num_factor_qubits`` is smaller than 1.
        """
        if constant < 2:
            raise ValueError(
                "The constant number to multiply must be at least 2.")
        if num_factor_qubits < 1:
            raise ValueError(
                "The number factor qubits must be greater than 1.")
        elif num_factor_qubits == 1:
            omit_highest_qubit = True
            
        # reduced constant
        reduced_constant = constant
        num_padding_qubits = 0
        while reduced_constant % 2 == 0:
            reduced_constant /= 2
            num_padding_qubits += 1
        reduced_constant  = int(reduced_constant / 2)
        min_num_cq = ceil(log2(reduced_constant + 1))
        
        # maximum needed constant qubits
        if reduced_constant != 0 and num_factor_qubits > 1:
            if omit_highest_qubit:
                ctrl_adder = CtrlCnstHalfAdd(
                    reduced_constant, num_propagation_qubits=num_factor_qubits - 2, has_cout=True)
            else:
                ctrl_adder = CtrlCnstHalfAdd(
                    reduced_constant, num_propagation_qubits=num_factor_qubits - 1, has_cout=True)
            self._num_constant_qubits = ctrl_adder.num_constant_qubits
        else:
            self._num_constant_qubits = 0
        
        self._constant = constant
        self._num_product_qubits = num_factor_qubits + min_num_cq
        if not omit_highest_qubit:
            self._num_product_qubits += 1
        if has_padding_qubits:
            self._num_padding_qubits = num_padding_qubits
        else:
            self._num_padding_qubits = 0
        super().__init__(name=name)

        # build circuit
        circuit = QuantumCircuit(name=name)
        qr_cnst = AncillaRegister(self._num_constant_qubits, name="cnst")
        circuit.add_register(qr_cnst)
        self.add_register(qr_cnst)
        if self._num_padding_qubits > 0:
            qr_prd_pad = QuantumRegister(self._num_padding_qubits, name="prd_pad")
            circuit.add_register(qr_prd_pad)
            self.add_register(qr_prd_pad)
        qr_prd = QuantumRegister(self._num_product_qubits, name="prd")
        circuit.add_register(qr_prd)
        self.add_register(qr_prd)
        
        if do_swaps and self.num_padding_qubits > 0:
            # half swaps / shift
            combined_prd_qubits = qr_prd_pad[:] + qr_prd[:]
            for i in reversed(range(num_factor_qubits)):
                circuit.cx(combined_prd_qubits[i], combined_prd_qubits[i + self.num_padding_qubits])
                circuit.cx(combined_prd_qubits[i + self.num_padding_qubits], combined_prd_qubits[i])
        
        if reduced_constant != 0:
            # first pseudo adder on 0 intialized qubits
            circuit.append(
                CtrlPseudoAdd(reduced_constant, min_num_cq).to_gate(), 
                qr_prd[num_factor_qubits: num_factor_qubits + min_num_cq] + [qr_prd[num_factor_qubits - 1]])

            # cumulative adders
            for i in range(num_factor_qubits - 1):
                if omit_highest_qubit:
                    ctrl_adder = CtrlCnstHalfAdd(
                        reduced_constant, num_propagation_qubits=i, has_cout=True)
                else:
                    ctrl_adder = CtrlCnstHalfAdd(
                        reduced_constant, num_propagation_qubits=i + 1, has_cout=True)
                circuit.append(
                    ctrl_adder.to_gate(),
                    qr_cnst[:ctrl_adder.num_constant_qubits] + qr_prd[num_factor_qubits - i - 1:] \
                        + [qr_prd[num_factor_qubits - i - 2]])

        self.append(circuit.to_gate(), self.qubits)

    @property
    def constant(self) -> int:
        """The constant number to multiply.
        Returns:
            The constant.
        """
        return self._constant
    
    @property
    def num_constant_qubits(self) -> int:
        """The constant register size.
        Returns:
            The number of qubits in the constant register.
        """
        return self._num_constant_qubits
    
    @property
    def num_product_qubits(self) -> int:
        """The product register size.
        Returns:
            The number of qubits in the product register.
        """
        return self._num_product_qubits + self._num_padding_qubits
    
    @property
    def num_padding_qubits(self) -> int:
        """The number of initail least significant zero bits of the constant.
        Returns:
            Number of padding qubits at the start of the product register.
        """
        return self._num_padding_qubits
