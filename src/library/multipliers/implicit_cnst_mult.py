"""Compute the product of a qubit register with a contstant using classical multiplication approach."""

from typing import Optional, Tuple, Union
from math import log2, ceil
from qiskit.circuit import QuantumRegister, AncillaRegister, QuantumCircuit
from ..adders.full_add import FullAdd


class ImplicitCnstMult(QuantumCircuit):
    r"""A multiplication circuit to store product of one input register and a constant
    out-of-place.
 
    Multiplication in this circuit is implemented in a classical approach by performing
    a series of shifted additions [1] with the input register using the full adder [2],
    implicitely controlled according to the binary representation of a given constant.
    
    If given a float constant, new implicit controlled additions are used. They act on 
    the high part of the :math:`fct` register, controlled by the binary representation 
    of decimal places.

    Example circuit for constant=6:
    .. parsed-literal::
            
               ┌──────────┐┌──────────┐
          aux: ┤0         ├┤0         ├
               │          ││          │
        fct_0: ┤1         ├┤1         ├
               │          ││          │
        fct_1: ┤2         ├┤2         ├
               │          ││          │
        fct_2: ┤3         ├┤3         ├
               │          ││          │
        out_0: ┤  FullAdd ├┤          ├
               │          ││  FullAdd │
        out_1: ┤4         ├┤          ├
               │          ││          │
        out_2: ┤5         ├┤4         ├
               │          ││          │
        out_3: ┤6         ├┤5         ├
               │          ││          │
        out_4: ┤7         ├┤6         ├
               └──────────┘│          │
        out_5: ────────────┤7         ├
                           └──────────┘

    **References:**

    [1] Häner et al., Optimizing Quantum Circuits for Arithmetic, 2018.
    `arXiv:1805.12445 <https://arxiv.org/pdf/1805.12445.pdf>`_
    
    [2] Thapliyal et al., Design of Efficient Reversible Logic-Based Binary and BCD Adder Circuits, 2017.
    `arXiv:1712.02630 <https://arxiv.org/pdf/arXiv:1712.02630.pdf>`_

    """

    def __init__(
        self, constant: Union[int, float], num_factor_qubits: int, name: Optional[str] = "ImplCnstMult"
    ) -> None:
        r"""
        Args:
            constant: Number to multiply with.
            num_factor_qubits: Size of the register :math:`fct` which is treated as the factor.
            name: The name of the circuit object.
        Raises:
            ValueError: If ``constant`` is smaller or equal to 0.
            ValueError: If ``num_factor_qubits`` is smaller than 1.
            ValueError: If ``constant`` is too close to 0.
        """
        if constant <= 0:
            raise ValueError(
                "The constant number to multiply must be greater than 0.")

        if num_factor_qubits < 1:
            raise ValueError("Size of the factor register must be at least 1.")
    
        
        if int(2**(num_factor_qubits - 1) * constant) == 0:
            raise ValueError("Constant is to close to 0, product will be 0.")
        
        self._constant = constant
        self._num_factor_qubits = num_factor_qubits
        self._num_product_qubits = ceil(log2(int(constant) + 1)) + num_factor_qubits
        int_bin_repr, float_bin_repr = self._bin_reprs()
        
        super().__init__(name=name)

        # build circuit
        qr_aux = AncillaRegister(1, name="aux")
        qr_fct = QuantumRegister(num_factor_qubits, name="fct")
        qr_prd = QuantumRegister(self._num_product_qubits, name="prd")
        circuit = QuantumCircuit(qr_aux, qr_fct, qr_prd, name=name)
        self.add_register(qr_aux, qr_fct, qr_prd)
        

        # cumulative adders, implicitly controlled by binary representation of constant        
        for idx, bit in enumerate(reversed(float_bin_repr)):
            if bit == "1":
                circuit.append(
                    FullAdd(idx + 1).to_gate(),
                    [qr_aux[0]] + qr_fct[-idx - 1:] + qr_prd[: idx + 2])

        for idx, bit in enumerate(reversed(int_bin_repr)):
            if bit == "1":
                circuit.append(
                    FullAdd(num_factor_qubits).to_gate(),
                    [qr_aux[0]] + qr_fct[:] + qr_prd[idx: idx + num_factor_qubits + 1])

        self.append(circuit.to_gate(), self.qubits)

    @property
    def constant(self) -> int:
        """The constant number to multiply.
        Returns:
            The constant.
        """
        return self._constant
    
    @property
    def num_factor_qubits(self) -> int:
        """The factor register size.
        Returns:
            The number of qubits in the factor register.
        """
        return self._num_factor_qubits
    
    @property
    def num_product_qubits(self) -> int:
        """The product register size.
        Returns:
            The number of qubits in the product register.
        """
        return self._num_product_qubits
    
    def _bin_reprs(self) -> Tuple[str, str]:
        """Create approximate binary representation.
        Returns:
            Tuple[str, str]: Pre and post floating point representations.
        """
        int_bin_repr = bin(int(self._constant))[2:]
        float_bin_repr = ""
        cnst = self._constant
        cnst %= 1
        for _ in range(self._num_factor_qubits - 1):
            cnst *= 2
            float_bin_repr += str(int(cnst))
            cnst -= int(cnst)
        
        return int_bin_repr, float_bin_repr
