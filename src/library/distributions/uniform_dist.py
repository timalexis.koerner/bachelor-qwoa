"""Create an uniform distribution using a single tuned grover step."""

from math import acos, log2, ceil
from typing import Optional
from qiskit import QuantumCircuit, QuantumRegister, AncillaRegister
from qiskit.circuit.library.arithmetic import IntegerComparator


class UniformDistribution(QuantumCircuit):
    """Uniform Distribution over an arbitrary number of states.

    Uses a single Grover step with a tuned selection and diffusion angle.
    Uses the qiskit IntegerComparator.

    The implementation is presented by Ryan Babbush in [1].

    Example circuit for 12 states:
    .. parsed-literal::

                  ┌───┐                                                                          
        simple_0: ┤ H ├──────────────────────────────────────────────────────────────────────────
                  ├───┤                                                                          
        simple_1: ┤ H ├──────────────────────────────────────────────────────────────────────────
                  ├───┤┌───────┐             ┌───────┐┌───┐┌───┐                       ┌───┐┌───┐
        grover_0: ┤ H ├┤0      ├─────────────┤0      ├┤ H ├┤ X ├──■─────────────────■──┤ X ├┤ H ├
                  ├───┤│       │             │       │├───┤├───┤  │                 │  ├───┤├───┤
        grover_1: ┤ H ├┤1      ├─────────────┤1      ├┤ H ├┤ X ├──■─────────────────■──┤ X ├┤ H ├
                  └───┘│  qg<3 │┌───────────┐│  qg<3 │└───┘└───┘┌─┴─┐┌───────────┐┌─┴─┐└───┘└───┘
           phase: ─────┤2      ├┤ Rz(1.231) ├┤2      ├──────────┤ X ├┤ Rz(1.231) ├┤ X ├──────────
                       │       │└───────────┘│       │          └───┘└───────────┘└───┘          
             aux: ─────┤3      ├─────────────┤3      ├───────────────────────────────────────────
                       └───────┘             └───────┘                                           

    **References:**

    [1] Ryan Babbush et al., Encoding Electronic Spectra in Quantum Circuits with Linear T Complexity, 2018.
    `arXiv:1805.03662 <https://arxiv.org/pdf/arXiv:1805.03662.pdf>`_

    """

    def __init__(self, num_states: int, name: Optional[str] = "UniDist") -> None:
        """Creates an uniform distribution over an arbitrary number of states.       

        Args:
            num_states: Number of superposed states.
            name: Name of the circuit object.

        Raises:
            ValueError: If the ``num_states`` is smaller than 1.
        """
        if num_states < 1:
            raise ValueError(
                "The number of superposed states must be at least 1.")

        self._num_states = num_states
        self._set_register_sizes()

        qregs = []
        if num_states == 1:
            qr = QuantumRegister(1, name="single")
            qregs.append(qr)
        if self._simple_reg_size > 0:
            qr_s = QuantumRegister(self._simple_reg_size, name="simple")
            qregs.append(qr_s)
        if self._grover_reg_size > 0:
            qr_g = QuantumRegister(self._grover_reg_size, name="grover")
            qr_phase = AncillaRegister(1, name="phase")
            qr_aux = AncillaRegister(self._grover_reg_size - 1, name="aux")
            qregs += [qr_g, qr_phase, qr_aux]

        super().__init__(*qregs, name=name)

        circuit = QuantumCircuit(*qregs, name=name)

        if num_states > 1 and self._simple_reg_size > 0:
            circuit.h(qr_s)

        if self._grover_reg_size > 0:
            remaining_states = int(num_states / 2**self._simple_reg_size)
            theta = acos(1 - 2**int(log2(remaining_states)) / remaining_states)
            qc_ic = IntegerComparator(
                self._grover_reg_size,
                remaining_states,
                geq=False,
                name="qg<" + str(remaining_states)
            )

            circuit.h(qr_g)
            circuit.append(qc_ic.to_gate(), circuit.qubits[self._simple_reg_size:])
            circuit.rz(theta, qr_phase)
            circuit.append(qc_ic.to_gate(), circuit.qubits[self._simple_reg_size:])

            circuit.h(qr_g)
            circuit.x(qr_g)
            circuit.mcx(qr_g, qr_phase, qr_aux, mode="v-chain")
            circuit.rz(theta, qr_phase)
            circuit.mcx(qr_g, qr_phase, qr_aux, mode="v-chain")
            circuit.x(qr_g)
            circuit.h(qr_g)

        self.append(circuit.to_gate(), self.qubits)

    @property
    def num_states(self) -> int:
        """The number of superposed states.

        Returns:
            int: Number of states.
        """
        return self._num_states

    @property
    def num_state_qubits(self) -> int:
        """The number of qubits used.

        Returns:
            int: 1 if only one state, 
            otherwise number of simple + grover qubits.
        """
        if self._num_states == 1:
            return 1
        return self._simple_reg_size + self._grover_reg_size

    def _set_register_sizes(self) -> None:
        """Sets the register sizes needed to support the states.
        """
        n = self._num_states
        k = 0
        while n > 0:
            if n % 2 == 0:
                k += 1
                n /= 2
            else:
                break
        self._simple_reg_size = k
        self._grover_reg_size = ceil(log2(self._num_states))-k
