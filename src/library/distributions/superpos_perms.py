"""Superposition over permutations."""

from typing import Optional, Any, List, Tuple
from math import log2, ceil
from qiskit.circuit import QuantumRegister, AncillaRegister, QuantumCircuit
from . import UniformDistribution
from ..permutations import PermToLehmerCode, LehmerCodeToPerm


class SuperPosPerms(QuantumCircuit):
    r"""Superposition of all permutations.
    
    Using the Lehmer-Code, the superposition of all possible permutations
    over :math:`n` states can be created by first building
    a superposition over all possible digits for the Lehmer-Code with
    :math:`0 \leq d_i < n-i`, i.e.
    :math:`|\Psi_i\rangle = \frac{1}{\sqrt{n-i}} \sum_{j<n-i} |j\rangle`.
    Then the Lehmer-Code is converted into Permutations with
    LCtoP and later uncomputed with the inverse of PtoLC.
    
    The permutations are grouped over the maximally necessary ancillas
    with a gready bin packing algortihm.

    Example circuit for num_states=3:
    .. parsed-literal::

                              ┌─────────┐┌────────────┐
        perm_0_0: ────────────┤0        ├┤0           ├
                              │         ││            │
        perm_0_1: ────────────┤1        ├┤1           ├
                              │         ││            │
        perm_1_0: ────────────┤2        ├┤2           ├
                              │         ││            │
        perm_1_1: ────────────┤3        ├┤3           ├
                  ┌──────────┐│         ││            │
        perm_2_0: ┤3         ├┤4        ├┤4           ├
                  │          ││         ││            │
        perm_2_1: ┤2         ├┤5  LCtoP ├┤5  PtoLC_dg ├
                  │  UniDist ││         ││            │
           aux_0: ┤0         ├┤6        ├┤6           ├
                  │          ││         ││            │
           aux_1: ┤1         ├┤7        ├┤7           ├
                  ├─────────┬┘│         ││            │
           aux_2: ┤ UniDist ├─┤8        ├┤8           ├
                  └─────────┘ │         ││            │
           aux_3: ────────────┤9        ├┤9           ├
                              │         ││            │
           aux_4: ────────────┤10       ├┤10          ├
                              └─────────┘└────────────┘

    """

    def __init__(
        self, num_states: int, name: Optional[str] = "SPPerm") -> None:
        r"""Superposition over permutations.
        
        Args:
            num_states: Number of permuted states.
            name: The name of the circuit object.
        Raises:
            ValueError: If ``num_states`` is smaller or equal to 1.
        """
        if num_states <= 1:
            raise ValueError(
                "The number of permuted states must be greater than 1.")
        
        self._num_states = num_states
        self._num_state_qubits = ceil(log2(num_states))
        self._num_lehmer_digit_qubit_list = []
        for i in range(num_states - 1):
            self._num_lehmer_digit_qubit_list.append(ceil(log2(num_states - i)))
        self._num_permutation_qubits = num_states * self._num_state_qubits
        
        super().__init__(name=name)

        # circuit blocks
        perm_to_lehmer_code = PermToLehmerCode(num_states)
        lehmer_code_to_perm = LehmerCodeToPerm(num_states)
        num_ancillas = max(perm_to_lehmer_code.num_ancillas, lehmer_code_to_perm.num_ancillas)
        
        uni_dist_infos = []
        num_uni_dist_ancillas = 0
        for digit_index in range(num_states - 1):
            uni_dist = UniformDistribution(num_states - digit_index)
            uni_dist_infos.append(((uni_dist, digit_index), uni_dist.num_ancillas))
            num_uni_dist_ancillas = max(num_uni_dist_ancillas, uni_dist.num_ancillas)
        
        num_ancillas = max(num_ancillas, 
                           num_uni_dist_ancillas - self.num_permutation_qubits)
        
        grouped_uni_dist_infos = self._first_fit_decreasing(
            uni_dist_infos, num_ancillas + self.num_permutation_qubits)
        
        # registers
        qr_perm_states = []
        for i in range(self.num_states):
            qr_perm_states.append(QuantumRegister(self.num_state_qubits, f"perm_{i}"))
        qr_digits = []
        for i, num_qubits in enumerate(self.num_lehmer_digit_qubit_list):
            qr_digits.append(QuantumRegister(num_qubits, f"digit_{i}"))
        qr_aux = AncillaRegister(num_ancillas, "aux")
                
        # circuit
        circuit = QuantumCircuit(*qr_perm_states, *qr_digits, qr_aux, name=name)
        
        # own registers
        self_qr_aux = AncillaRegister(sum(self.num_lehmer_digit_qubit_list) + num_ancillas, "aux")
        self.add_register(*qr_perm_states, self_qr_aux)
        
        all_perm_state_qubits = [q for qr_perm_state in qr_perm_states for q in qr_perm_state]
        all_digit_qubits = [q for qr_digit in qr_digits for q in qr_digit]
        
        # create superpositions
        uni_dist_ancillas = list(reversed(qr_aux[:] + all_perm_state_qubits))
        for group in grouped_uni_dist_infos:
            sum_ancillas = 0
            for uni_dist_infos in group:
                uni_dist, digit_index = uni_dist_infos[0]
                circuit.append(uni_dist.to_gate(),
                               qr_digits[digit_index][:] +\
                               uni_dist_ancillas[sum_ancillas: sum_ancillas + uni_dist.num_ancillas])
                sum_ancillas += uni_dist.num_ancillas
        
        # compute permutations and uncompute lehmer code
        circuit.append(lehmer_code_to_perm.to_gate(),
                       all_perm_state_qubits[:] + all_digit_qubits[:] + qr_aux[:lehmer_code_to_perm.num_ancillas])
        circuit.append(perm_to_lehmer_code.inverse().to_gate(),
                       all_perm_state_qubits[:] + all_digit_qubits[:] + qr_aux[:perm_to_lehmer_code.num_ancillas])
            
        self.append(circuit.to_gate(), self.qubits)


    @property
    def num_states(self) -> int:
        """The number of permuted states.
        Returns:
            The number of states.
        """
        return self._num_states
    
    @property
    def num_state_qubits(self) -> int:
        """Number of qubits for each permutation register.
        Returns:
            The number of state qubits.
        """
        return self._num_state_qubits
    
    @property
    def num_permutation_qubits(self) -> int:
        """Overall number of qubits for the permutation.
        Returns:
            The number of permutation qubits.
        """
        return self._num_permutation_qubits
    
    @property
    def num_lehmer_digit_qubit_list(self) -> list:
        """Number of qubits to store each Lehmer digit.
        Returns:
            The number of Lehmer digit qubits in a list.
        """
        return self._num_lehmer_digit_qubit_list

    def _first_fit_decreasing(self, items: List[Tuple[Any, int]], capacity: int) -> List[List[Any]]:
        """Approximation algorithm for the bin packing problem.
        First sorts the items by size in decreasing order, then uses
        the first possible bin.

        Args:
            items: Tuples of (id, weight) representing each item.
            capacity: Maximum (inlcusive) size of each bin.
        Returns:
            The bins as grouped lists of items.
        """
        items = sorted(items, key=lambda x:x[1], reverse=True)
        bins = []
        for item in items:
            weight = item[1]
            inserted = False
            for bin in bins:
                if bin[1] + weight <= capacity:
                    bin[0].append(item)
                    bin[1] += weight
                    inserted = True
                    break
            
            if not inserted:
                bins.append([[item], weight])
        return [bin[0] for bin in bins]