"""Checks if a pair of adjacend values is in a Permutation."""

from typing import Optional, Tuple, List
from math import log2, ceil
from qiskit.circuit import QuantumRegister, AncillaRegister, QuantumCircuit
from qiskit.circuit.library import MCXVChain
from .. gates import MCXBorrowedVChain


class PermPairComparator(QuantumCircuit):
    r"""Checks if a pair of adjacend values is in a permutation.
    
    The comparison of a value pair with all adjacend permutation states
    :math:`\forall k <n: (i,j) = (\pi_k, \pi_{k+1 \text{ mod } n})`
    is done via multicontrolled NOT gates, Before each MCNOT gate,
    NOT gates are added according to the inverted bit pattern of
    :math:`(i,j)`. Since each value pair can only exist once in a
    permutation, the controlled NOT gates can share a target qubit.
    The gates can be somewhat parallelized by checking sets of
    adjacend permutation states without overlap.
    The index sets are :math:`\{(2k, 2k+1) | 0 \leq k < \lfloor n/2\rfloor \}`,
    :math:`\{(2k + 1, 2k+2 \text{ mod } n) | 0 \leq k < \lfloor n/2 \rfloor\}`,
    :math:`\{(n-1, 0)\}` if :math:`n>1 \land n \text{ odd}`.

    If the adjusted cost matrix :math:`\bar Q` is symmetric each permutation
    state can be compared seperatly :math:`\forall k<n: (i = \pi_k) \lor (j =  \pi_k)`
    with one target qubit each. The :math:`n` target qubits are not uncomputed
    and later grouped as CCNOT gate controls with
    :math:`\{(k, k+1 \text{ mod } n) | 0 \leq k < n\}` or
    :math:`\{(0, 1)\}` if :math:`n=2`, otherwise  acting on a final single target.
    The comparison is done in two steps: 
    1. Calculate the bitwise XOR :math:`(l)_2=(i)_2 \veebar (j)_2` and find an
    index :math:`r` with :math:`(l)_{2_r}=1`. Using this index as a control,
    perform CNOTs on all other set bit indices :math:`s\neq r: (l)_{2_s}=1`.
    2. Secondly add NOT gates according to the inverted bit pattern of
    :math:`i` if :math:`i_r=0` or :math:`j` if :math:`j_r=0` and use a
    MCNOT gate on all indices but :math:`r`.

    Because of the high ancilla demand for multicontrolled gates with low depth,
    accumulation qubits of the later computed sum :math:`Val_{\bar Q}(\pi)`
    may be borrowed as ancilla qubits, see MCXBorrowedVChain. 

    Example circuit for num_states=3, value_pair=(2,1), is_symmetric=False:
    .. parsed-literal::

                  ┌───────────────┐                 ┌───────────────┐
        perm_0_0: ┤0              ├─────────────────┤2              ├
                  │               │                 │               │
        perm_0_1: ┤1              ├─────────────────┤3              ├
                  │               │┌───────────────┐│               │
        perm_1_0: ┤2              ├┤0              ├┤               ├
                  │               ││               ││               │
        perm_1_1: ┤3              ├┤1              ├┤               ├
                  │               ││               ││               │
        perm_2_0: ┤  Cmp(=(2, 1)) ├┤2              ├┤0 Cmp(=(2, 1)) ├
                  │               ││               ││               │
        perm_2_1: ┤               ├┤3 Cmp(=(2, 1)) ├┤1              ├
                  │               ││               ││               │
             cmp: ┤4              ├┤4              ├┤4              ├
                  │               ││               ││               │
           aux_0: ┤5              ├┤5              ├┤5              ├
                  │               ││               ││               │
           aux_1: ┤6              ├┤6              ├┤6              ├
                  └───────────────┘└───────────────┘└───────────────┘
    
    Example circuit for num_states=3, value_pair=(2,1), is_symmetric=True:
    .. parsed-literal::
    
                   ┌───────────────┐                                                 
         perm_0_0: ┤0              ├─────────────────────────────────────────────────
                   │               │                                                 
         perm_0_1: ┤1              ├─────────────────────────────────────────────────
                   │               │┌───────────────┐                                
         perm_1_0: ┤               ├┤0              ├────────────────────────────────
                   │               ││               │                                
         perm_1_1: ┤  Cmp(={1, 2}) ├┤1              ├────────────────────────────────
                   │               ││               │┌───────────────┐               
         perm_2_0: ┤               ├┤               ├┤0              ├───────────────
                   │               ││  Cmp(={1, 2}) ││               │               
         perm_2_1: ┤               ├┤               ├┤1              ├───────────────
                   │               ││               ││               │               
        pre_cmp_0: ┤2              ├┤               ├┤  Cmp(={1, 2}) ├──■─────────■──
                   └───────────────┘│               ││               │  │         │  
        pre_cmp_1: ─────────────────┤2              ├┤               ├──■────■────┼──
                                    └───────────────┘│               │  │    │    │  
        pre_cmp_2: ──────────────────────────────────┤2              ├──┼────■────■──
                                                     └───────────────┘┌─┴─┐┌─┴─┐┌─┴─┐
              cmp: ───────────────────────────────────────────────────┤ X ├┤ X ├┤ X ├
                                                                      └───┘└───┘└───┘

    """

    def __init__(
        self, num_states: int, value_pair: Tuple[int, int], has_borrowed_ancillas: Optional[bool] = False,
        is_symmetric: Optional[bool] = False, name: Optional[str] = None) -> None:
        r"""State pair comparison with adjacend permutation states.
        
        Args:
            num_states: Number of permuted states.
            value_pair: Two different values to compare.
            has_borrowed_ancillas: If the circuit uses borrowed ancillas for the state comparison.
            is_symmetric: If the value pair order is irrelevant.
            name: The name of the circuit object.
        Raises:
            ValueError: If ``num_states`` is smaller or equal to 1.
            ValueError: If ``state_pair`` inlcudes a state smaller than 0 or greater than ``num_states``-1.
            ValueError: If ``state_pair`` consists of equal states.
        """
        if num_states <= 1:
            raise ValueError(
                "The number of permuted states must be greater than 1.")
        if min(value_pair) < 0 or max(value_pair) >= num_states:
            raise ValueError("The states to compare must be in [0, num_states[.")
        if value_pair[0] == value_pair[1]:
            raise ValueError("The states to compare must be different.")
        
        if name is None:
            if is_symmetric:
                name = f"PPCmp(={set(value_pair)})"
            else:
                name = f"PPCmp(={value_pair})"
        
        self._value_pair = value_pair
        self._has_borrowed_ancillas = has_borrowed_ancillas
        self._is_symmetric = is_symmetric
        self._num_states = num_states
        self._num_state_qubits = ceil(log2(num_states))
        self._num_permutation_qubits = num_states * self._num_state_qubits
        
        super().__init__(name=name)

        if is_symmetric:
            cmp = self._compare_single_state(value_pair, has_borrowed_ancillas)
            num_ancillas = cmp.num_ancillas * num_states
        else:
            cmp = self._compare_state_pair(value_pair, has_borrowed_ancillas)
            num_ancillas = cmp.num_ancillas * int(num_states / 2)
            
        # registers
        qr_perm_states = []
        for i in range(self.num_states):
            qr_perm_states.append(QuantumRegister(self.num_state_qubits, f"perm_{i}"))
        if is_symmetric:
            qr_pre_cmp = QuantumRegister(self.num_states, f"pre_cmp")
        qr_cmp = QuantumRegister(1, "cmp")
        qr_aux = AncillaRegister(num_ancillas, "aux")
        
        # circuit
        if is_symmetric:
            circuit = QuantumCircuit(*qr_perm_states, qr_pre_cmp, qr_cmp, qr_aux, name=name)
            self.add_register(*qr_perm_states, qr_pre_cmp, qr_cmp, qr_aux)
            
            # build circuit
            for i in range(num_states):
                circuit.append(cmp.to_gate(),
                               qr_perm_states[i][:] + [qr_pre_cmp[i]] +\
                               qr_aux[i * cmp.num_ancillas: (i + 1) * cmp.num_ancillas])
            
            if num_states == 2:
                circuit.ccx(qr_pre_cmp[0], qr_pre_cmp[1], qr_cmp[0])
            else:
                for i, j in [(k, (k + 1) % self.num_states) for k in range(self.num_states)]:
                   circuit.ccx(qr_pre_cmp[i], qr_pre_cmp[j], qr_cmp[0])
            
        else:
            circuit = QuantumCircuit(*qr_perm_states, qr_cmp, qr_aux, name=name)
            self.add_register(*qr_perm_states, qr_cmp, qr_aux)
            
            # build circuit
            for group in self._paralellize_indices():
                for i, indices in enumerate(group):
                    circuit.append(cmp.to_gate(),
                                   qr_perm_states[indices[0]][:] + qr_perm_states[indices[1]][:] +\
                                   [qr_cmp[0]] + qr_aux[i * cmp.num_ancillas: (i + 1) * cmp.num_ancillas])      
        
        self.append(circuit.to_gate(), self.qubits)

    @property
    def num_states(self) -> int:
        """The number of permuted states.
        Returns:
            The number of states.
        """
        return self._num_states
    
    @property
    def num_state_qubits(self) -> int:
        """Number of qubits for each permutation register.
        Returns:
            The number of state qubits.
        """
        return self._num_state_qubits
    
    @property
    def num_permutation_qubits(self) -> int:
        """Overall number of qubits for the permutation.
        Returns:
            The number of permutation qubits.
        """
        return self._num_permutation_qubits
    
    @property
    def num_pre_compare_qubits(self) -> int:
        """Additional number of qubits for comparing.
        Returns:
            The number of states if symmetric comparisions else 0.
        """
        if self._is_symmetric:
            return self.num_states
        return 0
    
    @property
    def value_pair(self) -> Tuple[int, int]:
        """The value pair to compare.
        Returns:
            The value pair to compare.
        """
        return self._value_pair
    
    @property
    def has_borrowed_ancillas(self) -> bool:
        """If the ancillas are borrowed.
        Returns:
            If the ancillas are borrowed.
        """
        return self._has_borrowed_ancillas

    def _paralellize_indices(self) -> List[List[Tuple[int, int]]]:
        """Orders indices into groups without overlap for parallelization.
        Returns:
            List[List[Tuple[int, int]]]: List of indices groups.
        """
        first_set = [(2*k, 2*k + 1) for k in range(int(self.num_states/2))]
        second_set = [(2*k + 1, (2*k + 2) % self.num_states) for k in range(int(self.num_states/2))]
        if self.num_states % 2 == 0:
            return [first_set, second_set]
        else:
            return [first_set, second_set, [(self.num_states-1, 0)]]

    def _compare_single_state(self, value_pair: Tuple[int, int], has_borrowed_ancillas: bool) -> QuantumCircuit:
        """Checks if either of the values is in a single state register.
        Args:
            value_pair: Two different values to compare.
            has_borrowed_ancillas: If the circuit uses borrowed ancillas for the state comparison.
        Returns:
            QuantumCircuit: The circuit.
        """
        # multicontrol not
        if self.num_state_qubits > 1:
            if has_borrowed_ancillas:
                mcx = MCXBorrowedVChain(self.num_state_qubits - 1)
            else:
                mcx = MCXVChain(self.num_state_qubits - 1)
        
        # registers and circuit
        qr_state = QuantumRegister(self.num_state_qubits, "state")
        qr_cmp = QuantumRegister(1, "cmp")
        qr_aux = AncillaRegister(max(0, self.num_state_qubits - 3), "aux")
        circuit = QuantumCircuit(qr_state, qr_cmp, qr_aux, name=f"Cmp(={set(value_pair)})")
        if self.num_state_qubits == 1:
            return circuit
        
        # control nots for value bit differences
        value_xor = value_pair[0] ^ value_pair[1]
        ctrl_index = None
        for idx, bit in enumerate(reversed(bin(value_xor)[2:])):
            if bit == "1":
                if ctrl_index is None:
                    ctrl_index = idx
                else:
                    circuit.cx(ctrl_index, idx)
        
        # not gates for inverted control
        value_0_bin_string = bin(value_pair[0])[2:]
        if len(value_0_bin_string) > ctrl_index and list(reversed(value_0_bin_string))[ctrl_index] == "0":
            ctrl_value = value_pair[0]
        else:
            ctrl_value = value_pair[1]

        for idx, bit in enumerate(reversed(bin(ctrl_value)[2:])):
            if bit == "0" and idx != ctrl_index:
                circuit.x(idx)
        for idx in range(len(bin(ctrl_value)[2:]), self.num_state_qubits):
            if idx != ctrl_index:
                circuit.x(idx)
        
        # multicontrol not
        circuit.append(mcx, qr_state[:ctrl_index] + qr_state[ctrl_index + 1:] + [qr_cmp[0]] + qr_aux[:])
        
        # uncompute
        for idx, bit in enumerate(reversed(bin(ctrl_value)[2:])):
            if bit == "0" and idx != ctrl_index:
                circuit.x(idx)
        for idx in range(len(bin(ctrl_value)[2:]), self.num_state_qubits):
            if idx != ctrl_index:
                circuit.x(idx)
        for idx, bit in enumerate(reversed(bin(value_xor)[2:])):
            if bit == "1" and idx != ctrl_index:
                circuit.cx(ctrl_index, idx)

        return circuit

    def _compare_state_pair(self, value_pair: Tuple[int, int], has_borrowed_ancillas: bool) -> QuantumCircuit:
        """Checks if the values are in two state registers, with the order lower register, higher register.
        Args:
            value_pair: Two different values to compare.
            has_borrowed_ancillas: If the circuit uses borrowed ancillas for the state comparison.
        Returns:
            QuantumCircuit: The circuit.
        """
        # multicontrol not
        if has_borrowed_ancillas:
            mcx = MCXBorrowedVChain(2 * self.num_state_qubits)
        else:
            mcx = MCXVChain(2 * self.num_state_qubits)
        
        # registers and circuit
        qr_state_0 = QuantumRegister(self.num_state_qubits, "state_0")
        qr_state_1 = QuantumRegister(self.num_state_qubits, "state_1")
        qr_states = [qr_state_0, qr_state_1]
        qr_cmp = QuantumRegister(1, "cmp")
        qr_aux = AncillaRegister(max(0, 2 * self.num_state_qubits - 2), "aux")
        circuit = QuantumCircuit(*qr_states, qr_cmp, qr_aux, name=f"Cmp(={value_pair})")

        # not gates for inverted control
        for i in range(2):
            for idx, bit in enumerate(reversed(bin(value_pair[i])[2:])):
                if bit == "0":
                    circuit.x(qr_states[i][idx])
            for idx in range(len(bin(value_pair[i])[2:]), self.num_state_qubits):
                    circuit.x(qr_states[i][idx])
        
        # multicontrol not
        circuit.append(mcx, 
                       qr_state_0[:] + qr_state_1[:] + [qr_cmp[0]] + qr_aux[:])
        
        # uncompute
        for i in range(2):
            for idx, bit in enumerate(reversed(bin(value_pair[i])[2:])):
                if bit == "0":
                    circuit.x(qr_states[i][idx])
            for idx in range(len(bin(value_pair[i])[2:]), self.num_state_qubits):
                    circuit.x(qr_states[i][idx])

        return circuit