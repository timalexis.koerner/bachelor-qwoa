"""Comparator based on a full ripple-carry adder."""

from typing import Optional
from qiskit.circuit import QuantumCircuit, QuantumRegister, AncillaRegister
from qiskit.circuit.library.arithmetic.adders.adder import Adder


class Comparator(Adder):
    r"""The comparator variant of the full ripple-carry adder proposed by Curccaro et al..
    
    This is implemented by negating a register and uncomputing all adder qubits, 
    except the highest one (carry-out).
    
    Example circuit for 3 qubits:
    .. parsed-literal::

                   ┌───┐┌───┐                         ┌───┐     ┌───┐          
    aux: ──────────┤ X ├┤ X ├───────■──────────────■──┤ X ├─────┤ X ├──────────
         ┌───┐     └─┬─┘└─┬─┘       │              │  └─┬─┘┌───┐└─┬─┘          
    a_0: ┤ X ├───────┼────■─────────┼──────────────┼────■──┤ X ├──┼────────────
         ├───┤       │    │  ┌───┐┌─┴─┐          ┌─┴─┐  │  ├───┤  │       ┌───┐
    a_1: ┤ X ├──■────■────┼──┤ X ├┤ X ├───────■──┤ X ├──┼──┤ X ├──■────■──┤ X ├
         ├───┤  │         │  └─┬─┘└─┬─┘       │  └─┬─┘  │  └─┬─┘       │  ├───┤
    a_2: ┤ X ├──┼────■────┼────■────┼────■────┼────┼────┼────■────■────┼──┤ X ├
         └───┘  │    │    │         │    │    │    │    │         │    │  └───┘
    b_0: ───────┼────┼────■─────────┼────┼────┼────┼────■─────────┼────┼───────
              ┌─┴─┐  │              │    │    │    │              │  ┌─┴─┐     
    b_1: ─────┤ X ├──┼──────────────■────┼────┼────■──────────────┼──┤ X ├─────
              └───┘┌─┴─┐                 │    │                 ┌─┴─┐└───┘     
    b_2: ──────────┤ X ├─────────────────┼────■─────────────────┤ X ├──────────
                   └───┘               ┌─┴─┐┌─┴─┐┌───┐          └───┘          
    cmp: ──────────────────────────────┤ X ├┤ X ├┤ X ├─────────────────────────
                                       └───┘└───┘└───┘      
                       
    **References:**

    [1] Cuccaro et al., A new quantum ripple-carry addition circuit, 2004.
    `arXiv:quant-ph/0410184 <https://arxiv.org/pdf/quant-ph/0410184.pdf>`_
    
    """
    
    def __init__(
        self, num_state_qubits: int, geq: Optional[bool] = True,
        name: Optional[str] = None
    ) -> None:
        r"""
        Args:
            num_state_qubits: The number of qubits in either input register for
                state :math:`|a\rangle` or :math:`|s\rangle`. The two input
                registers must have the same number of qubits.
            geq: If the first register should be greater or equal to the second
                :math:`a >= b`.
            name: The name of the circuit object.
        Raises:
            ValueError: If ``num_state_qubits`` is lower than 1.
        """
        if num_state_qubits < 1:
            raise ValueError("The number of qubits must be greater than zero.")
        
        if name is None:
            if geq:
                name = "Cmp(>=)"
            else:
                name = "Cmp(<)"
        
        super().__init__(num_state_qubits, name=name)
        
        if num_state_qubits > 1:
            qr_aux = AncillaRegister(1, "aux")
            self.add_register(qr_aux)
        qr_a = QuantumRegister(num_state_qubits, "a")
        qr_b = QuantumRegister(num_state_qubits, "b")
        qr_cmp = QuantumRegister(1, "cmp")
        self.add_register(qr_a, qr_b, qr_cmp)
            
        # build adder circuit
        circuit = QuantumCircuit(*self.qregs, name=name)
        
        # inverse first register
        circuit.x(qr_a)

        if num_state_qubits == 1:
            #circuit.cx(qr_a, qr_cmp)
            circuit.ccx(qr_a, qr_b, qr_cmp)
        else:       
            # pre CNOT gates
            for i in range(1, num_state_qubits):
                circuit.cx(qr_a[i], qr_b[i])
            if num_state_qubits > 1:
                circuit.cx(qr_a[1], qr_aux)
            for i in range(1, num_state_qubits - 1):
                circuit.cx(qr_a[i+1], qr_a[i])

            # main ripple-carry part (self inverse except cmp)
            circuit.ccx(qr_a[0], qr_b[0], qr_aux)
            if num_state_qubits > 2:
                circuit.ccx(qr_aux, qr_b[1], qr_a[1])
            for i in range(1, num_state_qubits - 2):
                circuit.ccx(qr_a[i], qr_b[i+1], qr_a[i+1])

            # cmp
            circuit.cx(qr_a[-1], qr_cmp)
            if num_state_qubits <= 2:
                circuit.ccx(qr_aux, qr_b[-1], qr_cmp)
            else:
                circuit.ccx(qr_a[-2], qr_b[-1], qr_cmp)

            for i in reversed(range(1, num_state_qubits-2)):
                circuit.ccx(qr_a[i], qr_b[i+1], qr_a[i+1])
            if num_state_qubits > 2:
                circuit.ccx(qr_aux, qr_b[1], qr_a[1])
            circuit.ccx(qr_a[0], qr_b[0], qr_aux)

            # post CNOT gates (inversed to pre CNOT)
            for i in reversed(range(1, num_state_qubits - 1)):
                circuit.cx(qr_a[i+1], qr_a[i])
            if num_state_qubits > 1:
                circuit.cx(qr_a[1], qr_aux)
            for i in reversed(range(1, num_state_qubits)):
                circuit.cx(qr_a[i], qr_b[i])
            
        # inverse first register
        circuit.x(qr_a)
        
        # if check for a >= b, add NOT on cmp
        if geq:
            circuit.x(qr_cmp)
            
        self.append(circuit.to_gate(), self.qubits)
