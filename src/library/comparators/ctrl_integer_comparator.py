# This code is derived from Qiskit.
#
# Qiskit(C) Copyright IBM 2017, 2020.
#
# This code is licensed under the Apache License, Version 2.0. You may
# obtain a copy of this license in the LICENSE.txt file in the root directory
# of this source tree or at http://www.apache.org/licenses/LICENSE-2.0.
#
# Any modifications or derivative works of this code must retain this
# copyright notice, and modified files need to carry a notice indicating
# that they have been altered from the originals.

"""Controlled integer comparator."""

from typing import List, Optional
from math import ceil, log2
from qiskit.circuit import QuantumCircuit, QuantumRegister, AncillaRegister
from qiskit.circuit.library.boolean_logic import OR


class CtrlIntegerComparator(QuantumCircuit):
    r"""Controlled integer comparator, based on the qiskit
    IntegerComparator.
    
    Uses one extra ancilla qubit for the original compare state.
    The new compare state is set with CCNOT gates, controlled by
    the original compare qubit and the control qubit. 
    
    Example circuit for value=6:
    .. parsed-literal::
                                   
        state_0: ─────────────────────────────
                 ┌─────┐               ┌─────┐
        state_1: ┤0    ├───────────────┤0    ├
                 │     │               │     │
        state_2: ┤     ├──■─────────■──┤     ├
                 │     │  │         │  │     │
           ctrl: ┤     ├──┼────■────┼──┤     ├
                 │  or │  │  ┌─┴─┐  │  │  or │
            cmp: ┤     ├──┼──┤ X ├──┼──┤     ├
                 │     │  │  └─┬─┘  │  │     │
          aux_0: ┤1    ├──┼────┼────┼──┤1    ├
                 │     │  │    │    │  │     │
          aux_1: ┤2    ├──■────┼────■──┤2    ├
                 └─────┘┌─┴─┐  │  ┌─┴─┐└─────┘
          aux_2: ───────┤ X ├──■──┤ X ├───────
                        └───┘     └───┘       
            
    **References:**

    [1] Qiskit-Terra, Integer Comparator, 2020.
    `IntegerComparator` <https://qiskit.org/documentation/stubs/qiskit.circuit.library.IntegerComparator.html>`_
    
    """

    def __init__(
        self, value: int, num_state_qubits: Optional[int] = None,
        geq: bool = True, name: str = None,
    ) -> None:
        """Create a new fixed value controlled comparator circuit.

        Args:
            value: The fixed value to compare with.
            num_state_qubits: Number of state qubits. If this is set it will determine the number
                of qubits required for the circuit.
            geq: If True, evaluate a ``>=`` condition, else ``<``.
            name: Name of the circuit.
        Raises:
            ValueError: If ``num_state_qubits`` is not big enough to hold the value.
        """
        min_num_sq = ceil(log2(value + 1))
        if num_state_qubits is None:
            num_state_qubits = min_num_sq
        elif num_state_qubits < min_num_sq:
            raise ValueError(
                f"The number of state qubits must be at least {min_num_sq} to fit the value.")
        
        self._value = value
        self._num_state_qubits = num_state_qubits
        self._geq = geq
        
        if name is None:
            if geq:
                name = f"CtrlIntCmp(>={value})"
            else:
                name = f"CtrlIntCmp(<{value})"
        
        super().__init__(name=name)
        
        qr_state = QuantumRegister(num_state_qubits, "state")
        qr_ctrl = QuantumRegister(1, "ctrl")
        qr_cmp = QuantumRegister(1, "cmp")
        qr_aux = AncillaRegister(num_state_qubits, "aux")
        self.add_register(qr_state, qr_ctrl, qr_cmp, qr_aux)
        
        # build circuit
        circuit = QuantumCircuit(*self.qregs, name=name)

        if value <= 0:  # condition always satisfied for non-positive values
            if geq:  # otherwise the condition is never satisfied
                circuit.cx(qr_ctrl, qr_cmp)
        # condition never satisfied for values larger than or equal to 2^n
        elif value < pow(2, num_state_qubits):

            if num_state_qubits > 1:
                twos = self._get_twos_complement()
                for i in range(num_state_qubits):
                    if i == 0:
                        if twos[i] == 1:
                            circuit.cx(qr_state[i], qr_aux[i])
                    elif i < num_state_qubits - 1:
                        if twos[i] == 1:
                            circuit.compose(
                                OR(2), [qr_state[i], qr_aux[i - 1], qr_aux[i]], inplace=True
                            )
                        else:
                            circuit.ccx(qr_state[i], qr_aux[i - 1], qr_aux[i])
                    else:
                        if twos[i] == 1:
                            # OR needs the result argument as qubit not register, thus
                            # access the index [0]
                            circuit.compose(
                                OR(2), [qr_state[i], qr_aux[i - 1], qr_aux[-1]], inplace=True
                            )
                            circuit.ccx(qr_aux[-1], qr_ctrl, qr_cmp)
                            circuit.compose(
                                OR(2), [qr_state[i], qr_aux[i - 1], qr_aux[-1]], inplace=True
                            )
                        else:
                            circuit.ccx(qr_state[i], qr_aux[i - 1], qr_aux[-1])
                            circuit.ccx(qr_aux[-1], qr_ctrl, qr_cmp)
                            circuit.ccx(qr_state[i], qr_aux[i - 1], qr_aux[-1])

                # flip result bit if geq flag is false
                if not geq:
                    circuit.cx(qr_ctrl, qr_cmp)

                # uncompute ancillas state
                for i in reversed(range(num_state_qubits - 1)):
                    if i == 0:
                        if twos[i] == 1:
                            circuit.cx(qr_state[i], qr_aux[i])
                    else:
                        if twos[i] == 1:
                            circuit.compose(
                                OR(2), [qr_state[i], qr_aux[i - 1], qr_aux[i]], inplace=True
                            )
                        else:
                            circuit.ccx(qr_state[i], qr_aux[i - 1], qr_aux[i])
            else:

                # num_state_qubits == 1 and value == 1:
                circuit.ccx(qr_state[0], qr_ctrl, qr_cmp)

                # flip result bit if geq flag is false
                if not geq:
                    circuit.cx(qr_ctrl, qr_cmp)

        else:
            if not geq:  # otherwise the condition is never satisfied
                circuit.cx(qr_ctrl, qr_cmp)

        self.append(circuit.to_gate(), self.qubits)

    @property
    def value(self) -> int:
        """The value to compare the qubit register to.

        Returns:
            The value against which the value of the qubit register is compared.
        """
        return self._value

    @property
    def geq(self) -> bool:
        """Return whether the comparator compares greater or less equal.

        Returns:
            True, if the comparator compares ``>=``, False if ``<``.
        """
        return self._geq

    @property
    def num_state_qubits(self) -> int:
        """The number of qubits encoding the state for the comparison.

        Returns:
            The number of state qubits.
        """
        return self._num_state_qubits

    def _get_twos_complement(self) -> List[int]:
        """Returns the 2's complement of ``self.value`` as array.

        Returns:
             The 2's complement of ``self.value``.
        """
        twos_complement = pow(2, self.num_state_qubits) - ceil(self.value)
        twos_complement = f"{twos_complement:b}".rjust(self.num_state_qubits, "0")
        twos_complement = [
            1 if twos_complement[i] == "1" else 0 for i in reversed(range(len(twos_complement)))
        ]
        return twos_complement
