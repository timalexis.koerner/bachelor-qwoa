# Bachelor Thesis Project

Repository for the Bachelor thesis "Quantum Approximation Algorithms and the Traveling Salesman Problem" by Tim Alexis Körner.  
The requirements are listed in `requirements.txt`.  
The quantum circuits are presented in the Jupyter-Notebooks in `/src`, the circuits themself are written in the Python programming language and can be found in `/src/library`.  